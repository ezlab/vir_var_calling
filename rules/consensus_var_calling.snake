import yaml

localrules: recalibrate, prepare_recalibration, postproc, call_variant_samtools, ref_pileup

## Callers
CALLERS=["samtools", "freebayes", "snver", "varscan2", "gatk", "lofreq"]

## Load executable paths
EXE_LOFREQ=config["exe"]["lofreq"]
EXE_FREEBAYES=config["exe"]["freebayes"]
EXE_SNVER=config["exe"]["SNVerIndividual"]
EXE_VARSCAN=config["exe"]["VarScan2"]
EXE_SAMTOOLS=config["exe"]["samtools"]
EXE_BCFTOOLS=config["exe"]["bcftools"]
EXE_SNVERPROC="%s/%s"%(SCRIPT_DIR, config["exe"]["snver_merge_snp_indel_files"])
EXE_VARSCANPROC="%s/%s"%(SCRIPT_DIR, config["exe"]["varscan_merge_snp_indel_files"])
EXE_GATK=config["exe"]["GATK"]
EXE_SNPEFF=config["exe"]["snpEff"]
EXE_RANK="%s/%s"%(SCRIPT_DIR, config["exe"]["rank_variants"])
EXE_ANNOTATE="%s/%s"%(SCRIPT_DIR, config["exe"]["annotate_vcf"])
EXE_VCFLIBBREAKMULTI=config["exe"]["vcfbreakmulti"]
EXE_VCFALLELLICPRIMITIVES=config["exe"]["vcfallelicprimitives"]
EXE_EXTRACT="%s/%s"%(SCRIPT_DIR, config["exe"]["extract_coord_vcf"])
EXE_MERGE="%s/%s"%(SCRIPT_DIR, config["exe"]["merge_vcf_cut_genes"])
EXE_FILTER="%s/%s"%(SCRIPT_DIR, config["exe"]["filter_vcf_max_rank"])
EXE_PATCH="%s/%s"%(SCRIPT_DIR, config["exe"]["patch"])
EXE_SPLITVCF="%s/%s"%(SCRIPT_DIR, config["exe"]["split_vcf"])


## OK... Seems when passing that to subworkflows tranforms it into an absolute pass.
bam_postproc="mapping/{sample}/{sample}.%s.%s.sorted.dedup.realigned.indelqual.bam"%(TAG,MAPPER)
SUB_DIR="calling/{sample}"
BAM_FILE="mapping/{sample}/{sample}.%s.%s.postproc.bam"%(TAG,MAPPER)
BAI_FILE="mapping/{sample}/{sample}.%s.%s.postproc.bai"%(TAG,MAPPER)

###
## Compresse the vcf files
rule compress_and_index_vcf:
    input:
        consensus_vcf="calling/{sample}/{sample}.{ref}.%s.consensus.cont_ann.func_ann.all.vcf"%(MAPPER)
    output:
        compressed_vcf="calling/{sample}/{sample}.{ref}.%s.consensus.cont_ann.func_ann.all.vcf.bgz"%(MAPPER),
        vcf_index="calling/{sample}/{sample}.{ref}.%s.consensus.cont_ann.func_ann.all.vcf.bgz.tbi"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{ref}.%s.compress_vcf.log"%(MAPPER),
        err="log/{sample}/{sample}.{ref}.%s.compress_vcf.err"%(MAPPER)
    threads: 1
    run:
        shell("bgzip -c {input.consensus_vcf} > {output.compressed_vcf}")
        shell("tabix -f {output.compressed_vcf} > {output.vcf_index}")

def get_cut_genes_files(wildcards):
    sample = wildcards.sample
    ref = wildcards.ref
    parts = [f"calling/{sample}/cutgenes/{sample}.{ref}.{cutgene}.{MAPPER}.consensus.cont_ann.func_ann.vcf"
             for cutgene in CUT_GENES]
    return parts

###
## The VCF files of cut genes are merged with the principal one. Cut genes span through the origin. Consequently,
## this produces artificial frameshifts due to scrambled coordinates.
rule process_cut_genes:
    input:
        parts=get_cut_genes_files,
        raw_consensus_vcf="calling/{sample}/{sample}.{ref}.%s.consensus.cont_ann.func_ann.vcf"%(MAPPER)
    output:
        fused_consensus_vcf="calling/{sample}/{sample}.{ref}.%s.consensus.cont_ann.func_ann.all.vcf"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{ref}.%s.process_cutgene.log"%(MAPPER),
        err="log/{sample}/{sample}.{ref}.%s.process_cutgene.err"%(MAPPER)
    threads: 1
    run:
        for part_vcf in input.parts:
            shell("{EXE_MERGE} \
                  -gv {input.raw_consensus_vcf} \
                  -pv {part_vcf} \
                  -o {output.fused_consensus_vcf} \
                  2> {log.err} > {log.log}")

###
## The cut genes are annotated in terms in terms
rule annotate_part:
    input:
        part_vcf="calling/{sample}/cutgenes/{sample}.{ref}.{cutgene}.%s.consensus.cont_ann.vcf"%(MAPPER)
    output:
        ann_part_vcf="calling/{sample}/cutgenes/{sample}.{ref}.{cutgene}.%s.consensus.cont_ann.func_ann.vcf"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{ref}.%s.annotate_part_{cutgene}.log"%(MAPPER),
        err="log/{sample}/{sample}.{ref}.%s.annotate_part_{cutgene}.err"%(MAPPER)
    threads: 1
    run:
        ref = get_prim_contig(wildcards)
        shell("{EXE_SNPEFF} ann \
              HHV4_{ref}_{wildcards.cutgene} \
              {input.part_vcf} \
              -no-downstream \
              -no-upstream \
              -no-intergenic \
              > {output.ann_part_vcf} \
              2> {log.err}")

def get_coord_map(wildcards):
    ref = get_prim_contig(wildcards)
    return f"{REF_DIR}/{wildcards.ref}_{wildcards.cutgene}.bed"
def get_vcf_cons(wildcards):
    ref = get_ref(wildcards)
    sample = wildcards.sample
    cutgene = wildcards.cutgene
    return f"calling/{sample}/{sample}.{ref}.{MAPPER}.consensus.cont_ann.vcf"
###
## Some genes might be on the origin of replication. The coordinates of these genes
## have been previously detected and are extracted into different VCF files.
rule extract_cut_gene_variants:
    input:
        vcf_cons=get_vcf_cons,
        coord_map=get_coord_map
    output:
        part_vcf="calling/{sample}/cutgenes/{sample}.{ref}.{cutgene}.%s.consensus.cont_ann.vcf"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{ref}.%s.extract_part_{cutgene}.log"%(MAPPER),
        err="log/{sample}/{sample}.{ref}.%s.extract_part_{cutgene}.err"%(MAPPER)
    threads: 1
    run:
        ref = get_prim_contig(wildcards)
        shell("{EXE_EXTRACT} \
              -iv {input.vcf_cons} \
              -im {input.coord_map} \
              -c {ref}-{wildcards.cutgene} \
              -o {output.part_vcf} \
              2> {log.err}")

###
## Use snpEff to add functionnal annotation to the VCF file
rule add_func_annotation:
    input:
        var_call="calling/{sample}/{sample}.{ref}.%s.consensus.cont_ann.vcf"%(MAPPER)
    output:
        ann_var_call="calling/{sample}/{sample}.{ref}.%s.consensus.cont_ann.func_ann.vcf"%(MAPPER),
        ann_vcf_stats="calling/{sample}/{sample}.{ref}.%s.consensus.cont_ann.func_annstats.csv"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{ref}.%s.annotate_variants_consensus.log"%(MAPPER),
        err="log/{sample}/{sample}.{ref}.%s.annotate_variants_consensus.err"%(MAPPER)
    threads: 1
    run:
        shell("{EXE_SNPEFF} ann \
              HHV4_{wildcards.ref} \
              {input.var_call} \
              -no-downstream \
              -no-upstream \
              -no-intergenic \
              -csvStats {output.ann_vcf_stats} \
              > {output.ann_var_call} \
              2> {log.err}")

def get_consensus_vcf(wildcards):
    if wildcards.tag in REF:
        ref = wildcards.tag
    else:
        ref = REF[0]

    sample = wildcards.sample
    consensus_vcf = f"calling/{sample}/{sample}.{ref}.{MAPPER}.consensus.raw.vcf"
    return consensus_vcf
def get_cont_ann_file(wildcards):
    if wildcards.tag in REF:
        ref = wildcards.tag
    else:
        ref = REF[0]

    sample = wildcards.sample
    consensus_vcf = f"calling/{sample}/{sample}.{ref}.{MAPPER}.annotations.vcf"
    return consensus_vcf
###
## The final VCF is annotated with contextual information from a reference pileup.
rule add_context_annotation:
    input:
        consensus_vcf=get_consensus_vcf,
        ref_pileup=get_cont_ann_file
    output:
        consensus_vcf="calling/{sample}/{sample}.{tag}.%s.consensus.cont_ann.vcf"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{tag}.%s.annotate_context_ann.log"%(MAPPER),
        err="log/{sample}/{sample}.{tag}.%s.annotate_context_ann.err"%(MAPPER)
    threads: 1
    run:
        shell("{EXE_ANNOTATE} -iv {input.consensus_vcf} -a {input.ref_pileup} \
        -ov {output.consensus_vcf} 2> {log.err}")

def get_map_file(wildcards):
    if wildcards.tag in REF:
        ref = wildcards.tag
    else:
        ref = REF[0]
    sample = wildcards.sample

    map_file = f"mapping/{sample}/{sample}.{ref}.%s.postproc.bam"%(MAPPER)
    return map_file

###
## Create a reference pileup from which to drain information about coverage.
rule ref_pileup:
    input:
        indelqual_bam=get_map_file,
        ref=get_ref_file
    output:
        ref_pileup="calling/{sample}/{sample}.{tag}.%s.annotations.vcf"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{tag}.%s.reference_pileup.log"%(MAPPER),
        err="log/{sample}/{sample}.{tag}.%s.reference_pileup.err"%(MAPPER)
    params:
        ploidy=1,
        min_baseQ=config["callers_generic"]["min_baseQ"],
        min_mapQ=config["callers_generic"]["min_mapQ"]
    threads: 1
    run:
        shell("{EXE_SAMTOOLS} mpileup -uf {input.ref} --min-BQ {params.min_baseQ} --min-MQ {params.min_mapQ} -ABO -v {input.indelqual_bam} --output-tags AD,SP | \
        {EXE_BCFTOOLS} norm -f {input.ref} -m -both -oV -o - | \
        {EXE_BCFTOOLS} call --ploidy 1 -A -m --format-fields GP,GQ -Ov -o - | \
        gawk -F$'\\t' '{{if ($5 != \".\") print $0}}' > {output.ref_pileup} 2> {log.err}")


def get_files_for_var_consensus(wildcards):
    if wildcards.tag in REF:
        ref = wildcards.tag
    else:
        ref = REF[0]

    sample = wildcards.sample
    files = [f"calling/{sample}/{sample}.{ref}.{MAPPER}.{caller}.vcf" for caller in CALLERS]
    return files

###
## Create the consensus VCF file of all callers
rule calling:
    input:
        files=get_files_for_var_consensus,
        ref_file=get_ref_file
    output:
        consensus_vcf="calling/{sample}/{sample}.{tag}.%s.consensus.raw.vcf"%(MAPPER),
        sorting_fields="calling/{sample}/{sample}.{tag}.sorting_fields.yaml",
        rank_per_caller="calling/{sample}/stats/{sample}.{tag}.%s.rank_per_caller.dat"%(MAPPER),
        distances_between_callers="calling/{sample}/stats/{sample}.{tag}.%s.distances_between_callers.dat"%(MAPPER),
        nvar_per_caller="calling/{sample}/stats/{sample}.{tag}.%s.nvar_per_caller.dat"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{tag}.%s.consensus.log"%(MAPPER),
        err="log/{sample}/{sample}.{tag}.%s.consensus.err"%(MAPPER)
    threads: 1
    run:
        yaml.dump(config["calling"], open(output.sorting_fields, "w"))
        prefix = "calling/%s/%s.%s.%s"%(wildcards.sample, wildcards.sample, wildcards.tag, MAPPER)
        refx = get_ref(wildcards)
        shell("{EXE_RANK} -l {input.files} \
               -sf {output.sorting_fields} \
               --reference {input.ref_file} \
               --sample {wildcards.sample} \
               --outfile {output.consensus_vcf} \
               --statfile {output.rank_per_caller} \
               --distances {output.distances_between_callers} \
               --nvar_per_caller {output.nvar_per_caller} \
               --raw_scores stats/{refx}.%s.raw_scores"%(MAPPER))

def get_param_callers(config, caller_str, param):
    """ Get parameter for caller
    """
    return config["callers_generic"][param] if param not in config[caller_str] else config[caller_str][param]
###
## Call variance using LoFreq
rule call_variant_lofreq:
    input:
        indelqual_bam="mapping/{sample}/{sample}.{tag}.{mapper}.postproc.bam",
        ref=get_ref_file
    output:
        vcf_lofreq="calling/{sample}/{sample}.{tag}.{mapper}.lofreq.vcf"
    log:
        log="log/{sample}/{sample}.{tag}.{mapper}.call_variance_lofreq.log",
        err="log/{sample}/{sample}.{tag}.{mapper}.call_variance_lofreq.err"
    params:
        min_baseQ=get_param_callers(config, "call_var_lofreq", "min_baseQ"),
        min_mapQ=get_param_callers(config, "call_var_lofreq", "min_mapQ"),
        min_AF=get_param_callers(config, "call_var_lofreq", "min_AF"),
        ploidy=get_param_callers(config, "call_var_lofreq", "ploidy")
    threads: 1
    run:
        ## TODO: remove that hack
        shell("{EXE_LOFREQ} call --call-indel --verbose --no-default-filter \
        --min-alt-bq {params.min_baseQ} -f {input.ref} {input.indelqual_bam} \
        -o {output.vcf_lofreq} > {log.log} 2> {log.err}")

###
## Call variance using samtools
rule call_variant_samtools:
    input:
        indelqual_bam="mapping/{sample}/{sample}.{tag}.{mapper}.postproc.bam",
        ref=get_ref_file
    output:
        vcf_samtools="calling/{sample}/{sample}.{tag}.{mapper}.samtools.vcf"
    log:
        log="log/{sample}/{sample}.{tag}.{mapper}.call_variance_samtools.log",
        err="log/{sample}/{sample}.{tag}.{mapper}.call_variance_samtools.err"
    params:
        min_baseQ=get_param_callers(config, "call_var_samtools", "min_baseQ"),
        min_mapQ=get_param_callers(config, "call_var_samtools", "min_mapQ"),
        min_AF=get_param_callers(config, "call_var_samtools", "min_AF"),
        ploidy=get_param_callers(config, "call_var_samtools", "ploidy")
    threads: 1
    run:
        shell("bcftools mpileup -f {input.ref} {input.indelqual_bam} \
                --min-BQ {params.min_baseQ} \
                --min-MQ {params.min_mapQ} \
                --count-orphans |\
               bcftools norm -f {input.ref} \
                -m -both -Ov -o - | \
               bcftools call \
                --format-fields GQ \
                --prior 1e-2 \
                -cv \
                > {output.vcf_samtools} 2> {log.log}")
#                --ploidy {params.ploidy} -cv - \

###
## Call variance using freebayes
rule call_variant_freebayes:
    input:
        indelqual_bam="mapping/{sample}/{sample}.{tag}.{mapper}.postproc.bam",
        ref=get_ref_file
    output:
        vcf_freebayes_raw="calling/{sample}/{sample}.{tag}.{mapper}.freebayes.raw.vcf",
        vcf_freebayes="calling/{sample}/{sample}.{tag}.{mapper}.freebayes.vcf"
    log:
        log="log/{sample}/{sample}.{tag}.{mapper}.call_variance_freebayes.log",
        err="log/{sample}/{sample}.{tag}.{mapper}.call_variance_freebayes.err"
    params:
        min_baseQ=get_param_callers(config, "call_var_freebayes", "min_baseQ"),
        min_mapQ=get_param_callers(config, "call_var_freebayes", "min_mapQ"),
        min_AF=get_param_callers(config, "call_var_freebayes", "min_AF"),
        ploidy=get_param_callers(config, "call_var_freebayes", "ploidy"),
        n_best_alleles=3,
        min_score=10
    threads: 1
    run:
        shell("freebayes -f {input.ref} \
              {input.indelqual_bam} \
              --ploidy {params.ploidy} \
              --min-alternate-fraction {params.min_AF} \
              --min-mapping-quality {params.min_mapQ} \
              --min-base-quality {params.min_baseQ} \
              --use-best-n-alleles {params.n_best_alleles} \
              --no-mnps \
              --no-complex \
              --min-repeat-entropy 1 \
        2> {log.log} | {EXE_VCFLIBBREAKMULTI} | {EXE_VCFALLELLICPRIMITIVES} -kg | sed -r \"s/[A-Z0-9]+=;//g\" \
        > {output.vcf_freebayes_raw}")
        shell("vcffilter -f \"QUAL > {params.min_score}\" {output.vcf_freebayes_raw} > {output.vcf_freebayes}")

###
## Call variance using snver
rule call_variant_snver:
    input:
        indelqual_bam="mapping/{sample}/{sample}.{tag}.{mapper}.postproc.bam",
        ref=get_ref_file
    output:
        vcf_snver="calling/{sample}/{sample}.{tag}.{mapper}.snver.vcf",
    log:
        log="log/{sample}/{sample}.{tag}.{mapper}.call_variance_snver.log",
        err="log/{sample}/{sample}.{tag}.{mapper}.call_variance_snver.err"
    params:
        min_baseQ=get_param_callers(config, "call_var_snver", "min_baseQ"),
        min_mapQ=get_param_callers(config, "call_var_snver", "min_mapQ"),
        min_AF=get_param_callers(config, "call_var_snver", "min_AF"),
        ploidy=get_param_callers(config, "call_var_snver", "ploidy")
    threads: 1
    run:
        prefix="calling/%s/%s.%s.%s.snver"%(wildcards.sample, wildcards.sample, wildcards.tag, wildcards.mapper)
        shell("SNVerIndividual -r {input.ref} -i {input.indelqual_bam} \
               -n {params.ploidy} \
               -bq {params.min_baseQ} \
               -mq {params.min_mapQ}  \
               -b {params.min_AF} \
               -o {prefix} \
               > {log.log} 2> {log.err}")
        ## SNVer messes up the header of the PL field. Num should be 3
        if os.path.exists(f"{prefix}.filter.vcf"):
            if os.stat(f"{prefix}.filter.vcf").st_size == 0:
                shell(f"cp {prefix}.raw.vcf {prefix}.filter.vcf")
        else:
            shell(f"cp {prefix}.raw.vcf {prefix}.filter.vcf")
        if os.path.exists(f"{prefix}.indel.filter.vcf"):
            if os.stat(f"{prefix}.indel.filter.vcf").st_size == 0:
                shell(f"cp {prefix}.indel.raw.vcf {prefix}.indel.filter.vcf")
        else:
            shell(f"cp {prefix}.indel.raw.vcf {prefix}.indel.filter.vcf")
        shell("sed -ri 's/PL,Number=1/PL,Number=3/g' {prefix}.*")
        shell("touch {prefix}.filter.vcf")
        shell("{EXE_SNVERPROC} -p {prefix} -pl {params.ploidy} -s {wildcards.sample} 2>> {log.log}")
        shell("touch {output.vcf_snver}")

###
## Call variance using freebayes
rule call_variant_varscan:
    input:
        indelqual_bam="mapping/{sample}/{sample}.{tag}.{mapper}.postproc.bam",
        ref=get_ref_file
    output:
        pileup=temp("calling/{sample}/{sample}.{tag}.{mapper}.sorted.dedup.realigned.indelqual.pileup"),
        snp_call_varscan="calling/{sample}/{sample}.{tag}.{mapper}.varscan2.snp.vcf",
        indel_call_varscan="calling/{sample}/{sample}.{tag}.{mapper}.varscan2.indel.vcf",
        vcf_varscan="calling/{sample}/{sample}.{tag}.{mapper}.varscan2.vcf"
    log:
        log="log/{sample}/{sample}.{tag}.{mapper}.call_variance_varscan2.log",
        err="log/{sample}/{sample}.{tag}.{mapper}.call_variance_varscan2.err"
    params:
        min_baseQ=get_param_callers(config, "call_var_varscan", "min_baseQ"),
        min_mapQ=get_param_callers(config, "call_var_varscan", "min_mapQ"),
        min_AF=get_param_callers(config, "call_var_varscan", "min_AF"),
        ploidy=get_param_callers(config, "call_var_varscan", "ploidy")
    threads: 1
    run:
        prefix="calling/%s/%s.%s.%s.varscan2"%(wildcards.sample, wildcards.sample, wildcards.tag, wildcards.mapper)
        shell("samtools mpileup  {input.indelqual_bam} \
            -A \
            --fasta-ref {input.ref} \
            --output-tags AD,ADF,ADR \
            --min-BQ {params.min_baseQ} \
            --min-MQ {params.min_mapQ} \
            > {output.pileup} 2> {log.log}")
        shell("VarScan2 mpileup2snp {output.pileup} \
            --min-var-freq {params.min_AF} \
            --min-avg-qual {params.min_baseQ} \
            --output-vcf 1 \
            > {output.snp_call_varscan} 2>> {log.log}")
        shell("VarScan2 mpileup2indel {output.pileup} \
            --min-var-freq {params.min_AF} \
            --min-avg-qual {params.min_baseQ} \
            --output-vcf 1 \
            > {output.indel_call_varscan} 2>> {log.log}")
        shell("{EXE_VARSCANPROC} -p {prefix} -s {wildcards.sample}")

###
## Call variance using GATK HaplotypeCaller
rule call_variant_GATK:
    input:
        indelqual_bam="mapping/{sample}/{sample}.{tag}.%s.postproc.bam"%(MAPPER),
        ref=get_ref_file
    output:
        vcf_gatk="calling/{sample}/{sample}.{tag}.%s.gatk.vcf"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{tag}.%s.call_variance_gatk.log"%(MAPPER),
        err="log/{sample}/{sample}.{tag}.%s.call_variance_gatk.err"%(MAPPER)
    params:
        min_baseQ=get_param_callers(config, "call_var_gatk", "min_baseQ"),
        min_mapQ=get_param_callers(config, "call_var_gatk", "min_mapQ"),
        min_AF=get_param_callers(config, "call_var_gatk", "min_AF"),
        ploidy=get_param_callers(config, "call_var_gatk", "ploidy")
    threads: 1
    run:
        if wildcards.tag in REF:
            shell("gatk4.1 \
                   HaplotypeCaller \
                   -R {input.ref} \
                   -I {input.indelqual_bam} \
                   -ploidy {params.ploidy} \
                   --minimum-mapping-quality {params.min_mapQ} \
                   --min-base-quality-score {params.min_baseQ} \
                   -stand-call-conf 4 \
                   -O {output.vcf_gatk} \
                   > {log.log} 2> {log.err}")
