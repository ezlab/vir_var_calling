#!/usr/bin/env python
## Last modified:
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
import yaml
import itertools

import collections
import pprint

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

_pipeline_dir = os.path.dirname(os.path.realpath(__file__))

def dict_merge(dct, merge_dct):
    """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k, v in merge_dct.items():
        if (k in dct and isinstance(dct[k], dict)
                and isinstance(merge_dct[k], collections.Mapping)):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]
    return dct

###------------------------------------------------------------------------------------------------
## MAIN
##
def main():
    """ Main function
    """

    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-o', '--output_config_file', dest='output_config_file',
                        help='Output config file',
                        metavar='FILE', required=False)
    parser.add_argument('-d', '--default', dest='default',
                        help='Template for config',
                        metavar='FILE', required=False)
    parser.add_argument('-e', '--external', dest='external',
                        help='External configuration',
                        metavar='FILE', required=False)
    parser.add_argument('-n', '--project_name', dest='project_name',
                        help='Name of your project',
                        type=str, required=False, default="default_name")
    parser.add_argument('-s', '--sample_sheet', dest='sample_sheet',
                        help='Location of the sample sheet',
                        metavar="FILE", required=True)
    parser.add_argument('-r', '--reference', dest='reference',
                        help='Reference against which to call variants',
                        type=str, required=False, nargs="+")
    parser.add_argument('-f', '--reference_file', dest='reference_file',
                        help='Reference containing reference fasta sequences',
                        required=False, metavar='FILE', nargs="?")
    parser.add_argument('-b', '--batch', dest='batch',
                        help='Batch, in case you should treat samples separately',
                        type=str, required=False)
    parser.add_argument('-m', '--mapper', dest='mapper',
                        help='Mapper to use',
                        type=str, required=False)
    parser.add_argument('-c', '--cut_genes', dest='cut_genes',
                        help='Cut genes, like LMP-2A in EBV',
                        type=str, required=False)
    parser.add_argument('-cd', '--cut_genes_dir', dest='cut_genes_dir',
                        help='Cut genes, like LMP-2A in EBV',
                        type=str, required=False)
    parser.add_argument('-rd', '--ref_dir', dest='ref_dir',
                        help='Location of the reference files',
                        type=str, required=False, default=f"{_pipeline_dir}/References")
    parser.add_argument('-w', '--work_dir', dest='work_dir',
                        help='Working directory',
                        type=str, required=False, default="")


    opts = parser.parse_args()

    ## Template config file
    if opts.default:
        default_config_file = opts.default
    else:
        default_config_file = f"{_pipeline_dir}/config/config_template.yaml"

    ## Output config file
    if opts.output_config_file:
        output_config_file = opts.output_config_file
    else:
        output_config_file = f"{work_dir}/config_snake.{opts.project_name}.yaml"

    ## Working directory
    if opts.work_dir:
        work_dir = os.path.realpath(opts.work_dir)
    else:
        work_dir = os.path.realpath(f"{opts.work_dir}/assembly_{opts.batch}")

    ## Handle reference files
    if opts.reference_file:
        reference_file = [os.path.realpath(ref_f) for ref_f in opts.reference_file.split(",")]
    else:
        reference_file = [f"{opts.ref_dir}/{ref}.fasta" for ref in opts.reference]
    ## Relocate reference files
    if opts.ref_dir:
        prefix = os.path.abspath(opts.ref_dir)
        [os.symlink(ref_f, f"{prefix}/{os.path.basename(ref_f)}") for ref_f in reference_file
                if not os.path.exists(f"{prefix}/{os.path.basename(ref_f)}")]
        reference_file = [f"{prefix}/{os.path.basename(ref_f)}" for ref_f in reference_file]
        ref_dir = os.path.realpath(opts.ref_dir)

    if opts.reference:
        reference = list(itertools.chain(*[ref.split(",") for ref in opts.reference]))
    references = dict(zip(reference, reference_file))

    ## Location of scripts
    script_dir = f"{_pipeline_dir}/scripts"

    with open(default_config_file, "r") as DEFAULT_CONFIG:
        config = yaml.load(DEFAULT_CONFIG)

    config["WORKDIR"] = f"{work_dir}"
    config["PIPELINE_DIR"] = f"{_pipeline_dir}"
    config["SCRIPT_DIR"] = f"{script_dir}"

    config["SAMPLE_SHEET"] = os.path.realpath(opts.sample_sheet)
    config["PROJECT_NAME"] = opts.project_name
    config["BATCH"] = opts.batch
    config["REF_DIR"] = ref_dir
    config["REF_FILE"] = references
    config["MAPPER"] = opts.mapper
    config["REF"] = reference

    if opts.external and opts.external != "None":
        with open(opts.external) as EXT:
            external = yaml.load(EXT)
            config = dict_merge(config, external)

    if opts.cut_genes:
        cut_genes = opts.cut_genes.split(",")
        if len(cut_genes) == 1 and cut_genes[0] == "":
            cut_genes = []
        config["CUT_GENES"] = cut_genes
        print(reference)
        [[os.symlink(f"{opts.cut_genes_dir}/{ref}_{gene}.bed", f"{ref_dir}/{ref}_{gene}.bed")
            for gene in cut_genes
                if not os.path.exists(f"{ref_dir}/{ref}_{gene}.bed")]
            for ref in reference]

    with open(output_config_file, "w") as OUT_CONFIG_FILE:
        OUT_CONFIG_FILE.write(yaml.dump(config, default_flow_style=False))

if __name__ == "__main__":
    main()
