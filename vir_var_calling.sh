#!/bin/bash

## Setup bash
set -o errexit
set -o pipefail
set -o nounset
## Principal locations
BASE_DIR="$( cd "$( dirname $( readlink  -f $(which "${BASH_SOURCE[0]}" ) ) )" && pwd  )"
BASE_FILE="${BASE_DIR}$(basename "${BASH_SOURCE[0]}")"
BASE="$(basename "${BASE_FILE}" .sh)"
CURR_DIR="$(pwd)"

PIPELINE_DIR="${BASE_DIR}"

SCRIPTS_DIR="${PIPELINE_DIR}/scripts/"
SNAKEFILE="${PIPELINE_DIR}/Snakefile"
CLUSTERCONFIGFILE="${PIPELINE_DIR}/config/cluster_config.yaml"

## Use the following return codes for functions
## Bool
BOOLTRUE=0
BOOLFALSE=66

# shellcheck disable=SC1091,SC1090
source "${SCRIPTS_DIR}/common_methods.sh"

## NOTE: Dirty hack not to specify strain
strain="EBV"
dataset="hiv"
mapper="bwa_mem"
cut_genes="LMP-2A"
cut_genes_dir="${HOME}/research/EBV/reference_pool"

workload_manager="slurm"
#workload_manager="lsf"

## Cluster specific stuff
if [[ ${workload_manager} == "lsf" ]]; then
    cluster_command="bsub -M 4194304 -q normal -n 1 -J \"vir_var_calling\" -o log/cluster/lsf.%J.out -e log/cluster/lsf.%J.err"
elif [[ ${workload_manager} == "slurm" ]]; then
    cluster_command="sbatch --time={cluster.time} --exclude={cluster.exclude} --cpus-per-task={cluster.CPUs} --mem={cluster.mem_per_cpu} --job-name={cluster.job_name} --output={cluster.log} --error={cluster.err} --partition={cluster.partition}"
fi


###
## Make a list of all reads pairs
make_sample_map(){
    WDIR=$1
    OUT=$2
    echo "sample,forward,reverse" > "${OUT}"
    for sample in $(ls "${PWD}/${WDIR}"); do
        echo ${sample},$(find "${PWD}/${WDIR}/${sample}" | sort | egrep "\.((1|2)|(r|f)|(R1|R2))?\.(fastq|fq)?(\.gz)?$") | sed -r "s/\s/,/g" >> "${OUT}"
    done
    echo ${OUT}
}

###
## Read arguments
external_config=None
while getopts "ptdr:f:s:x:b:c:m:g:e:vu" opt; do
    case ${opt} in
        r)
            reference=$(echo "${OPTARG}" | sed -r "s/,/ /g")
            ;;
        f)
            reference_file=${OPTARG}
            ;;
        s)
            sample_sheet="${OPTARG}"
            ;;
        x)
            strain="${OPTARG}"
            ;;
        m)
            mapper="${OPTARG}"
            ;;
        g)
            cut_genes="${OPTARG}"
            ;;
        e)
            external_config="${OPTARG}"
            ;;
        b)
            ## Set path for all directories
            export PROJECT_NAME="${OPTARG}"
            export PROJECT_DIR="VVC_results_${PROJECT_NAME}"
            export CONFIGFILE="${PROJECT_DIR}/config.${PROJECT_NAME}.yaml"
            export REF_DIR="${PROJECT_DIR}/reference"
            export RAWDATA_DIR="${PROJECT_DIR}/rawdata"
            export PREPROC_DIR="${PROJECT_DIR}/preproc"
            export MAPPING_DIR="${PROJECT_DIR}/mapping"
            export CALLING_DIR="${PROJECT_DIR}/calling"
            export LOG_DIR="${PROJECT_DIR}/log"
            export CLUSTER_LOG_DIR="${PROJECT_DIR}/log/cluster"
            export STATUS_DIR="${PROJECT_DIR}/joblist"
            export STATISTICS_DIR="${PROJECT_DIR}/stats"
            export SCRIPT_DIR="${PROJECT_DIR}/scripts"
            export TMP_DIR="/tmp"
            export BQS_DIR="${PROJECT_DIR}/BQS_recalibration"

            mkdir -p "${PROJECT_DIR}"
            mkdir -p "${PREPROC_DIR}"
            mkdir -p "${MAPPING_DIR}"
            mkdir -p "${CALLING_DIR}"
            mkdir -p "${REF_DIR}"
            mkdir -p "${STATUS_DIR}"
            mkdir -p "${STATISTICS_DIR}"
            mkdir -p "${BQS_DIR}"
            mkdir -p "${SCRIPT_DIR}"
            mkdir -p "${TMP_DIR}"
            mkdir -p "${LOG_DIR}"
            mkdir -p "${CLUSTER_LOG_DIR}"
            ## Create log cluster directories. Jobs crash without error without doing this.
            for sample in $(ls ${RAWDATA_DIR}); do
                mkdir -p ${CLUSTER_LOG_DIR}/${sample}
            done

            ## Files locations
            [[ -z ${reference_file+x} ]] && reference_file=""
            ## Make sample sheet
            if [ -z ${sample_sheet+x} ]; then
                sample_sheet="${PROJECT_DIR}/sample_sheet.csv"
                echo "sample,forward,reverse" > ${sample_sheet}
                sample_sheet=$(make_sample_map ${RAWDATA_DIR} ${PROJECT_DIR}/sample_sheet.csv)
            fi

            ## All variables to be passed to the snakemake pipeline.
            config_line="PIPE_DIR=${BASE_DIR}"
            ${BASE_DIR}/pipeline_setup.py \
                -n "${PROJECT_NAME}" \
                -r ${reference} \
                -e ${external_config} \
                -c ${cut_genes} \
                -cd ${cut_genes_dir} \
                -f ${reference_file} \
                -m "bwa_mem" \
                -w "${PROJECT_DIR}" \
                -rd "${REF_DIR}" \
                -o "${CONFIGFILE}" \
                -s "${sample_sheet}"
            ;;
        p)
            snakemake --snakefile ${SNAKEFILE} --configfile "${CONFIGFILE}" --ignore-incomplete \
            --directory "${PROJECT_DIR}" --reason --printshellcmds --verbose --nolock --latency-wait 60 \
            --cluster "${cluster_command}" --cluster-config "${CLUSTERCONFIGFILE}" --jobs 285
            ;;
        t)
            snakemake --snakefile ${SNAKEFILE} --ignore-incomplete  \
            --configfile "${CONFIGFILE}" \
            --directory "${PROJECT_DIR}" --reason --printshellcmds --verbose --touch --latency-wait 60 \
            --cluster-config "${CLUSTERCONFIGFILE}" \
            --cluster "${cluster_command}" \
            --jobs 144
            ;;
        c)
            snakemake --snakefile ${SNAKEFILE} \
            --configfile "${CONFIGFILE}" \
            --directory "${PROJECT_DIR}" --verbose --debug --cleanup-metadata ${OPTARG} \
            ;;
        d)
            snakemake --snakefile ${SNAKEFILE} --ignore-incomplete --forceall \
            --configfile "${CONFIGFILE}" \
            --directory "${PROJECT_DIR}" --dryrun --reason --touch --printshellcmds \
            ;;
        v)
            pipeline_sketch="${PROJECT_DIR}/workflow.pdf"
            snakemake --snakefile ${SNAKEFILE}  --forceall \
            --configfile "${CONFIGFILE}" \
            --directory "${PROJECT_DIR}" --keep-going \
            --forceall --rulegraph | dot -Tpdf > "${pipeline_sketch}"
            ;;
        u)
            snakemake --snakefile ${SNAKEFILE} \
            --configfile "${CONFIGFILE}" \
            --directory "${PROJECT_DIR}" --unlock --verbose
            ;;
    esac
done


