#!/usr/bin/bash

###
## Common methods for the vir_assembly pipeline
##

###
## Make a list of all reads pairs
make_sample_map(){
	[[ -s ${samples_map} ]] && return 0
	for f in ${RAWDATA_DIR}/*.f.fastq.gz; do
		sampleID="$(basename "${f}" .f.fastq.gz)"
		echo -e "${sampleID}\t${sampleID}.f.fastq.gz\t${sampleID}.r.fastq.gz" >> "${samples_map}"
	done	
}

###
## Check if the reference has been prepare
check_reference_prep(){
	local ext_list=(dict fasta amb ann bwt fai pac sa)
	[[ -f ${REF_DIR}/${reference}.fasta ]]	|| { echo "No fasta file for this reference."; exit 1; }
	[[ -f ${REF_DIR}/${reference}.dict ]]	|| { echo "No dictionary for this reference. Please run Picard CreateSequenceDictionary."; exit 1; }
	[[ -f ${REF_DIR}/${reference}.fasta.fai ]]	|| { echo "No index file .fai. Please run bwa index to solve this."; exit 1; }
}

###
##
info_msg(){
	local lvl=$1
	local msg=${*:2}
	local prompt=$(printf "%${lvl}s" | tr ' ' -)
	local date=$(date +"%Y-%m-%d %H:%M:%S")
	echo "[${date}] ${prompt} ${msg}"
}

###
##
err_msg(){
	local lvl=$1
	local msg=${*:2}
	local date=$(date +"%Y-%m-%d %H:%M:%S")
	local red=$'\e[1;31m'
	local end=$'\e[0m'
	>&2 printf "%s[%s] X %s %s\n" "${red}" "${date}" "${msg}" "${end}"
}

debug_msg(){
	local lvl=$1
	local msg=${*:2}
	local date=$(date +"%Y-%m-%d %H:%M:%S")
	local grn=$'\e[1;32m'
	local end=$'\e[0m'
	>&2 printf "%s[%s] DBG %s %s\n" "${grn}" "${date}" "${msg}" "${end}"
}

###
## Get the id of the sample being processed
get_sample_id(){
	local in=$1
	echo "${in}" | sed -r "s/\..*$//g" | sed -r "s/^.*\///g"
}

###
## Get tags of steps the file has sustained
get_tags(){
	local in=$1
	local sampleID="$(get_sample_id "${in}")"
	echo "${in}" | sed -r "s/^.*${sampleID}\.([f|r]\.)?//g" | sed -r "s/(fastq\.gz)|(bam)|(sam)|(bai)|(vcf)|(csv)//g"
}

###
##
get_fastq_header(){
	local in=$1
	zcat "${in}" | head -1 | sed -r "s/@//g" | sed -r "s/:.+$//g"
}

###
## Utility methods for job completion check.
## States:
##	DOIT: Not done, but not failed nor cancelled. Sample to be processed the FIRST time
##	DOING: In process
##	DONE: Successfully done
##	REDO: Failed, cancelled,...

###
## Check if the sample is already on the list, if not add it.
check_sample(){
	local stepfile=$1
	local sample=$2
	if [[ $(grep "${sample}" "${stepfile}") == "" ]]; then
		return 1
	else
		return 0
	fi
}

###
## Check the step status
## Return:
##	True: when the step terminated successfully
##	False: when the step is running or terminated unsuccessfully
check_step(){
	local step=$1
	local stepfile=$2

	local jobDONE=0
	local jobDOIT=0
	local jobDOING=0
	local jobREDO=0
	local stepOK=false
	local stepTODO=false
	local stepFAILED=false
	local stepRUNNING=false
    
    ## Check if the step is already done thanks to the .DONE files in the project root
	if [[ -f ${STATUS_DIR}/.DONE_${step} ]] ||
		[[ -f ${STATUS_DIR}/.DONE_${step}_${reference} ]]; then 
		echo "DONE"
		return "${BOOLTRUE}"
	fi
	## Loop through the lines of the jobs file
	while read -r line; do
		local sampleID=$(echo "${line}" | cut -d$' ' -f1)
		local jobID=$(check_jobtable_jobID "${stepfile}" "${sampleID}")
		local state=$(check_jobtable_state "${stepfile}" "${sampleID}")
		## A job run locally currently has only two possible states.
		if [[ "${jobID}" = "LOCAL" ]]; then
			if [[ "${state}" = "DONE" ]]; then
				((jobDONE++))
			elif [[ "${state}" = "DOIT" ]]; then
				((jobDOIT++))
			fi
		## Jobs using SLURM
		else
		    ## The job hasn't started
			if [[ "${state}" = "DOIT" ]]; then
				((jobDOIT++))
				continue
			## The job has already completed previously
			elif [[ "${state}" = "DONE" ]]; then
				((jobDONE++))
				continue
			## The job has already completed previously
			elif [[ "${state}" = "REDO" ]]; then
				((jobREDO++))
				continue
			## If the job is neither completed nor yet to be started, then this
			## job is currently running.
			else
			    ## Update slurm status
				newstate=$(get_slurm_state "${jobID}")
				if [[ "${newstate}" = "COMPLETED" ]]; then
					update_jobtable_state "${stepfile}" "${sampleID}" "DONE"
					((jobDONE++))
				## The job is to be redone if a following status occured
				elif [[ "${newstate}" = "FAILED" ]] ||
			            [[ "${newstate}" = "NODE_FAIL" ]] ||
					    [[ "${newstate}" = "BOOT_FAIL" ]] ||
					    [[ "${newstate}" = "DEADLINE" ]] ||
					    [[ "${newstate}" = "TIMEOUT" ]] ||
					    [[ "${newstate}" = "CANCELLED" ]]; then
					update_jobtable_state "${stepfile}" "${sampleID}" "REDO"
					((jobREDO++))
				## Wait until the job is finished
				elif [[ "${newstate}" = "RUNNING" ]] ||
					    [[ "${newstate}" = "SUSPENDING" ]] ||
					    [[ "${newstate}" = "CONFIGURING" ]] ||
					    [[ "${newstate}" = "PENDING" ]]; then
					update_jobtable_state "${stepfile}" "${sampleID}" "DOING"
					((jobDOING++))
				fi
			fi
		fi
	done < "${stepfile}"
	#""debug_msg 1 "nDONE: ${jobDONE}"
	#debug_msg 1 "nREDO: ${jobREDO}"
	#debug_msg 1 "nDOING: ${jobDOING}"
	#debug_msg 1 "nDOIT: ${jobDOIT}"
	#debug_msg 1 "nsample: ${nsample}"
	if [[ ! "${jobDOING}" = "0" ]]; then
		stepRUNNING=true
		echo "RUNNING"
		return ${BOOLFALSE}
	elif [[ ! "${jobREDO}" = "0" ]]; then
		stepFAILED=true
		echo "FAILED"
		return ${BOOLFALSE}
	elif [[ ! "${jobDONE}" = "${nsample}" ]] ||
		[[ ! ${jobDOIT} = "0" ]]; then
		stepTODO=true
		echo "TODO"
		return ${BOOLFALSE}
	elif [[ ! "${jobDONE}" = "0" ]]; then
		stepOK=true
		if [[ "${step}" = "trimmomatic" ]] ||
			[[ "${step}" = "tagdust" ]] ||
			[[ "${step}" = "cdhitdup" ]]; then
			touch ${STATUS_DIR}/.DONE_${step}
		else
			touch ${STATUS_DIR}/.DONE_${step}_${reference}
		fi
		echo "OK"
		return ${BOOLTRUE}
	fi	
}

###
##
run_step(){
	local step=$1
	local stepfile=$2
	local in=$3
	local tagout=$4
	local scheduler=$5
	local threads=$6
	
	## Make a quick check if the step is pending or done.
	if stepstatus=$(check_step ${step} ${stepfile}); then
		info_msg 2 "Step ${step} is already done. Skipping."
		return ${BOOLTRUE}
	else
		if [[ "${stepstatus}" = "RUNNING" ]]; then
			dostuff=1
		fi
	fi
	
	for s in $(cut -d$'\t' -f1 ${samples_map}); do
		in_t=$(echo ${in} | sed -r "s/<sample>/${s}/g")
		## Check if the sample has already been processed
		if ! check_sample ${stepfile} ${s}; then
			echo -e "${s}\t${scheduler}\tDOIT" >> ${stepfile}
		fi
		state=$(check_jobtable_state ${stepfile} ${s})
		if [[ "${state}" = "DOIT" ]] ||
			[[ "${state}" = "REDO"  ]]; then
			## Generate a running script
			local cmd=$(run_${step} ${in_t} ${threads} ${tagout})
			make_running_script ${step} ${s} ${threads} "${cmd}"
			## Run the script using SLURM
			if [[ "${scheduler}" = "SLURM" ]]; then
				jobID=$(sbatch ${SCRIPT_DIR}/${step}_${reference}_${s}.sh | sed "s/[^0-9]//g")
				update_jobtable_jobID ${stepfile} ${s} ${jobID}
				update_jobtable_state ${stepfile} ${s} "DOING"
				info_msg 3 "Task: ${step}; Sample: ${s}; Submitted slurm job: ${jobID}"
				sleep 3
			## Run the script locally
			elif [[ "${scheduler}" = "LOCAL" ]]; then
				info_msg 3 "Task: ${step}; Sample: ${s}; Submitted job locally"
				bash ${SCRIPT_DIR}/${step}_${reference}_${s}.sh > ${LOG_DIR}/${step}/${s}.out 2> ${LOG_DIR}/${step}/${s}.err ; local newstate=$?
					if [[ "${newstate}" = 0 ]]; then
					update_jobtable_state ${stepfile} ${s} "DONE"
				else
					update_jobtable_state ${stepfile} ${s} "REDO"
				fi
			fi
		else
			info_msg 3 "Step ${step} already done for sample ${s}"
		fi
	done
}

###
## Wait until all jobs are completed for a step
wait_step(){
	local step=$1
	local stepfile=$2
	[[ -f ${stepfile} ]] || { info_msg 1 "X ERROR: could not find stepfile: ${stepfile}."; exit 1; }
	
	info_msg 2 "Waiting..."
	T=$(date +%s)
	t=0
	timeskip=1
	OK=false
	while [[ "${OK}" == false ]]; do
		## Check if the jobs have successfully completed
		if statestep=$(check_step ${step} ${stepfile}); then
			OK=true
			break
		else
			if [[ "${statestep}" = "RUNNING" ]]; then
				sleep ${timeskip}
				t=$((t=${t}+${timeskip}))
				[[ $(( ${t} % 1800 )) == 0 ]] && { echo "."; } || { echo -n "."; }
			elif [[ "${statestep}" = "FAILED" ]]; then 
				print_redo ${step} ${stepfile}
				exit 1
			fi
			OK=false
		fi
	done
	info_msg 2 "Done with ${step}"	
}

###
##
clear_step(){
	local step=$1
	local stepfile=$2

	rm ${stepfile}
	touch ${stepfile}
}

###
## Print the jobs to redo
print_redo(){
	local step=$1
	local stepfile=$2
	err_msg 2 "These jobs were previously unsuccessful. Please check the logs in ${LOG_DIR}"
	while read -r line; do
		jobstate=$(echo ${line} | cut -d$' ' -f3)
		[[ "${jobstate}" = "REDO" ]] || continue
		err_msg 3 ${line}
	done < ${stepfile}
}

###
## Return the state of the job for a sample
check_jobtable_state(){
	local stepfile=$1
	local sampleID=$2
	awk -F$'\t' -v sample=${sampleID} ' BEGIN { OFS=FS } $1==sample { print $3 } ' ${stepfile}
}

###
## Return the jobID of the job for a sample
check_jobtable_jobID(){
	local stepfile=$1
	local sampleID=$2
	awk -F$'\t' -v sample=${sampleID} ' BEGIN { OFS=FS } $1==sample { print $2 } ' ${stepfile}
}

###
## Update the job state
update_jobtable_state(){
	local stepfile=$1
	local sampleID=$2
	local newstate=$3
	gawk -i -F$'\t' -v sample=${sampleID} -v newstate=${newstate} ' BEGIN { OFS=FS } $1==sample { $3=newstate }1 ' ${stepfile}
}

###
## Update the job ID
update_jobtable_jobID(){
	local stepfile=$1
	local sampleID=$2
	local newjobID=$3
	gawk -i -F$'\t' -v sample=${sampleID} -v newjob=${newjobID} ' BEGIN { OFS=FS } $1==sample { $2=newjob }1 ' ${stepfile}
}

set_step_redo(){
	local stepfile=$1
	local task=$2
	rm "${STATUS_DIR}/.DONE_${task}_${reference}"
	gawk -i -F$'\t' '{printf "%s\tSLURM\t"}' "${stepfile}"
}

###
## Make a slurm script for a given command
make_running_script(){
	local task=$1
	local sample=$2
	local threads=$3
	local cmd=${*:4}
	
	## Make sure the directory exists...
	[[ -d ${LOG_DIR}/${task} ]] || { mkdir ${LOG_DIR}/${task}; }
	local header="#!/usr/bin/bash\n"
	header+="#SBATCH --partition=normal\n"
	header+="#SBATCH --exclude=b17\n"
	header+="#SBATCH --nodes=1\n"
	header+="#SBATCH --cpus-per-task=${threads}\n"
	header+="#SBATCH -J ${task}_${sample}\n"
	header+="#SBATCH --output=${LOG_DIR}/${task}/${sample}_slurm.%j.%N.out\n"
	header+="#SBATCH --error=${LOG_DIR}/${task}/${sample}_slurm.%j.%N.err\n"
	echo -e "${header}${cmd}" > ${SCRIPT_DIR}/${task}_${reference}_${sample}.sh
	## Ugly hack to make BWA actually make the reads groups header
	if [[ "${task}" ==  "bwa_align"* ]]; then
		sed -ri "s/\t/\\\t/g" ${SCRIPT_DIR}/${task}_${reference}_${sample}.sh
	fi
	#echo -e "${header} sleep 10" > ${SCRIPT_DIR}/${task}.sh
}

###
## Utility for SLURM jobs
##

###
## Get job SLURM state
get_slurm_state(){
	local jobID=$1
	state=$(sacct -j ${jobID} --format=State%30 | tail -1 | sed -r "s/^\s+//g" | sed -r "s/\s+$//g")
	echo $(echo ${state} | cut -f1 -d$' ')
}

## Export common methods
export -f make_sample_map
export -f check_reference_prep
export -f info_msg
export -f get_sample_id
export -f get_tags
export -f get_fastq_header
export -f check_sample
export -f check_step
export -f run_step
export -f wait_step
export -f print_redo
export -f check_jobtable_state
export -f check_jobtable_jobID
export -f update_jobtable_state
export -f update_jobtable_jobID
export -f make_running_script
export -f get_slurm_state
