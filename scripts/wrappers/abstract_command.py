#!/usr/bin/python3.6
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
import shutil
import executor
import abc

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'

logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

class AbstractCommand(object):

    def __init__(self,
                 exec_name,
                 exec_path=None):
        self._exec_name = exec_name
        self._exec_path = exec_path
        if exec_path is not None:
            self._exec_path = exec_path
        else:
            self.check_path()


    def check_path(self):
        """ If no path is provided, try to find it in PATH
        """
        if self._exec_name is not None:
            exec_path = shutil.which(self._exec_name)
        else:
            raise ExecutableNotFound("Executable not found: %s. Executable name not provided."%(
                                    self._exec_name))
        if self._exec_path is None:
            if exec_path is None:
                raise ExecutableNotFound("Executable not found: %s. Not in $PATH."%(
                                        self._exec_name))
            else:
                self._exec_path = exec_path
        return self._exec_path

    def execute(self):
        """ Execute the command
        """
        executor.execute(str(self))

    def __str__(self):
        """ Each command should be constructed differently. Return string formatted command.
        """
        raise NotImplementedError("")

##TODO: Implement an automatic checker for the format of the parameters.
## Something like: 'fmt_checker=check_int'
class AbstractParameter(object):
    """ An abstract class for all argument. Not for direct use.
    """
    def __init__(self,
                name=None,
                long_name=None,
                is_required=False,
                description=""):
        """ Set up default attribute for all arguments.
            name: (str)
                the short name of an argument (e.g: -h, -iv,...)
            long_name: (str)
                the long name of an argument (e.g. --test, --invcffile,...)
            is_required: (bool)
                whether the argument is required or not
            description: (str)
                Description of the argument
        """
        self._name = name
        self._long_name = long_name
        self._is_required = is_required
        self._description = description

    def __str__(self):
        """ String representation of the argument. Has to be implemented.
        """
        raise NotImplementedError

    @property
    def description(self):
        return self._description
    @description.setter
    def description(self, description):
        self._description = description

##TODO: Implement a bank of format recognition functions.
class FileParam(AbstractParameter):
    """ A class for file input or output.
    """
    def __init__(self,
                path,
                listed=None,
                has_name=True,
                *args,
                **kwargs):
        """ Set up a file.
            path: (str)
                Path of the input or output file
            listed: (bool)
                Whether there are several input files
            has_name: (bool)
                Whether the argument requires a name or not
        """
        super().__init__(*args, **kwargs)
        self._path = path
        self._listed = listed
        self._has_name = has_name

    def __str__(self):
        """ String representation for input or output files.
            Prioritize the long name over the short name.
        """
        if self.path is None:
            return ""
        elif self._long_name is not None:
            return "%s=%s"%(self._long_name, self._path)
        elif self._name is not None:
            return "%s %s"%(self._name, self._path)
        elif self._has_name:
            return "%s"%(self._path)
        else:
            raise ValueError("File parameter require a name, but none provided.")

    def check_path(self):
        pass

    @property
    def path(self):
        return self._path
    @path.setter
    def path(self, path):
        self._path = path

class Option(AbstractParameter):
    """ A class for options 
    """
    def __init__(self,
                value,
                *args,
                **kwargs):
        super().__init__(*args, **kwargs)
        self._value = value

    def __str__(self):
        if self.value is None:
            return ""
        elif self._long_name is not None:
            return "%s=%s"%(self._long_name, self.value)
        elif self._name is not None:
            return "%s %s"%(self._name, self.value)
        else:
            raise ValueError("Options require a name")

    @property
    def value(self):
        return self._value
    @value.setter
    def value(self, value):
        self._value = value

class Flag(AbstractParameter):
    def __init__(self,
                is_set,
                *args,
                **kwargs):
        super().__init__(*args, **kwargs)
        self._is_set = is_set
        self.has_name = True

    def __str__(self):
        if self.is_set:
            if self._long_name is not None:
                return "%s"%(self._long_name)
            elif self._name is not None:
                return "%s"%(self._name)
            else:
                raise ValueError("Flags require a name.")
        else:
            return ""

    @property
    def is_set(self):
        return self._is_set
    @is_set.setter
    def is_set(self, is_set):
        self._is_set = is_set



###--------------------------------------------------------------------------------------------
## Custom exception
##

class ExecutableNotFound(Exception):
    def __init__(self, msg):
        self._msg = msg
        return(self)
    def __str__(self):
        return self._msg

class MissingMandatoryArgument(Exception):
    def __init__(self, msg):
        self._msg = msg
        return(self)
    def __str__(self):
        return self._msg

class InvalidArgumentCombination(Exception):
    def __init__(self, msg):
        self._msg = msg
    def __str__(self):
        return self._msg

###-----------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-f', '--file', dest='somefile',  help='Some file', metavar='FILE', required=True)

    opts = parser.parse_args()

if (__name__ == "__main__"):
	main()
