#!/usr/bin/python3.6
## Last modified: jeu 02 mar 2017 19:03:20 CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json

from .abstract_command import AbstractCommand, FileParam, Flag, Option
from .samtools import SamtoolsDict, SamtoolsView

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"


