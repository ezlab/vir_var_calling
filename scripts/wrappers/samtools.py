#!/usr/bin/python3.6
## Last modified: jeu 02 mar 2017 18:33:35 CET
""" Intended as a toy example. PySAM actually is a better wrapper for samtools.
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json

import pysam

from wrappers.abstract_command import AbstractCommand, FileParam, Flag, Option
from wrappers.abstract_command import MissingMandatoryArgument, InvalidArgumentCombination

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

class SamtoolsCommand(AbstractCommand):
    """ Generic samtools command. Should not be used directly!
        The different samtools command are implemented as subclasses.
    """
    def __init__(self,
                 command,
                 my_input,
                 my_output,
                 exec_name="samtools",
                 *args,
                 **kwargs):
        super().__init__(exec_name)
        self._input = my_input
        self._output = my_output
        self._command = command
        self._params = {}
        ## Set all arguments as attributes
        for key, value in kwargs.items():
            if key in ["input", "output"]:
                setattr(self, key, value)
            else:
                self._params[key] = value

    def __call__(self, my_input, output):
        """ Upon calling the instance, apply the same command with the same parameters.
        """
        self.input = my_input
        self.output = output
        self.execute()

    def __str__(self):
        """ This is the default samtools layout command. Any variation are implemented
            in subclasses.
        """
        cmd = "%s %s %s %s %s"%(self._exec_path,
                             self.command,
                             " ".join([str(arg) for arg in self.params.values()]),
                             self.input,
                             self.output)
        return cmd

    @property
    def command(self):
        return self._command
    @command.setter
    def command(self, command):
        self._command = command

    @property
    def params(self):
        return self._params
    @params.setter
    def params(self, params):
        self._params = params

    @property
    def input(self):
        return self._input
    @input.setter
    def input(self, path):
        self._input.path = path

    @property
    def output(self):
        return self._output
    @output.setter
    def output(self, path):
        self._output.path = path

class SamtoolsDict(SamtoolsCommand):
    def __init__(self,
            my_input,
            my_output,
            assembly=None,
            no_header=False,
            species=None):
        my_input = FileParam(my_input, long_name="--output", is_required=True)
        my_output = FileParam(my_output, name="-o", is_required=True)
        params = {
                "assembly":FileParam(path=assembly, name="-a", long_name="--assembly"),
                "no_header":Flag(is_set=no_header, name="-H", long_name="--no-header")
                }
        super().__init__("dict", my_input, my_output, **params)

##TODO: implement format check
##TODO: implement default behaviour (bam<->sam conversion, print header,...)
class SamtoolsView(SamtoolsCommand):
    def __init__(self,
               my_input,
               my_output,
               reference_file=None,
               threads=None,
               out_bam=False,
               out_cram=False,
               out_fmt=None,
               fast_compression=False,
               uncompress=False,
               header=False,
               header_only=False):
        my_input = FileParam(my_input, is_required=True)
        my_output = FileParam(my_output, name="-o", is_required=True)
        params = {
                "reference_file":FileParam(reference_file, name="-T", long_name="--reference"),
                "threads":Option(threads, name="-@", long_name="--threads"),
                "out_bam":Flag(out_bam, name="-b"),
                "out_cram":Flag(out_cram, name="-C"),
                "out_fmt":Option(out_fmt, name="-O", long_name="--output-fmt"),
                "fast_compression":Flag(fast_compression, name="-1"),
                "uncompress":Flag(uncompress, name="-u"),
                "header":Flag(header, name="-h"),
                "header_only":Flag(header_only, name="-H")
                }
        params = self._check_params(params)
        super().__init__("view", my_input, my_output, **params)

    def _check_params(self, params):
        ## Define valid options for parameters
        ## TODO: Fix :
        ## TypeError: __init__() should return None, not 'MissingMandatoryArgument'
        _OUT_FMT = ["SAM", "BAM", "CRAM"]
        if params["fast_compression"].is_set or params["uncompress"].is_set:
            params["out_bam"].is_set = True
        if (params["out_cram"].is_set and
            params["reference_file"].value is None):
            raise InvalidArgumentCombination("A reference should be provided for CRAM conversion.")
        if (params["out_fmt"].value is not None and
                params["out_fmt"].value not in _OUT_FMT):
            raise MissingMandatoryArgument(
                    "%s is not a valid option for --output-format."%(params["out_fmt"].value))
        return params

class SamtoolsSort(SamtoolsCommand):
    """ Concrete implementation of samtools sort, which sort an alignment."""
    def __init__(self,
               my_input,
               my_output,
               threads=None,
               memory=None,
               out_fmt=None,
               compression_lvl=None,
               sort_by_name=False,
               prefix=None):
        my_input = FileParam(my_input, is_required=True)
        my_output = FileParam(my_output, name="-o", is_required=True)
        params = {
                "threads":Option(value=threads, name="-@", long_name="--threads"),
                "out_fmt":Option(value=out_fmt, name="-O", long_name="--output-fmt"),
                "memory":Option(value=memory, name="-m"),
                "compression_lvl":Option(value=compression_lvl, name="-l"),
                "sort_by_name":Flag(value=sort_by_name, name="-n"),
                "prefix":Option(value=prefix, name="-T")
                }
        params = self._check_params(params)
        super().__init__("sort", my_input, my_output, **params)

##TODO: samtools mpileup accepts multiple bam files.
class SamtoolsMpileup(SamtoolsCommand):
    def __init__(self,
                 my_input,
                 my_output,
                 illumina13=False,
                 count_orphans=False,
                 bam_list=None,
                 no_BAQ=False,
                 adjust_MQ=None,
                 max_depth=None,
                 redo_BAQ=False,
                 fasta_ref=None,
                 exclude_RG=None,
                 positions=None,
                 min_MQ=None,
                 min_BQ=None,
                 region=None,
                 ignore_RG=False,
                 incl_flags=None,
                 excl_flags=None,
                 ignore_overlaps=False,
                 out_bcf=False,
                 out_vcf=False,
                 output_BP=False,
                 output_MQ=False,
                 output_tags=None,
                 uncompressed=False,
                 ext_prob=None,
                 gap_fract=None,
                 tandem_qual=None,
                 skip_indels=False,
                 max_idepth=None,
                 open_prob=None,
                 per_sample_mF=False,
                 platform=None):
        my_input = FileParam(my_input, is_required=True)
        my_output = FileParam(my_output, name="-o", long_name="--output")
        params = {
                ## Input options
                "illumina13":Flag(illumina13, name="-6", long_name="--illumina1.3+"),
                "count_orphans":Flag(count_orphans, name="-A", long_name="--count-orphans"),
                "bam_list":FileParam(bam_list, name="-b", long_name="--bam-list"),
                "no_BAQ":Flag(no_BAQ, name="-B", long_name="--no-BAQ"),
                "adjust_MQ":Option(adjust_MQ, name="-C", long_name="--adjust-MQ"),
                "max_depth":Option(max_depth, name="-d", long_name="--max-depth"),
                "redo_BAQ":Flag(redo_BAQ, name="-E", long_name="--redo-BAQ"),
                "fasta_ref":FileParam(fasta_ref, name="-f", long_name="--fasta-ref"),
                "exclude_RG":FileParam(exclude_RG, name="-G", long_name="--exclude-RG"),
                "positions":FileParam(positions, name="-l", long_name="--positions"),
                "min_MQ":Option(min_MQ, name="-q", long_name="--min-MQ"),
                "min_BQ":Option(min_BQ, name="-Q", long_name="--min_BQ"),
                "region":Option(region, name="-r", long_name="--region"),
                "ignore_RG":Flag(ignore_RG, name="-R", long_name="--ignore-RG"),
                "incl_flags":Option(incl_flags, name="-rf", long_name="--incl-flags"),
                "excl_flags":Option(excl_flags, name="-ff", long_name="--excl_flags"),
                "ignore_overlaps":Flag(ignore_overlaps, name="-x", long_name="--ignore_overlaps"),
                ## output options
                "out_bcf":Flag(out_bcf, name="-b"),
                "out_vcf":Flag(out_vcf, name="-v"),
                ## Output options not working with -g or -v
                "output_BP":Flag(output_BP, name="-O", long_name="--output-BP"),
                "output_MQ":Flag(output_MQ, name="-s", long_name="--output-MQ"),
                ## Output option working only when -g or -v is set
                "output_tags":Option(output_tags, name="-t", long_name="--output-tags")
                }


###-----------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-f', '--file', dest='somefile',  help='Some file', metavar='FILE', required=False)

    opts = parser.parse_args()

if (__name__ == "__main__"):
	main()
