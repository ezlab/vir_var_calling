#!/usr/bin/env python
## Last modified: jeu 02 mar 2017 15:42:28 CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json

from wrappers import SamtoolsDict, SamtoolsView, FileParam, Option, Flag

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

###-----------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-f', '--file', dest='somefile',  help='Some file', metavar='FILE', required=False)

    opts = parser.parse_args()

    cmd = SamtoolsView(my_input="10042.KC207814.bwa_mem.sorted.dedup.realigned.indelqual.bam", my_output="test.sam", header=True)
    print(cmd)
    cmd.execute()

if (__name__ == "__main__"):
	main()
