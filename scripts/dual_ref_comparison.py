#!/usr/bin/env python

"""
-- aa_variants_matrix.py is part of the VVC pipeline

This tools wrap some operations on an allele frequency matrices.

"""

import sys
import argparse
import re
import collections
import operator
import json
import textwrap
import Bio
import logging
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC, generic_rna
import vcf
from variants_matrix import VariantIndex, VariantList, VariantMatrix, compare_calls

__author__  = "Alexis Loetscher"
__date__    = "June 2016"
__copyright__ = ""

__status__ = "Development"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

_Observation = collections.namedtuple("_Observation", ["sample", "aa_variant"])

# SNPeff keywords
_snpeff_impact = ["HIGH", "MODERATE", "LOW", "MODIFIER"]
_snpeff_effect = ["coding_sequence_variant", "chromosome", "coding_sequence_variant",
                  "inframe_insertion", "disruptive_inframe_insertion", "inframe_deletion",
                  "disruptive_inframe_deletion", "downstream_gene_variant", "exon_variant",
                  "exon_loss_variant", "frameshift_variant", "gene_variant",
                  "intergenic_region", "conserved_intergenic_variant", "intragenic_variant",
                  "intron_variant", "conserved_intron_variant", "miRNA", "missense_variant",
                  "initiator_codon_variant", "stop_retained_variant", "rare_amino_acid_variant",
                  "splice_acceptor_variant", "splice_donor_variant", "splice_region_variant",
                  "stop_lost", "5_prime_UTR_premature start_codon_gain_variant", "start_lost",
                  "stop_gained", "synonymous_variant", "start_retained", "stop_retained_variant",
                  "transcript_variant", "regulatory_region_variant", "upstream_gene_variant",
                  "3_prime_UTR_variant", "3_prime_UTR_truncation", "5_prime_UTR_variant",
                  "5_prime_UTR_truncation", "sequence_feature", "non_coding_exon_variant",
                  "non_coding_transcript_variant", "5_prime_UTR_premature_start_codon_gain_variant"]
_snpeff_warning = ["WARNING_REF_DOES_NOT_MATCH_GENOME", "WARNING_SEQUENCE_NOT_AVAILABLE",
                   "WARNING_TRANSCRIPT_INCOMPLETE", "WARNING_TRANSCRIPT_MULTIPLE_STOP_CODONS",
                   "WARNING_TRANSCRIPT_NO_START_CODON", "ERROR_CHROMOSOME_NOT_FOUND",
                   "ERROR_OUT_OF_CHROMOSOME_RANGE"]


# Default filters
_default_filter = {"pos":[[1,"end"]],
                   "qual":0,
                   "DP":0,
                   "AF":0,
                   "TVOTES":0,
                   "SB":1000000,
                   "impact":["HIGH", "MODERATE", "LOW"],
                   "effect":_snpeff_effect,
                   "warnings":_snpeff_warning
        }

# Symbols
_gene_sep = ":"


def extract_info_hgvs_tag(tag):
    """ Extract info from HGVS tag
    """
    gene = re.sub(r":.*$", "", tag)
    hgvs_tag = re.sub(r"^.*:", "", tag)
    pos = re.sub(r"[^0-9]+", "", hgvs_tag)
    ref = re.findall(r"[A-Z]{1}[a-z]{2}", hgvs_tag)[0]
    alt = re.findall(r"[A-Z]{1}[a-z]{2}", hgvs_tag)[1]
    return gene, pos, ref, alt

def make_combined_var_database(var_list):
    """
    Make a list of variant formatted as such:
    <gene>:<pos>:<ref>:<alt>
    """
    combined_var_database = []
    ## Regex to match a protein snp
    aa_snp = re.compile(r"[a-zA-Z0-9]+:p\.[A-Z]{1}[a-z]{2}[0-9]+[A-Z]{1}[a-z]{2}")
    for var in var_list:
        if not re.match(aa_snp, var):
            continue
        gene, pos, ref, alt = extract_info_hgvs_tag(var)
        combined_var_database.append((gene, pos, ref, alt))
    return combined_var_database

def get_gene_name(aa_variant):
    """ Get gene name from aa_variant ID
    """
    return re.split("%s"%_gene_sep, aa_variant)[0]

def get_ref_name(fasta_file_name):
    """ Get reference name
    """
    ref_name = re.sub(r"^.+/", "", fasta_file_name)
    ref_name = re.split("\.", ref_name)
    return ref_name[0]

def get_sample_id(fname):
    """ Get sample id from file name.
    """
    fname = re.sub(r"^.+\/", "", fname)
    fname = re.sub(r"\..+$", "", fname)
    return fname


##---------------------------------------------------------------------------
# Main
def main():
    parser = argparse.ArgumentParser(description="This tool extract amino acid variants from a snpEff annotated VCF file.")

    parser.add_argument('-v', '--vcffiles', dest='vcffiles',
                        help='List of VCF file', metavar='FILE', required=True, nargs="+")
    parser.add_argument('-v2', '--vcffiles2', dest='vcffiles2',
                        help='List of VCF file', metavar='FILE', required=True, nargs="+")
    parser.add_argument('-m', '--multivcffile', dest='mvcffile',
                        help='Multi-sample VCF file', metavar='FILE', required=False)
    parser.add_argument('-m2', '--multivcffiles2', dest='mvcffile2',
                        help='Multi-sample VCF file', metavar='FILE', required=False)
    parser.add_argument('-c', '--covfiles', dest='covfiles',
                        help='List of COV file', metavar='FILE', required=False, nargs="+")
    parser.add_argument('-c2', '--covfiles2', dest='covfiles2',
                        help='List of COV file', metavar='FILE', required=False, nargs="+")
    parser.add_argument('-r', '--gbreference', dest='gbreference',
                        help='reference', metavar='FILE', required=True)
    parser.add_argument('-r2', '--gbreference2', dest='gbreference2',
                        help='reference', metavar='FILE', required=True)
    parser.add_argument('-f', '--filters', dest='filters',
                        help='Json file containing filters', metavar='FILE', required=False)
    parser.add_argument('-b', '--sampleblacklist', dest='sampleblacklist',
                        help='IDs of blacklisted samples', metavar='FILE', required=False)
    parser.add_argument('-o', '--outputfolder', dest='output',
                        help='Were to store all output', required=True)
    parser.add_argument('-cm', '--coord_map', dest='coord_map',
                        help='Were to store all output', required=False)

    options = parser.parse_args()

    ## Read filters, take default if not set
    if options.filters:
        with open("%s"%(options.filters), "r") as JSON:
            filters = json.load(JSON)
    else:
        filters = _default_filter

    ## Read the blacklist of samples
    blacklist = []
    if options.sampleblacklist:
        with open("%s"%options.sampleblacklist, "r") as BLACKLIST:
            for rawline in BLACKLIST.readlines():
                if (rawline == ""
                        or rawline[0] == "#"):
                    continue
                line = rawline.rstrip('\n').strip()
                sline = re.split(r",|\t|\s", line)
                blacklist = blacklist + sline

    ## Read the coordinate maps between ref1 and ref2
    coord_map = {}
    if options.coord_map:
        with open("%s"%(options.coord_map), "r") as MAP:
            json_map = json.load(MAP)
            for protein in json_map:
                coord_map[protein] = dict(zip([x[0] for x in json_map[protein]],
                                              [x[1] for x in json_map[protein]]))

    ## Create the aa_variant matrix objects
    variant_matrix = VariantMatrix(filters, options.gbreference, blacklist)
    ## Create an other matrix, different reference
    variant_matrix2 = VariantMatrix(filters, options.gbreference2, blacklist)

    ## Get reference tag #1
    ref_name1 = get_ref_name(options.gbreference)
    ref_name2 = get_ref_name(options.gbreference2)

    ###----------------------------------
    ## FILLING MATRICES
    ##

    logger.info("Reading VCF files for strain: %s"%(ref_name1))
    ## Fill up the matrix with a list of VCF files
    for vfile in options.vcffiles:
        sample = get_sample_id(vfile)
        variant_matrix.add_from_vcf(vfile, sample=sample)

    logger.info("Reading VCF files for strain: %s"%(ref_name2))
    ## Fill up a second matrix (for comparison)
    for vfile in options.vcffiles2:
        sample = get_sample_id(vfile)
        variant_matrix2.add_from_vcf(vfile, sample=sample)

    ###----------------------------------
    ## CHECKING COVERAGE
    ##

    logger.info("Reading coverage files for strain: %s"%(ref_name1))
    ## Using coverage files, check if the non variants are due to lack of depth
    if options.covfiles:
        for cfile in options.covfiles:
            sample = get_sample_id(cfile)
            variant_matrix.check_coverage(cfile, sample=sample, threshold=6)

    logger.info("Reading coverage files for strain: %s"%(ref_name2))
    ## Using coverage files, check if the non variants are due to lack of depth
    if options.covfiles2:
        for cfile in options.covfiles2:
            sample = get_sample_id(cfile)
            variant_matrix2.check_coverage(cfile, sample=sample, threshold=6)

    ###----------------------------------
    ## Write general summaries
    ##

    ## Write summary for matrix 1
    with open("%s/summary_%s.dat"%(options.output, ref_name1), "w") as SUMMATRIX1:
        SUMMATRIX1.write(variant_matrix.print_stats())

    ## Write summary for matrix 2
    with open("%s/summary_%s.dat"%(options.output, ref_name2), "w") as SUMMATRIX2:
        SUMMATRIX2.write(variant_matrix2.print_stats())

    ###----------------------------------
    ## 6 VOTES
    ##
    variant_matrix.filter_maj(6)
    variant_matrix2.filter_maj(6)

    ###----------------------------------
    ## COMPARING CALLSETS
    ##

    logger.info("Compare call sets")
    ## Compare the two callsets
    ref_comp = "%s/refcomp_%s_vs_%s.dat"%(options.output, ref_name1, ref_name2)
    compare_calls(variant_matrix, variant_matrix2, ref_name1, ref_name2, coord_map, file_path=ref_comp)


    logger.info("")
    ## 
    matrix1 = "%s/%s_gene_corr.dat"%(options.output, ref_name1)
    matrix2 = "%s/%s_gene_corr.dat"%(options.output, ref_name2)
    variant_matrix.make_smpl2gene_cov_mat(file_path=matrix1)
    variant_matrix2.make_smpl2gene_cov_mat(file_path=matrix2)

    ###----------------------------------
    ## VARIANT MATRICES
    ##
    logger.info("")
    for info in ["binary", "AF"]:
        logger.info("Print amino acid variant matrix: %s"%(info))
        ## Print amino acid variants
        matrix1 = "%s/%s_aa_variant_matrix.%s.dat"%(options.output, ref_name1, info)
        matrix2 = "%s/%s_aa_variant_matrix.%s.dat"%(options.output, ref_name2, info)
        variant_matrix.make_smpl2aa_var_mat(info, filtered=True, file_path=matrix1)
        variant_matrix2.make_smpl2aa_var_mat(info, filtered=True, file_path=matrix2)

        logger.info("Print non synonymous amino acid variant matrix: %s"%(info))
        ## Print non synonymous amino acid variants
        matrix1 = "%s/%s_aa_variant_matrix.non_synonymous.%s.dat"%(options.output, ref_name1, info)
        matrix2 = "%s/%s_aa_variant_matrix.non_synonymous.%s.dat"%(options.output, ref_name2, info)
        variant_matrix.make_smpl2aa_var_mat(info, filtered=True,
                                            non_synonymous=True, file_path=matrix1)
        variant_matrix2.make_smpl2aa_var_mat(info, filtered=True,
                                             non_synonymous=True, file_path=matrix2)

        logger.info("Print genomic variant matrix: %s"%(info))
        ## Print genomic variants
        matrix1 = "%s/%s_gen_variant_matrix.%s.dat"%(options.output, ref_name1, info)
        matrix2 = "%s/%s_gen_variant_matrix.%s.dat"%(options.output, ref_name2, info)
        variant_matrix.make_smpl2gen_var_mat(info, filtered=True, file_path=matrix1)
        variant_matrix2.make_smpl2gen_var_mat(info, filtered=True, file_path=matrix2)


    for n_samp in [0, 5, 1]:
        ## Filter by number of samples
        variant_matrix.filter_n_samples(n_samp, invert=True)
        variant_matrix2.filter_n_samples(n_samp, invert=True)

        ###----------------------------------
        ## MAKING SLIDING WINDOW
        ##
        logger.info("Make sliding window")
        ##
        sw1 = "%s/%s_SW.min%ssamp.dat"%(options.output, ref_name1, n_samp)
        sw2 = "%s/%s_SW.min%ssamp.dat"%(options.output, ref_name2, n_samp)
        variant_matrix.print_pos_sliding_window(100, 1000, filtered=True, file_path=sw1)
        variant_matrix2.print_pos_sliding_window(100, 1000, filtered=True, file_path=sw2)

        logger.info("Make sliding window per sample")
        ##
        sw1 = "%s/%s_SW_per_sample.min%ssamp.dat"%(options.output, ref_name1, n_samp)
        sw2 = "%s/%s_SW_per_sample.min%ssamp.dat"%(options.output, ref_name2, n_samp)
        variant_matrix.print_pos_sliding_window_per_sample(100, 1000, filtered=True, file_path=sw1)
        variant_matrix2.print_pos_sliding_window_per_sample(100, 1000, filtered=True, file_path=sw2)
        ###----------------------------------
        ## FULL DATASET VARIANTS TO SAMPLES
        ##

        logger.info("Make amino acid variant list")
        ## Print amino acid variants histogram
        list1 = "%s/%s_aa_variant_list.min%ssamp.dat"%(options.output, ref_name1, n_samp)
        list2 = "%s/%s_aa_variant_list.min%ssamp.dat"%(options.output, ref_name2, n_samp)
        variant_matrix.print_aa_var2smpl_list(file_path=list1, filtered=True)
        variant_matrix2.print_aa_var2smpl_list(file_path=list2, filtered=True)

        logger.info("Make synonymous amino acid variant list")
        ## Print amino acid variants histogram
        list1 = "%s/%s_aa_variant_list.min%ssamp.non_synonymous.dat"%(options.output, ref_name1,
                                                                      n_samp)
        list2 = "%s/%s_aa_variant_list.min%ssamp.non_synonymous.dat"%(options.output, ref_name2,
                                                                      n_samp)
        variant_matrix.print_aa_var2smpl_list(file_path=list1, filtered=True, non_synonymous=True)
        variant_matrix2.print_aa_var2smpl_list(file_path=list2, filtered=True, non_synonymous=True)

        logger.info("Make genomic variant list")
        ## Print amino acid variants histogram
        list1 = "%s/%s_gen_variant_list.min%ssamp.dat"%(options.output, ref_name1, n_samp)
        list2 = "%s/%s_gen_variant_list.min%ssamp.dat"%(options.output, ref_name2, n_samp)
        variant_matrix.print_gen_var2smpl_list(file_path=list1, filtered=True)
        variant_matrix2.print_gen_var2smpl_list(file_path=list2, filtered=True)

        logger.info("Make sample list for amino acid variants")
        ## Print sample histogram (amino acid variants)
        list1 = "%s/%s_aa_sample_list.min%ssamp.dat"%(options.output, ref_name1, n_samp)
        list2 = "%s/%s_aa_sample_list.min%ssamp.dat"%(options.output, ref_name2, n_samp)
        variant_matrix.print_smpl2aa_var_list(file_path=list1, filtered=True)
        variant_matrix2.print_smpl2aa_var_list(file_path=list2, filtered=True)

        logger.info("Make sample list for amino acid variants")
        ## Print sample histogram (amino acid variants)
        list1 = "%s/%s_aa_sample_list.min%ssamp.non_synonymous.dat"%(options.output, ref_name1,
                                                                     n_samp)
        list2 = "%s/%s_aa_sample_list.min%ssamp.non_synonymous.dat"%(options.output, ref_name2,
                                                                     n_samp)
        variant_matrix.print_smpl2aa_var_list(file_path=list1, filtered=True, non_synonymous=True)
        variant_matrix2.print_smpl2aa_var_list(file_path=list2, filtered=True, non_synonymous=True)

        logger.info("Make sample list for genomic variants")
        ## Print sample histogram (genomic variants)
        list1 = "%s/%s_gen_sample_list.min%ssamp.dat"%(options.output, ref_name1, n_samp)
        list2 = "%s/%s_gen_sample_list.min%ssamp.dat"%(options.output, ref_name2, n_samp)
        variant_matrix.print_smpl2gen_var_list(file_path=list1, filtered=True)
        variant_matrix2.print_smpl2gen_var_list(file_path=list2, filtered=True)

        ###----------------------------------
        ## Collapsed gene matrices
        ##

        logger.info("Make collapsed gene matrices - binary")
        ## Print collapsed gene matrix
        genemat1 = "%s/%s_gene_matrix.min%ssamp.binary.dat"%(options.output, ref_name1, n_samp)
        genemat2 = "%s/%s_gene_matrix.min%ssamp.binary.dat"%(options.output, ref_name2, n_samp)
        variant_matrix.make_smpl2gene_mat(file_path=genemat1, filtered=True, binary=True)
        variant_matrix.make_smpl2gene_mat(file_path=genemat2, filtered=True, binary=True)

        logger.info("Make collapsed gene matrices - counts")
        ## Print collapsed gene matrix
        genemat1 = "%s/%s_gene_matrix.min%ssamp.counts.dat"%(options.output, ref_name1, n_samp)
        genemat2 = "%s/%s_gene_matrix.min%ssamp.counts.dat"%(options.output, ref_name2, n_samp)
        variant_matrix.make_smpl2gene_mat(file_path=genemat1, filtered=True)
        variant_matrix.make_smpl2gene_mat(file_path=genemat2, filtered=True)

        logger.info("Make collapsed gene matrices - corrected counts")
        ## Print collapsed gene matrix
        genemat1 = "%s/%s_gene_matrix.min%ssamp.corr_counts.dat"%(options.output, ref_name1,
                                                                  n_samp)
        genemat2 = "%s/%s_gene_matrix.min%ssamp.corr_counts.dat"%(options.output, ref_name2,
                                                                  n_samp)
        variant_matrix.make_smpl2gene_mat(file_path=genemat1, filtered=True, corrected=True)
        variant_matrix.make_smpl2gene_mat(file_path=genemat2, filtered=True, corrected=True)

        logger.info("Make non synonymous collapsed gene matrices - binary")
        ## Print collapsed gene matrix
        genemat1 = "%s/%s_gene_matrix.min%ssamp.non_synonymous.binary.dat"%(options.output,
                                                                            ref_name1, n_samp)
        genemat2 = "%s/%s_gene_matrix.min%ssamp.non_synonymous.binary.dat"%(options.output,
                                                                            ref_name2, n_samp)
        variant_matrix.make_smpl2gene_mat(file_path=genemat1, filtered=True, binary=True,
                                          non_synonymous=True)
        variant_matrix.make_smpl2gene_mat(file_path=genemat2, filtered=True, binary=True,
                                          non_synonymous=True)

        logger.info("Make non synonymous collapsed gene matrices - counts")
        ## Print collapsed gene matrix
        genemat1 = "%s/%s_gene_matrix.min%ssamp.non_synonymous.counts.dat"%(options.output,
                                                                            ref_name1, n_samp)
        genemat2 = "%s/%s_gene_matrix.min%ssamp.non_synonymous.counts.dat"%(options.output,
                                                                            ref_name2, n_samp)
        variant_matrix.make_smpl2gene_mat(file_path=genemat1, filtered=True, non_synonymous=True)
        variant_matrix.make_smpl2gene_mat(file_path=genemat2, filtered=True, non_synonymous=True)

        logger.info("Make non synonymous collapsed gene matrices - corrected counts")
        ## Print collapsed gene matrix
        genemat1 = "%s/%s_gene_matrix.min%ssamp.non_synonymous.corr_counts.dat"%(options.output,
                                                                                 ref_name1, n_samp)
        genemat2 = "%s/%s_gene_matrix.min%ssamp.non_synonymous.corr_counts.dat"%(options.output,
                                                                                 ref_name2, n_samp)
        variant_matrix.make_smpl2gene_mat(file_path=genemat1, filtered=True, corrected=True,
                                          non_synonymous=True)
        variant_matrix.make_smpl2gene_mat(file_path=genemat2, filtered=True, corrected=True,
                                          non_synonymous=True)


if __name__ == "__main__":
    main()
