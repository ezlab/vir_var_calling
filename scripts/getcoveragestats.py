#!/usr/bin/env python
##Last modified: Mon 16 Jan 2017 11:10:55 AM CET

###
## This tools calculates and extract stats about coverage. 
## It also can get the sequence of unusually highly covered regions if
## the genbank file of the reference is provided.
##

import time


import os
import sys
import argparse
import re
import collections
import json
import Bio
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.Alphabet import generic_rna 
import vcf


import pandas


class HistCov():
    def __init__(self, HIST, DATA, ref = "undefined", sample = "undefined", n_reads=None):
        self._ref = ref
        self._sample = sample
        self._chromlist = []
        self._nvircopies = 0
        self._ntotreads = n_reads if n_reads else 0
        self._nmappedreads = 0
        self._nsingletonreads = 0
        self._fract_mapped = 0
        self._length = 0
        self._mean_depth = 0
        self._depth10X = 0

        ## Read the files
        self.read_hist(HIST)
        self.read_data(DATA)

    def __str__(self):
        strout = "%s\t%s\t%s\t%s\t%s\t%.3f\t%.3f\t%.3f"%(
            self._ref,
            self._sample,
            self._nvircopies,
            self._ntotreads,
            self._nmappedreads,
            self._fract_mapped,
            self._mean_depth,
            self._depth10X
            )
        return strout

    def read_hist(self, HIST):
        """
        Parse a coverage histogram file
        """
        self._hist = pandas.read_csv(HIST, delimiter="\t", header=None)
        self._hist.columns = ["chrom", "pos", "cov"]

        self._length = self._hist.shape[0]

    def read_data(self, DATA):
        """
        Parse the output of the samtools flagstat command
        """
        for rawline in DATA.readlines():
            line = rawline.rstrip("\n").strip()
            if line == "":
                continue
            if line[0] == "#":
                continue
            sline = re.split("\s", line)
            if re.search("in total", line):
                self._ntotreads = sline[0] if self._ntotreads == 0 else self._ntotreads
            elif sline[3] == "mapped":
                self._nmappedreads = sline[0]
            elif re.search("singleton", line):
                self._nsingletonreads = sline[0]
        self._fract_mapped = int(self._nmappedreads)/int(self._ntotreads)

    def read_sample_list(self, SAMPLELIST):
        """
        Parse the sample list. Nothing of interest for the purpose of this script but the number of viral copies.
        In fact, I could make a comparison of my assemblie to theirs.
        TODO: for the moment, it just returns the number of viral copies. I could imagine a more elaborate set of options for this function.
        """
        sampleList = {}
        for rawline in SAMPLELIST.readlines():
            line = rawline.rstrip("\n").strip()
            if line == "":
                continue
            if line[0] == "#":
                continue
            sline = re.split(",", line)
            sampleList[sline[0]] = sline[2]
        if self._sample in sampleList:
            self._nvircopies = sampleList[self._sample]
        return self._nvircopies

    def get_mean_depth(self):
        """
        Get the mean depth of the sample's assembly
        """
        tot_depth = 0
        self._mean_depth = self._hist.loc[:,"cov"].mean()
        return self._mean_depth

    def get_depthX(self, threshold=10):
        """
        Get the fraction of the assembly covered by more than 10 reads.
        """
        self._depth10X = len(self._hist.loc[self._hist.loc[:,"cov"] > threshold, "cov"])/self._length
        return self._depth10X

    ###
    ## Get highly covered regions
    def get_high_cov_intervals():
       pass

##-------------------------------------------
# Main
def main():
    """
    """
    parser = argparse.ArgumentParser(description="I print and summarize stats about coverage")

    parser.add_argument('-c', '--histcoveragefile',  dest='histcoveragefile',
                        help='Histogram of coverage.',
                        metavar='FILE', required=True)
    parser.add_argument('-d', '--datafile', dest='datafile',
                        help='Data about reads or coverage.',
                        metavar='FILE', required=True)
    parser.add_argument('-l', '--samplelistfile', dest='samplelistfile',
                        help='List of sample, with stats about number of viral copies.',
                        metavar='FILE', required=False)
    parser.add_argument('-s', '--sample', dest='sample',
                        help='Sample name', required=False)
    parser.add_argument('-r', '--reference', dest='reference',
                        help='Reference name',
                        metavar='string', required=False)
    parser.add_argument('-g', '--genbankfile', dest='genbankfile',
                        help='Genbank file',
                        metavar='FILE', required=False)
    parser.add_argument('-t', '--threshold', dest='threshold',
                        help='Threshold for high coverage regions',
                        default=500, type=int, required=False)
    parser.add_argument('-n', '--n_reads', dest='n_reads',
                        help='Number of reads',
                        type=int, required=False)
    parser.add_argument('-o', '--outfile', dest='outfile',
                        help='Output file',
                        metavar='FILE', required=False)

    opts = parser.parse_args()

    ## Read and parse non-optional files: data and coverage histogramm
    with open(opts.histcoveragefile, "r") as HIST, open("%s"%opts.datafile, "r") as DATA:
        cov = HistCov(HIST, DATA, opts.reference, opts.sample, opts.n_reads)

    ## Read and parse optional file 
    if opts.samplelistfile:
        with open(opts.samplelistfile, "r") as SAMPLELIST:
            cov.read_sample_list(SAMPLELIST)

    ## If given a genbank file
    if opts.genbankfile:
        with open(opts.genbankfile, "r") as GBFILE:
            cov.read_sample_list(GBFILE)

    cov.get_mean_depth()
    cov.get_depthX(opts.threshold)

    if opts.outfile:
        with open(opts.outfile, "w") as OUT:
            OUT.write(cov)

if __name__ == "__main__":
    main()


