#!/usr/bin/env python
## Last modified: Tue 14 Mar 2017 02:17:29 PM CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
import vcf

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

###-----------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-f', '--file', dest='somefile',  help='Some file', metavar='FILE', required=True)

    opts = parser.parse_args()

    try:
        SOMEFILE = open("%s"%(opts.somefile), "r")
        some_vcf = vcf.Container(vcf.Reader(SOMEFILE))
        for record in some_vcf:
            print(record.INFO)
            for sample in some_vcf.samples:
                print(record.get_samp_data(sample))
    except (IOError, FileNotFoundError) as e:
        logging.error("Could not open %s in writing mode"%(opts.somefile), e)
    SOMEFILE.close()

if (__name__ == "__main__"):
	main()
