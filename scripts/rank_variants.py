#!/usr/bin/env python
## Last modified: mar 21 fev 2017 12:05:25 CET
"""
Rank variants called on the same dataset with different tools
"""

from __future__ import print_function
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import time

import yaml
import json
import scipy
from copy import copy
from collections import OrderedDict
from Bio import SeqIO
import itertools
from scipy import stats
import numpy
from itertools import combinations, permutations
from vcf import *
from vcf.model import _Call
import pandas

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

_Observation = collections.namedtuple("Observation", ["caller", "chrom", "pos", "alt"])
_callers = {"freebayes":-1, "lofreq":-1, "samtools":-1, "snver":10000, "varscan2":10000, "gatk":-1}
_callers_sorting_field = {"freebayes":"Phred quality score", "lofreq":"Phred quality score",
                          "samtools":"Genotype probability", "snver":"PValue", "varscan2":"Pvalue",
                          "gatk":"Phred quality score"}


def merge_dict(x,y):
    """Merge two dicts"""
    z = x.copy()
    z.update(y)
    return z

def get_closest(found_alt, alt_list):
    """
    Compare two variants at the same position
    """
    for alt in alt_list:
        if (re.search(r"%s"%alt, found_alt) or
            re.search(r"%s"%found_alt, alt)):
            if len(found_alt) < len(alt):
                return found_alt
            else:
                return alt
    return found_alt

def make_venn_data(vcffile):
    bin_numbers = [x for x in itertools.product([0,1], repeat=len(_callers))][1:]
    bin_numbers = sorted(bin_numbers, reverse=True)
    bin_numbers = sorted(bin_numbers, key=sum)
    bin_numbers = [",".join(str(y) for y in x) for x in bin_numbers]
    venn_data = collections.OrderedDict(zip(bin_numbers, [0]*len(bin_numbers)))


def kendall_tau_dist(rank_a, rank_b):
    """
    Computes the Kendall tau distancei between two ranking.
    Very suboptimal
    """
    tau = 0
    n_objects = len(rank_a)
    for i, j in combinations(range(n_objects), 2):
        tau += (numpy.sign(rank_a[i] - rank_b[j]) == numpy.sign(rank_b[i] - rank_a[j]))
    return tau/(n_objects*(n_objects-1)/2)

def read_reference_pileup(refpileup_path):
    """
    Get data from the reference pileup.
    """
    pass

def get_depth_data(record):
    """
    Compute data related to depth from a samtools pileup in VCF format.
    """
    pass

def annotate_with_ref_pileup(vcffile, annotation):
    """
    Annotate a vcffile (container) using the data in the reference pileup
    """
    pass

def get_minimal_representation(pos, ref, alt):
    """
    Trim overhanging positions in variant representation
    """
    ## Don't remap anything if it's a snv
    if len(ref) == 1 and len(alt) == 1:
        alt = str(alt[0])
        return pos, ref, alt
    else:
        alt = str(alt[0])
        ## Strip right position until different base are found
        while(alt[-1] == ref[-1] and min(len(alt),len(ref)) > 1):
            alt = alt[:-1]
            ref = ref[:-1]
        ## Strip left position until different base are found, increment position
        while(alt[0] == ref[0] and min(len(alt),len(ref)) > 1):
            alt = alt[1:]
            ref = ref[1:]
            pos += 1
        return pos, ref, alt

def get_hgvs_tag(vcf_record):
    """
    Build a hgvs tag from a vcf record
    """
    vcf_record.REF = str(vcf_record.REF)
    hgvs_g = ""
    ## In case of deletion
    if len(vcf_record.REF) > len(vcf_record.ALT[0]):
        if len(vcf_record.ALT[0]) == 1:
            if len(vcf_record.REF)-1 == 1:
                hgvs_g = "g.%sdel%s"%(vcf_record.POS+1, vcf_record.REF[1:])
            else:
                hgvs_g = "g.%s_%sdel%s"%(vcf_record.POS+1, vcf_record.POS+len(vcf_record.REF[1:]),
                                         vcf_record.REF[1:])
        else:
            hgvs_g = "g.%s_%sdel%sins%s"%(vcf_record.POS, vcf_record.POS+len(vcf_record.REF)-1,
                                          vcf_record.REF, vcf_record.ALT[0])
    ## In case of insertion or duplication
    elif len(vcf_record.REF) < len(vcf_record.ALT[0]):
        if vcf_record.ALT[0] == int((len(vcf_record.ALT[0])/len(vcf_record.REF)))*vcf_record.REF:
            if int(len(vcf_record.ALT[0])/len(vcf_record.REF)) == 2:
                hgvs_g = "g.%sdup%s"%(vcf_record.POS, vcf_record.REF)
            else:
                hgvs_g = "g.%s[%s]%s"%(vcf_record.POS, int(len(vcf_record.ALT[0])/len(vcf_record.REF)),
                                       vcf_record.REF)
        ## In case of insertion
        else:
            hgvs_g = "g.%s_%sins%s"%(vcf_record.POS, vcf_record.POS+1, vcf_record.ALT[0][1:])
    ## Point mutation
    elif len(vcf_record.REF) == 1:
        hgvs_g = "g.%s%s>%s"%(vcf_record.POS, vcf_record.REF, vcf_record.ALT[0])
    else:
        if vcf_record.REF[:-1] == vcf_record.ALT[0][:-1]:
            hgvs_g = "g.%s%s>%s"%(vcf_record.POS+len(vcf_record.REF)-1, vcf_record.REF[-1:],
                                  vcf_record.ALT[0][-1:])
        elif vcf_record.REF[:1] == vcf_record.ALT[0][1:]:
            hgvs_g = "g.%s%s>%s"%(vcf_record.POS, vcf_record.REF[:1], vcf_record.ALT[0][:1])
        else:
            hgvs_g = "g.%s_%sdel%sins%s"%(vcf_record.POS, vcf_record.POS+len(vcf_record.REF)-1,
                                          vcf_record.REF, vcf_record.ALT[0])
            print(hgvs_g)
    return hgvs_g

def rank_variants(variant_scores, var_list, sorting_fields):
    ## Sort list of position
    var_list = sorted(var_list)
    print(len(var_list))
    ## Create a dictionary with all variant position, that will contain the sum of ranks
    consensus = collections.OrderedDict(zip(var_list, [0]*len(var_list)))
    ## Create an empty dictionary that will contain the ranks by all methods
    pos2caller = collections.OrderedDict(zip(var_list, [{} for _ in range(len(var_list))] ))
    caller2pos = {}
    raw_scores = {}

    ## Loop over the callers
    for caller in sorting_fields.keys():
        if len(sorting_fields[caller]) == 3:
            reverse = sorting_fields[caller][2]
        else:
            reverse = False
        ranks = collections.OrderedDict(zip(var_list, [len(var_list)]*len(var_list)))
        scores = []
        positions = []
        for observation in variant_scores:
            if observation.caller == caller:
                scores.append(variant_scores[observation])
                positions.append((observation.chrom, observation.pos, observation.alt))
        raw_scores[caller] = sorted(scores)

        ## For these three tools, the higher the score the better
        if not reverse:
            variant_ranks = len(scores)-scipy.stats.rankdata(scores, method="min")+1
        else:
            variant_ranks = scipy.stats.rankdata(scores, method="max")

        for i in range(0,len(variant_ranks), 1):
            ranks[positions[i]] = variant_ranks[i]

        for pos in var_list:
            pos2caller[pos][caller] = ranks[pos]
        caller2pos[caller] = ranks

    ## Calculate quality score for all position and alternative
    for pos in pos2caller:
        values = [rank for rank in pos2caller[pos].values()]
        consensus[pos] = scipy.stats.hmean(values)
    return consensus, pos2caller, caller2pos, raw_scores

def is_indel(vcf_record):
    """
    Check if variant is indel
    """
    pos, ref, alt = get_minimal_representation(vcf_record.POS, vcf_record.REF, vcf_record.ALT)
    return (len(ref) != 1 or len(alt) != 1)

def get_chrom_name_from_fasta(fasta_path):
    contig_list = OrderedDict()
    try:
        FASTA = open("%s"%(fasta_path), "r")
        fasta = SeqIO.parse(FASTA, "fasta")
        for record in fasta:
            contig_list[record.id] = len(record.seq)
    except (IOError, FileNotFoundError) as error:
        logging.error("X ERROR: could not open %s in reading mode."%(fasta_path), error)
    FASTA.close()
    return contig_list


###----------------------------------------------
## MAIN
##

def main():
    """ Main
    """
    parser = argparse.ArgumentParser(description="Rank variants called on the same dataset with different tools")

    parser.add_argument('-p', '--prefix', dest='prefix',
                        help='Prefix of vcf files to be processed',
                        type=str, required=False)
    parser.add_argument('-l', '--vcflist', dest='vcflist', nargs="+",
                        help='List of vcf files to be processed',
                        metavar='FILE', required=False)
    parser.add_argument('-o', '--outfile', dest='outfile',
                        help='Output VCF file',
                        type=str, required=False)
    parser.add_argument('-s', '--sample', dest='sample',
                        help='Sample', 
                        type=str, required=False)
    parser.add_argument('-rps', '--refpileupSNP', dest='refpileup',
                        help='Reference pileup for SNP',
                        type=str, required=False)
    parser.add_argument('-rpi', '--refpileupINDEL', dest='refpileup',
                        help='Reference pileup for INDELs',
                        type=str, required=False)
    parser.add_argument('-r', '--reference', dest='reference',
                        help='Reference', 
                        type=str, required=False)
    parser.add_argument('-sf', '--sorting_fields_file', dest='sorting_fields_file',
                        help='YAML file containing the sorting field for each vcf',
                        metavar='FILE', required=True)
    parser.add_argument('-t', '--statfile', dest='statfile',
                        help='Ranks of each position for each variant called',
                        metavar='FILE', required=False)
    parser.add_argument('-d', '--distances', dest='distances',
                        help='File containing Kendall-tau distance between callers',
                        metavar='File', required=False)
    parser.add_argument('-rs', '--raw_scores', dest='raw_scores',
                        help='Prefix for files with raw scores',
                        type=str, required=False)
    parser.add_argument('-n', '--nvar_per_caller', dest='nvar_per_caller',
                        help='Number of variants per caller',
                        type=str, required=False)
    parser.add_argument('-c', '--chrom', dest='chrom',
                        help='Chromosome field in the new vcf file',
                        type=str, required=False)

    opts = parser.parse_args()

    if not opts.prefix and opts.sample and opts.reference:
        opts.prefix ="%s.%s"%(opts.sample, opts.reference)
    elif opts.prefix and not opts.sample:
        opts.sample = re.split(r"\.", opts.prefix)[0]
        opts.sample = re.sub(r"^.*\/", "", opts.sample)
    elif opts.prefix and opts.reference:
        reference = re.split(r"\\", opts.reference)[-1]
        reference = re.sub(r"^.*\/", "", reference)
    else:
        sys.exit("Please specify a prefix, or a sample and reference.")

    contig_list = get_chrom_name_from_fasta(opts.reference)

    with open(opts.sorting_fields_file, "r") as SF:
        next(SF, None)
        sorting_fields = yaml.load(SF, Loader=yaml.FullLoader)[0]
        sorting_fields = {ll[0]:ll[1] for ll in sorting_fields}
        caller_present = [re.search(r"|".join(sorting_fields.keys()), vcf)[0] for vcf in opts.vcflist]
        sorting_fields = {k:v for k, v in sorting_fields.items() if k in caller_present}

    ## vcf entries:
    ##  > INFO: record.INFO["<what you want>"]
    ##  > FORMAT: record.samples[0].data.<what you want>
    ## Note: There is only one sample per file, then it is always samples[0]


    ## Initiate new vcf file
    new_vcf = Container(fileformat="VCFv4.1",
                        reference="%s"%(opts.reference))
    new_vcf.add_metaline("source", " ".join(sys.argv))
    new_vcf.add_sample(opts.sample, "HHV4", "", "A sample from HHV4 infected patient")
    for caller in caller_present:
        new_vcf.add_sample(f"{opts.sample}_{caller}", f"{opts.sample}", "", f"{opts.sample} called by {caller}")
    ## Info field for predictions of callers
    caller_list = ",".join(sorted(sorting_fields.keys()))
    new_vcf.add_info("tag", 1, "String", "HGVS tag for this variant", None, None)
    new_vcf.add_info("TYPE", 1, "String", "Type of variant: SNP, INDEL", None, None)
    new_vcf.add_info("LEN", 2, "Integer", "Length of insertion/deletion: ins,del", None, None)
    new_vcf.add_info("RANKS", len(sorting_fields.keys()), "Integer", f"Ranking from: {caller_list}", None, None)
    new_vcf.add_info("TVOTES", 1, "Integer", "Total number of callers reporting the variant", None, None)
    new_vcf.add_info("VOTES", len(sorting_fields.keys()), "Integer", f"Variant found by: {caller_list}", None, None)
    new_vcf.add_format("GT", 1, "String", "Genotype")
    new_vcf.add_filter("MAJ", "Majority of votes")
    ## Add all contigs found in vcf file
    for contig in contig_list:
        new_vcf.add_contig(contig, contig_list[contig])

    ## Initiate data structures
    snp_list = [] ## List containing all positions and alternatives
    indel_list = [] ## List containing all positions and alternatives
    pos_list = []
    snp_scores = {} ## Dictionary, taking _Observation as index
    indel_scores = {} ## Dictionary, taking _Observation as index
    formats = set()
    all_calls = {}
    for vcffile_path in opts.vcflist:
        caller = re.search(r"|".join(sorting_fields.keys()), vcffile_path)[0]
        logging.info(caller)
        new_vcf.add_metaline(caller, "Ranking from %s, based on %s"%(caller, sorting_fields[caller]))
        ## Open the vcf file
        with open("%s"%(vcffile_path), "r") as VCF:
            vcffile = Reader(VCF)
            vcfcontainer = Container(reader=vcffile)
            all_calls[caller] = [record for record in vcfcontainer]
        if len(vcfcontainer) > 0 and len(vcfcontainer._format_cache):
            formats = formats | set(list(vcfcontainer._format_cache.keys())[0].split(":"))
        ## Loop through the VCF file
        for record in vcfcontainer:
            ## Create a new record
            pos, ref, alt = get_minimal_representation(record.POS, record.REF, record.ALT)
            chrom = record.CHROM
            record.POS = pos
            record.REF = ref
            record.ALT = [alt]
            ## Add new variant in VCF if a new one is found
            if (chrom, pos, alt) not in snp_list+indel_list:
                tag = get_hgvs_tag(record)
                #chrom = list(contig_list.keys())[0]
                pos_list.append(pos)
                new_info = {"tag":tag}
                ## Check if it is an INDEL
                if is_indel(record):
                    indel_list.append((chrom, pos, alt))
                    new_info["TYPE"] = "indel"
                    new_info["LEN"] = len(alt)-len(ref)
                else:
                    snp_list.append((chrom , pos, alt))
                    new_info["TYPE"] = "snp"
                new_vcf.add_record(chrom, pos, record.ID, ref, alt, "NULL", record.FILTER, new_info, "")
            obs = _Observation(caller, record.CHROM, pos, alt)
            ## Fetch score
            if len(sorting_fields[caller]) == 0:
                score = float(record.QUAL)
            else:
                ## Store sort score into a dict
                if sorting_fields[caller][0] == "info":
                    score = float(record.INFO[sorting_fields[caller][1]][0])
                elif sorting_fields[caller][0] == "format":
                    score = float(record.get_samp_field_data(opts.sample, sorting_fields[caller][1]))

            if is_indel(record):
                indel_scores[obs] = score
            else:
                snp_scores[obs] = score

    indices = [[record.CHROM, record.POS, str(record.ALT[0])] for record in new_vcf]

    formats_fields = pandas.DataFrame(index=pandas.MultiIndex.from_tuples(indices, names=("CHROM", "POS", "ALT")),
                                      columns=list(sorted(sorting_fields.keys())))
    for caller, calls in all_calls.items():
        for record in calls:
            formats_fields.loc[(record.CHROM, record.POS, str(record.ALT[0])),caller] = [record.get_samp_data(opts.sample)]
    formats_fields = formats_fields.fillna("")

    ## Rank SNPs and INDELs separately
    snp_consensus, snp_pos2caller, snp_caller2pos, snp_raw_scores = rank_variants(snp_scores, snp_list, sorting_fields)
    indel_consensus, indel_pos2caller, indel_caller2pos, indel_raw_scores = rank_variants(indel_scores, indel_list, sorting_fields)

    ## Add the variant ranks from each method as info
    for record in new_vcf:
        id_var = (record.CHROM, record.POS, str(record.ALT[0]))
        machin = formats_fields.loc[id_var,:]
        if record.INFO["TYPE"] == "snp":
            nvar = len(snp_pos2caller)
            record.QUAL = snp_consensus[id_var]
            rank_str = ",".join([str(rank[1]) for rank in sorted(snp_pos2caller[id_var].items())])
            tvotes = len([rank for rank in snp_pos2caller[id_var].values() if rank != nvar])
            votes = ",".join([str(int(rank[1] != nvar)) for rank in sorted(snp_pos2caller[id_var].items())])
        elif record.INFO["TYPE"] == "indel":
            nvar = len(indel_pos2caller)
            record.QUAL = indel_consensus[id_var]
            rank_str = ",".join([str(rank[1]) for rank in sorted(indel_pos2caller[id_var].items())])
            tvotes = len([rank for rank in indel_pos2caller[id_var].values() if rank != nvar])
            votes = ",".join([str(int(rank[1] != nvar)) for rank in sorted(indel_pos2caller[id_var].items())])

        record.INFO["RANKS"] = rank_str
        record.INFO["TVOTES"] = tvotes
        record.INFO["VOTES"] = votes
        ## Add the GT fields, since it is required since VCFv4.0
        samp_data_to_add = {"GT":{}, "DP":{}, "AD":{}, "GQ":{}, "PL":{}}
        machin["lofreq"] = ""
        for caller in caller_present:
            ## Genotype field
            if len(machin[caller]) == 0 or type(machin[caller][0]) == None:
                GT = "."
            else:
                GT = machin[caller][0]["GT"]
            ## Read depth
            if len(machin[caller]) == 0 or type(machin[caller][0]) == None:
                DP = "."
            else:
                if "DP" in machin[caller][0]:
                    DP = machin[caller][0]["DP"]
                else:
                    DP = "."
            ## Allelic depth
            if len(machin[caller]) == 0:
                AD = "."
            else:
                if "AD" in machin[caller][0]:
                    AD = machin[caller][0]["AD"]
                else:
                    AD = "."
            ## Genotype quality
            if len(machin[caller]) == 0:
                GQ = "."
            else:
                if "GQ" in machin[caller][0]:
                    GQ = machin[caller][0]["GQ"]
                else:
                    GQ = "."
            ## Phred likelihood score
            if len(machin[caller]) == 0:
                PL = "."
            else:
                if "PL" in machin[caller][0]:
                    PL = machin[caller][0]["PL"]
                else:
                    PL = "."
            samp_data_to_add["GT"][f"{opts.sample}_{caller}"] = GT
            samp_data_to_add["DP"][f"{opts.sample}_{caller}"] = DP
            samp_data_to_add["AD"][f"{opts.sample}_{caller}"] = AD
            samp_data_to_add["GQ"][f"{opts.sample}_{caller}"] = GQ
            samp_data_to_add["PL"][f"{opts.sample}_{caller}"] = PL
        record.add_samp_data(samp_data_to_add)

#        if tvotes >= len(_callers)/2:
        if tvotes == len(sorting_fields):
            record.FILTER = "PASS"
        else:
            record.FILTER = "."

        ## Example of how to add a format to sample
#        record.add_format("GT:PL:GQ")
#        stuff_to_add = {"GT":{"91120":"1"}, "PL":{"91120":1}, "GQ":{"91120":2}}
#        record.add_samp_data(stuff_to_add)
        ## Optionnally, add a _format_cache
#        new_vcf.add_samp_format("GT:PL:GQ")

    new_vcf.sort_pos()

    ## Write the final VCF
    with open(opts.outfile, "w") as OUTVCF:
        vcf_writer = Writer(OUTVCF, new_vcf)
        for record in new_vcf:
            vcf_writer.write_record(record)

    ## TODO: fix the order in these dictionary
    consensus = merge_dict(snp_consensus, indel_consensus)
    pos2caller = merge_dict(snp_pos2caller, indel_pos2caller)
    caller2pos = OrderedDict()
    for caller in sorting_fields.keys():
        caller2pos[caller] = merge_dict(snp_caller2pos[caller], indel_caller2pos[caller])
    variant_scores = merge_dict(snp_scores, indel_scores)
    var_list = snp_list + indel_list

    if opts.nvar_per_caller:
        with open(opts.nvar_per_caller, "w") as NVAR:
            NVAR.write("caller\tSNP\tINDELs\n")
            for caller in sorting_fields.keys():
                n_snp = len([x for x in snp_scores if x.caller == caller])
                n_indel = len([x for x in indel_scores if x.caller == caller])
                NVAR.write("%s\t%s\t%s\n"%(caller, n_snp, n_indel))

    ## Write
    if opts.statfile:
        with open(opts.statfile, "w") as STATS:
            header = "Type\tchrom\tpos\talt\trank_mean\t" + "\t".join(sorted(sorting_fields.keys()))+"\n"
            STATS.write(header)
            for pos in var_list:
                if pos in snp_list:
                    line = "snp\t%s\t%s\t%s\t%s"%(pos[0], pos[1], pos[2], consensus[pos])
                else:
                    line = "indel\t%s\t%s\t%s\t%s"%(pos[0], pos[1], pos[2], consensus[pos])
                for caller in sorted(sorting_fields.keys()):
                    if _Observation(caller, pos[0], pos[1], pos[2]) in variant_scores:
                        is_called = 1
                    else:
                        is_called = 0
                    item = "%s:%s"%(pos2caller[pos][caller],is_called)
                    line += "\t%s"%(item)
                STATS.write("%s\n"%line)

    ## Write the Kendall-tau distances into a file
    if opts.distances:
        with open(opts.distances, "w") as DIST:
            DIST.write("callers\t"+"\t".join(caller2pos.keys()))
            for caller1 in caller2pos:
                line = "%s"%(caller1)
                for caller2 in caller2pos:
                    tau, pval = scipy.stats.kendalltau(list(caller2pos[caller1].values()), list(caller2pos[caller2].values()))
                    corr = "%.3f +- %.2e"%(tau, pval)
                    line += "\t%s"%corr
                    if caller1 == caller2:
                        DIST.write("\n%s"%line)
                        break

    if opts.raw_scores:
        for caller in sorting_fields.keys():
            with open("%s.%s.snp.dat"%(opts.raw_scores, caller), "w") as SNPSCORES:
                SNPSCORES.write("\n".join([str(x) for x in snp_raw_scores[caller]]))
            with open("%s.%s.indel.dat"%(opts.raw_scores, caller), "w") as INDELSCORES:
                INDELSCORES.write("\n".join([str(x) for x in indel_raw_scores[caller]]))

if __name__ == "__main__":
    main()
