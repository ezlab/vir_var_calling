#!/usr/bin/env python
## Last modified: Thu 22 Dec 2016 01:18:24 PM CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
import copy
from vcf import *
from vcf.model import _Record, _Substitution

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()


def get_minimal_representation(pos, ref, alt):
    """
    Trim overhanging positions in variant representation
    """
    ## Don't remap anything if it's a snv
    if len(ref) == 1 and len(alt) == 1: 
        return pos, ref, alt
    else:
        ## Strip end position until different base are found
        while(alt[-1] == ref[-1] and min(len(alt),len(ref)) > 1):
            alt = alt[:-1]
            ref = ref[:-1]
        ## Strip start position until different base are found, increment position
        while(alt[0] == ref[0] and min(len(alt),len(ref)) > 1):
            alt = alt[1:]
            ref = ref[1:]
            pos += 1
        return pos, ref, alt

def dissociate_info_field(field, allele):
    """
    Dissociate a multiallelic info field
    """
    new_field = [field[0], field[allele]]
    return new_field

def dissociate_format_field(record, field, allele):
    """
    Dissociate a multiallelic format field
    Returns a 2D dict with field and samples as keys
    """
    sample_data = {}
    for call in record.samples:
        sample = call.sample
        old_field = record.get_samp_field_data(sample, field)
        sample_data[sample] = [old_field[0], old_field[allele]]
    return sample_data

def get_format_field(record, field):
    """
    Dissociate a multiallelic format field
    Returns a 2D dict with field and samples as keys
    """
    sample_data = {}
    for call in record.samples:
        sample = call.sample
        sample_data[sample] = record.get_samp_field_data(sample, field)
    return sample_data

###-----------------------------------------------------------
## MAIN
##
def main():
    """ Main
    """
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-iv', '--invcffile', dest='invcffile',
                        help='Input vcf file',
                        metavar='FILE', required=True)
    parser.add_argument('-ov', '--outvcffile', dest='outvcffile',
                        help='Input vcf file',
                        metavar='FILE', required=True)
    parser.add_argument( '--discard-invariant', dest='discard_invariant',
                        help='Discard invariant sites',
                        required=False, action="store_true")

    opts = parser.parse_args()

    format_to_dissociate = ["AD", "ADR", "ADF"]
    info_to_dissociate = ["QS"]

    with open(opts.invcffile, "r") as VCF:
        invcffile = Container(reader = Reader(VCF))
        dup_records = []
        for record in invcffile[:]:
            pos = record.POS
            ref = record.REF
            i=0
            for alt in record.ALT:
                ## Sometimes, it is not possible to convert alt to a string.
                try:
                    alt = str(alt)
                    new_pos, new_ref, new_alt = get_minimal_representation(pos, ref, alt)
                except:
                    new_alt = alt
                    pass
                ## Some INDEL are multiallelic. Separate them into two different entries.
                ## CHROMx   333     TCCC    TCC,TC
                if "INDEL" in record.INFO and len(record.ALT) != 1:
                    invcffile.remove(record)
                    ## Dissociate info field affected by multiallelic sites.
                    new_info = copy.copy(record.INFO)
                    for field in info_to_dissociate:
                        if field in record.INFO.keys():
                            new_info[field] = dissociate_info_field(new_info[field], i+1)
                    ## Dissociate format fields affected by multiallelic sites.
                    sample_data = {}
                    for field in record.FORMAT.split(":"):
                        if field in format_to_dissociate:
                            sample_data[field] = dissociate_format_field(record, field, i+1)
                        else:
                            sample_data[field] = get_format_field(record, field)
                    new_record = _Record(record.CHROM, new_pos, record.ID, new_ref,
                                         [_Substitution(new_alt)], record.QUAL, record.FILTER,
                                         new_info, record.FORMAT, invcffile._sample_indexes)
                    new_record.add_samp_data(sample_data)
                    dup_records.append(new_record)
                else:
                    record.ALT[i] = new_alt
                i+=1
            record.POS = new_pos
            record.REF = new_ref
        for record in dup_records:
            invcffile.append(record)
        invcffile.sort_pos()

    dummyvcf = invcffile.copy()

    with open(opts.outvcffile, "w") as VCF:
        outvcffile = Writer(VCF, invcffile)
        for record in invcffile:
            if opts.discard_invariant:
                if len(record.ALT) == 1 and str(record.ALT[0]) == "<*>":
                    continue
            outvcffile.write_record(record)

if (__name__ == "__main__"):
    main()
