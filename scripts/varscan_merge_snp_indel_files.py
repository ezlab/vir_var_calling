#!/usr/bin/env python
## Last modified: Fri 11 Nov 2016 05:58:27 PM CET
"""
SNVer produces two files, containing the SNP and the indels, resprectively.
This script fuse them into one entity.
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
from vcf import Container, Reader, Writer

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

###-----------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-p', '--prefix', dest='prefix',
            help='Prefix of indel and snp vcf files',
            type=str, required=True)
    parser.add_argument('-s', '--sample', dest='sample',
            help='Sample name',
            type=str, required=True)

    opts = parser.parse_args()


    with open(f"{opts.prefix}.snp.vcf") as SNPFILE:
        snpfile = Container(reader=Reader(SNPFILE))
    with open(f"{opts.prefix}.indel.vcf") as INDELFILE:
        indelfile = Container(reader=Reader(INDELFILE))

    snpfile.merge(indelfile)

    snpfile.formats["VD"] = snpfile.formats["AD"]
    del snpfile.formats["AD"]
    snpfile.add_format("AD", "R", "Integer",
            "Allelic depths for the ref and alt alleles in the order listed")
    fmt = ":".join(snpfile.formats.keys())
    snpfile._format_cache[fmt] = snpfile._parse_sample_format(fmt)

    snpfile.samples[0] = opts.sample
    snpfile._sample_indexes = {}
    snpfile._sample_indexes[opts.sample] = 0
    for record in snpfile:
        record._sample_indexes = snpfile._sample_indexes
        record.samples[0].sample = opts.sample
        samp_data = record.get_samp_data(opts.sample)
        samp_data_new = samp_data.copy()
        samp_data_new["VD"] = samp_data["AD"]
        samp_data_new["AD"] = [samp_data["AD"], samp_data["RD"]]
        record.add_samp_data({k:{opts.sample:v} for k, v in samp_data_new.items()})

    with open(f"{opts.prefix}.vcf", "w") as VCF:
        vcf_writer = Writer(VCF, snpfile)
        for record in snpfile:
            vcf_writer.write_record(record)

if (__name__ == "__main__"):
    main()
