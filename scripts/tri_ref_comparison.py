#!/usr/bin/env python

"""
-- aa_variants_matrix.py is part of the VVC pipeline

This tools wrap some operations on an allele frequency matrices.

"""

__author__  = "Alexis Loetscher"
__date__    = "June 2016"
__copyright__ = "" 

__status__ = "Development"

import sys
import argparse
import re
import collections
import operator
import json
import textwrap
import Bio
import logging
import vcf
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC, generic_rna
from variants_matrix import AminoAcidVariantsMatrix

## Set logger
FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

## Index for the VariantsMatrix class
_Observation = collections.namedtuple("_Observation",["sample", "aa_variant"])

## SNPeff keywords
_snpeff_impact = ["HIGH", "MODERATE", "LOW", "MODIFIER"]
_snpeff_effect = ["coding_sequence_variant", "chromosome", "coding_sequence_variant",
                    "inframe_insertion", "disruptive_inframe_insertion", "inframe_deletion",
                    "disruptive_inframe_deletion", "downstream_gene_variant", "exon_variant",
                    "exon_loss_variant", "frameshift_variant", "gene_variant",    
                    "intergenic_region", "conserved_intergenic_variant", "intragenic_variant",
                    "intron_variant", "conserved_intron_variant", "miRNA", "missense_variant",
                    "initiator_codon_variant", "stop_retained_variant", "rare_amino_acid_variant",
                    "splice_acceptor_variant", "splice_donor_variant", "splice_region_variant",
                    "stop_lost", "5_prime_UTR_premature start_codon_gain_variant", "start_lost",
                    "stop_gained", "synonymous_variant", "start_retained", "stop_retained_variant",
                    "transcript_variant", "regulatory_region_variant", "upstream_gene_variant",
                    "3_prime_UTR_variant", "3_prime_UTR_truncation", "5_prime_UTR_variant",
                    "5_prime_UTR_truncation", "sequence_feature", "non_coding_exon_variant",
                    "non_coding_transcript_variant", "5_prime_UTR_premature_start_codon_gain_variant"]
_snpeff_warning = ["WARNING_REF_DOES_NOT_MATCH_GENOME", "WARNING_SEQUENCE_NOT_AVAILABLE",
                    "WARNING_TRANSCRIPT_INCOMPLETE", "WARNING_TRANSCRIPT_MULTIPLE_STOP_CODONS",
                    "WARNING_TRANSCRIPT_NO_START_CODON", "ERROR_CHROMOSOME_NOT_FOUND",
                    "ERROR_OUT_OF_CHROMOSOME_RANGE"]


## Default filters
_default_filter = {
        "pos":[[1,"end"]],
        "qual":0,
        "DP":0,
        "AF":0,
        "SB":1000000,
        "impact":["HIGH", "MODERATE", "LOW"],
        "effect":_snpeff_effect,
        "warnings":_snpeff_warning
        }

## Symbols
_gene_sep = ":"

###---------------------------------------------------
## FUNCTIONS
##

###
## Get gene name from aa_variant ID
def get_gene_name(aa_variant):
    return re.split("%s"%_gene_sep, aa_variant)[0]

###
## Get reference name
def get_ref_name(fasta_file_name):
    ref_name = re.sub(r"^.+/", "", fasta_file_name)
    ref_name = re.split("\.", ref_name)
    return ref_name[0]

###
## Get sample id from file name.
def get_sample_id(fname):
    fname = re.sub(r"^.+\/", "", fname)
    fname = re.sub(r"\..+$", "", fname)
    return fname

###
## Compare called aa_variants
def compare_calls(matrix1, matrix2, matrix3, ref1, ref2, ref3, FILE, prefilter=0):
    ## Get reference name
    ref1 = get_ref_name(ref1)
    ref2 = get_ref_name(ref2)
    ref3 = get_ref_name(ref3)
    ## Print file header
    FILE.write("Gene\tID\t%s\t%s\t%s\t%s_%s\t%s_%s\t%s_%s\t%s_%s_%s\n"%(
        ref1, ref2, ref3, ref1, ref2, ref2, ref3, ref3, ref1, ref1, ref2, ref3
        ))
    ## Make sure those lists are done
    var2smpl1 = matrix1.make_var2smpl_list()
    var2smpl2 = matrix2.make_var2smpl_list()
    var2smpl3 = matrix3.make_var2smpl_list()
    comparison = {}
    all_keys = set(var2smpl1.keys()).union(var2smpl2.keys()).union(var2smpl3.keys())
    for key in all_keys:    
        ## Convert to list
        list1 = var2smpl1[key] if (key in var2smpl1) else []
        list2 = var2smpl2[key] if (key in var2smpl2) else []
        list3 = var2smpl3[key] if (key in var2smpl3) else []
        ## Make intersections
        ref1_inter_ref2 = set(list1).intersection(list2)
        ref2_inter_ref3 = set(list2).intersection(list3)
        ref3_inter_ref1 = set(list3).intersection(list1)
        ref1_inter_ref2_inter_ref3 = ref1_inter_ref2.intersection(ref2_inter_ref3)
        ## Substract
        ref1_inter_ref2 -= set(ref1_inter_ref2_inter_ref3)
        ref2_inter_ref3 -= set(ref1_inter_ref2_inter_ref3)
        ref3_inter_ref1 -= set(ref1_inter_ref2_inter_ref3)
        ## Inverted union
        ref1_only = set(list1) - set(ref1_inter_ref2) - set(ref3_inter_ref1) - set(ref1_inter_ref2_inter_ref3)
        ref2_only = set(list2) - set(ref1_inter_ref2) - set(ref2_inter_ref3) - set(ref1_inter_ref2_inter_ref3)
        ref3_only = set(list3) - set(ref3_inter_ref1) - set(ref2_inter_ref3) - set(ref1_inter_ref2_inter_ref3)
        gene_name = get_gene_name(key)
        if ( len(list1) >= prefilter and  len(list2) >= prefilter ):
            FILE.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n"%(gene_name,key,
                len(ref1_only), len(ref2_only), len(ref3_only), len(ref1_inter_ref2), len(ref2_inter_ref3), len(ref3_inter_ref1),
                len(ref1_inter_ref2_inter_ref3)))

###---------------------------------------------------------------------------
## Main
if (__name__ == "__main__"):
    parser = argparse.ArgumentParser(description="This tool extract amino acid variants from a snpEff annotated VCF file.")
     
    parser.add_argument('-v', '--vcffiles',  dest='vcffiles',     help='List of VCF file',   metavar='FILE', required=True,  nargs="+")
    parser.add_argument('-v2', '--vcffiles2',  dest='vcffiles2',     help='List of VCF file',   metavar='FILE', required=True,  nargs="+")
    parser.add_argument('-v3', '--vcffiles3',  dest='vcffiles3',     help='List of VCF file',   metavar='FILE', required=True,  nargs="+")
    parser.add_argument('-r', '--reference',  dest='reference',     help='reference',   metavar='FILE', required=True)
    parser.add_argument('-r2', '--reference2',  dest='reference2',     help='reference',   metavar='FILE', required=True)
    parser.add_argument('-r3', '--reference3',  dest='reference3',     help='reference',   metavar='FILE', required=True)
    parser.add_argument('-c', '--covfiles',  dest='covfiles',     help='List of COV file',   metavar='FILE', required=False,  nargs="+")
    parser.add_argument('-c2', '--covfiles2',  dest='covfiles2',     help='List of COV file',   metavar='FILE', required=False,  nargs="+")
    parser.add_argument('-c3', '--covfiles3',  dest='covfiles3',     help='List of COV file',   metavar='FILE', required=False,  nargs="+")
    parser.add_argument('-f', '--filters',  dest='filters',     help='Json file containing filters',   metavar='FILE', required=False)
    parser.add_argument('-b', '--sampleblacklist',  dest='sampleblacklist',     help='IDs of blacklisted samples',   metavar='FILE', required=False)
    parser.add_argument('-o', '--outputfolder',  dest='output',     help='Were to store all output', required=True)

    
    options = parser.parse_args()
    
    ## Read filters, take default if not set
    if (options.filters):
        with open("%s"%(options.filters), "r") as JSON:
            filters = json.load(JSON)
    else:
        filters = _default_filter
    
    ## Read the blacklist of samples
    blacklist = []
    if (options.sampleblacklist):
        try:
            BLACKLIST = open("%s"%options.sampleblacklist, "r")
            for rawline in BLACKLIST.readlines():
                if (rawline == ""
                        or rawline[0] == "#"):
                    continue
                line = rawline.rstrip('\n').strip()
                sline = re.split(r",|\t|\s", line)
                blacklist= blacklist + sline
        except (IOError, FileNotFoundError) as e:
            logging.error("X ERROR: could not open %s in reading mode."%vfile)
        BLACKLIST.close()
    
    ## Create the aa_variant matrix objects
    aa_variant_matrix = AminoAcidVariantsMatrix(filters, options.reference, blacklist)  
    ## Create an other matrix, different reference
    aa_variant_matrix2 = AminoAcidVariantsMatrix(filters, options.reference2, blacklist)
    ## Third matrix
    aa_variant_matrix3 = AminoAcidVariantsMatrix(filters, options.reference3, blacklist)

    ## Get reference tag #1
    ref_name1 = get_ref_name(options.reference)
    ref_name2 = get_ref_name(options.reference2)
    ref_name3 = get_ref_name(options.reference3)
   
    ## Fill up the matrix with a list of VCF files
    for vfile in options.vcffiles:
        try:
            sample = get_sample_id(vfile)
            VCFFILE = open("%s"%vfile, "r")
            aa_variant_matrix.add_from_vcf(VCFFILE, sample)
        except (IOError, FileNotFoundError) as e:
            logging.error("X ERROR: could not open %s in reading mode."%vfile)
        VCFFILE.close()
    print(aa_variant_matrix)

    ## Fill up a second matrix (for comparison)
    for vfile in options.vcffiles2:
        try:
            sample = get_sample_id(vfile)
            VCFFILE = open("%s"%vfile, "r")
            aa_variant_matrix2.add_from_vcf(VCFFILE, sample)
        except (IOError, FileNotFoundError) as e:
            logging.error("X ERROR: could not open %s in reading mode."%vfile)
        VCFFILE.close()
    print(aa_variant_matrix2)
    
    ## Fill up a third matrix (for comparison)
    for vfile in options.vcffiles3:
        try:
            sample = get_sample_id(vfile)
            VCFFILE = open("%s"%vfile, "r")
            aa_variant_matrix3.add_from_vcf(VCFFILE, sample)
        except (IOError, FileNotFoundError) as e:
            logging.error("X ERROR: could not open %s in reading mode."%vfile)
        VCFFILE.close()
    print(aa_variant_matrix3)

    ###----------------------------------
    ## CHECKING COVERAGE 
    ##

    ## Using coverage files, check if the non variants are due to lack of depth
    if (options.covfiles):
        for cfile in options.covfiles:
            try:
                sample = get_sample_id(cfile)
                COVFILE = open("%s"%cfile, "r")
                aa_variant_matrix.verify_coverage(COVFILE, sample, 2)
            except (IOError, FileNotFoundError) as e:
                logging.error("X ERROR: could not open %s in reading mode."%cfile)
            COVFILE.close()

    ## Using coverage files, check if the non variants are due to lack of depth
    if (options.covfiles2):
        for cfile in options.covfiles2:
            try:
                sample = get_sample_id(cfile)
                COVFILE = open("%s"%cfile, "r")
                aa_variant_matrix2.verify_coverage(COVFILE, sample, 2)
            except (IOError, FileNotFoundError) as e:
                logging.error("X ERROR: could not open %s in reading mode."%cfile)
            COVFILE.close()

    ## Using coverage files, check if the non variants are due to lack of depth
    if (options.covfiles3):
        for cfile in options.covfiles3:
            try:
                sample = get_sample_id(cfile)
                COVFILE = open("%s"%cfile, "r")
                aa_variant_matrix3.verify_coverage(COVFILE, sample, 2)
            except (IOError, FileNotFoundError) as e:
                logging.error("X ERROR: could not open %s in reading mode."%cfile)
            COVFILE.close()


    ## Compare the two callsets
    aa_variant_matrix.make_variant_hist()
    aa_variant_matrix2.make_variant_hist()
    aa_variant_matrix3.make_variant_hist()
    try:
        HIST = open("%s/refcomp_%s_%s_%s.dat"%(options.output, ref_name1, ref_name2, ref_name3), "w")
        compare_calls(aa_variant_matrix, aa_variant_matrix2, aa_variant_matrix3, options.reference, options.reference2, options.reference3, HIST, 0)    
    except (IOError, FileNotFoundError) as e:
        logging.error("X ERROR: could not open dual reference comparison file in writting mode.")
    HIST.close() 

    ###----------------------------------
    ## MAKING SLIDING WINDOW 
    ##
    logger.info("Make sliding window")
    try:
        HIST1 = open("%s/%s_SW.dat"%(options.output, ref_name1), "w")
        HIST2 = open("%s/%s_SW.dat"%(options.output, ref_name2), "w")
        HIST3 = open("%s/%s_SW.dat"%(options.output, ref_name3), "w")
        SW1 = aa_variant_matrix.genomic_variants_matrix.make_pos_sliding_window(100,1000)
        SW2 = aa_variant_matrix2.genomic_variants_matrix.make_pos_sliding_window(100,1000)
        SW3 = aa_variant_matrix3.genomic_variants_matrix.make_pos_sliding_window(100,1000)
        for step in sorted(SW1):
            line = ""
            for dat in sorted(SW1[step]):
                line += "\t%s"%SW1[step][dat]
            HIST1.write("%s%s\n"%(step,line))
        for step in sorted(SW2):
            line = ""
            for dat in sorted(SW2[step]):
                line += "\t%s"%SW2[step][dat]
            HIST2.write("%s%s\n"%(step,line))
        for step in sorted(SW3):
            line = ""
            for dat in sorted(SW3[step]):
                line += "\t%s"%SW3[step][dat]
            HIST3.write("%s%s\n"%(step,line))
    except (IOError, FileNotFoundError) as e:
        logging.error("X ERROR: could not open dual reference comparison file in writting mode.")
    HIST1.close() 
    HIST2.close() 
    HIST3.close() 

    ###----------------------------------
    ## PRINTING HISTOGRAM
    ##

    logger.info("Make genomic variant histogram")
    ## Print amino acid variants histogram
    try:
        HIST1 = open("%s/%s_aa_variant_hist.dat"%(options.output, ref_name1), "w")
        HIST2 = open("%s/%s_aa_variant_hist.dat"%(options.output, ref_name2), "w")
        HIST3 = open("%s/%s_aa_variant_hist.dat"%(options.output, ref_name3), "w")
        HIST1.write(aa_variant_matrix.print_variant_hist())
        HIST2.write(aa_variant_matrix2.print_variant_hist())
        HIST3.write(aa_variant_matrix3.print_variant_hist())
    except (IOError, FileNotFoundError) as e:
        logging.error("X ERROR: could not open the variant histogram files in writting mode.")
    HIST1.close()
    HIST2.close()
    HIST3.close()

    logger.info("Make amino acid variant histogram")
    ## Print amino acid variants histogram
    try:
        HIST1 = open("%s/%s_gen_variant_hist.dat"%(options.output, ref_name1), "w")
        HIST2 = open("%s/%s_gen_variant_hist.dat"%(options.output, ref_name2), "w")
        HIST3 = open("%s/%s_gen_variant_hist.dat"%(options.output, ref_name3), "w")
        HIST1.write(aa_variant_matrix.genomic_variants_matrix.print_variant_hist())
        HIST2.write(aa_variant_matrix2.genomic_variants_matrix.print_variant_hist())
        HIST3.write(aa_variant_matrix3.genomic_variants_matrix.print_variant_hist())
    except (IOError, FileNotFoundError) as e:
        logging.error("X ERROR: could not open the variant histogram files in writting mode.")
    HIST1.close()
    HIST2.close()
    HIST3.close()

    logger.info("Make sample histogram")
    ## Print sample histogram (amino acid variants)
    try:
        HIST1 = open("%s/%s_aa_sample_hist.dat"%(options.output, ref_name1), "w")
        HIST2 = open("%s/%s_aa_sample_hist.dat"%(options.output, ref_name2), "w")
        HIST3= open("%s/%s_aa_sample_hist.dat"%(options.output, ref_name3), "w")
        HIST1.write(aa_variant_matrix.print_sample_hist())
        HIST2.write(aa_variant_matrix2.print_sample_hist())
        HIST3.write(aa_variant_matrix3.print_sample_hist())
    except (IOError, FileNotFoundError) as e:
        logging.error("X ERROR: could not open the variant histogram files in writting mode.")
    HIST1.close()
    HIST2.close()
    HIST3.close()
 
    logger.info("Make sample histogram")
    ## Print sample histogram (genomic variants)
    try:
        HIST1 = open("%s/%s_gen_sample_hist.dat"%(options.output, ref_name1), "w")
        HIST2 = open("%s/%s_gen_sample_hist.dat"%(options.output, ref_name2), "w")
        HIST3 = open("%s/%s_gen_sample_hist.dat"%(options.output, ref_name3), "w")
        HIST1.write(aa_variant_matrix.genomic_variants_matrix.print_sample_hist())
        HIST2.write(aa_variant_matrix2.genomic_variants_matrix.print_sample_hist())
        HIST3.write(aa_variant_matrix3.genomic_variants_matrix.print_sample_hist())
    except (IOError, FileNotFoundError) as e:
        logging.error("X ERROR: could not open the variant histogram files in writting mode.")
    HIST1.close()
    HIST2.close()
    HIST3.close()
    
    ###----------------------------------
    ## PRINTING MATRICES 
    ##

    logger.info("Make amino acid variants matrices")
    ## Print amino acid matrix in csv format 
    info_to_csv = ["mutation_type", "depth", "score", "allele_frequency"]
    for info in info_to_csv:
        try:
            MATRIX1 = open("%s/%s_aa_variant_matrix.%s.csv"%(options.output, ref_name1, info), "w")
            MATRIX2 = open("%s/%s_aa_variant_matrix.%s.csv"%(options.output, ref_name2, info), "w")
            MATRIX3 = open("%s/%s_aa_variant_matrix.%s.csv"%(options.output, ref_name3, info), "w")
            MATRIX1.write(aa_variant_matrix.print_csv_smpl2var(info))
            MATRIX2.write(aa_variant_matrix2.print_csv_smpl2var(info))
            MATRIX3.write(aa_variant_matrix3.print_csv_smpl2var(info))
        except (IOError, FileNotFoundError) as e:
            logging.error("X ERROR: could not open %s in writting mode."%options.newmatrixfile)
        MATRIX1.close()
        MATRIX2.close()
        MATRIX3.close()

    logger.info("Make genomic variants matrices")
    ## Print genomic matrix in csv format 
    info_to_csv = ["score", "pos", "depth", "mutation_type", "allele_frequency"]
    for info in info_to_csv:
        try:
            MATRIX1 = open("%s/%s_gen_variant_matrix.%s.csv"%(options.output, ref_name1, info), "w")
            MATRIX2 = open("%s/%s_gen_variant_matrix.%s.csv"%(options.output, ref_name2, info), "w")
            MATRIX3 = open("%s/%s_gen_variant_matrix.%s.csv"%(options.output, ref_name3, info), "w")
            MATRIX1.write(aa_variant_matrix.genomic_variants_matrix.print_csv_smpl2var(info))
            MATRIX2.write(aa_variant_matrix2.genomic_variants_matrix.print_csv_smpl2var(info))
            MATRIX2.write(aa_variant_matrix3.genomic_variants_matrix.print_csv_smpl2var(info))
        except (IOError, FileNotFoundError) as e:
            logging.error("X ERROR: could not open %s in writting mode."%options.newmatrixfile)
        MATRIX1.close()
        MATRIX2.close()
        MATRIX3.close()
    
    ###----------------------------------
    ## PRINTING SAMPLES SUMMARIES
    ##

    logger.info("Make sample summaries for amino acid variants")
    ## Print samples summary about amino acid variants
    try:
        SUMMARY1 = open("%s/%s_aa_sample_summary.dat"%(options.output, ref_name1), "w")
        SUMMARY2 = open("%s/%s_aa_sample_summary.dat"%(options.output, ref_name2), "w")
        SUMMARY3 = open("%s/%s_aa_sample_summary.dat"%(options.output, ref_name3), "w")
        SUMMARY1.write(aa_variant_matrix.print_sample_summary())
        SUMMARY2.write(aa_variant_matrix2.print_sample_summary())
        SUMMARY3.write(aa_variant_matrix3.print_sample_summary())
    except (IOError, FileNotFoundError) as e:
        logging.error("X ERROR: could not open the variant histogram files in writting mode.")
    SUMMARY1.close()
    SUMMARY2.close()
    SUMMARY3.close()

    logger.info("Make sample summaries for genomic variants")
    ## Print samples summary about genomic variants
    try:
        SUMMARY1 = open("%s/%s_gen_sample_summary.dat"%(options.output, ref_name1), "w")
        SUMMARY2 = open("%s/%s_gen_sample_summary.dat"%(options.output, ref_name2), "w")
        SUMMARY3 = open("%s/%s_gen_sample_summary.dat"%(options.output, ref_name3), "w")
        SUMMARY1.write(aa_variant_matrix.genomic_variants_matrix.print_sample_summary())
        SUMMARY2.write(aa_variant_matrix2.genomic_variants_matrix.print_sample_summary())
        SUMMARY3.write(aa_variant_matrix3.genomic_variants_matrix.print_sample_summary())
    except (IOError, FileNotFoundError) as e:
        logging.error("X ERROR: could not open the variant histogram files in writting mode.")
    SUMMARY1.close()
    SUMMARY2.close()
    SUMMARY3.close()
  
    ###----------------------------------
    ## PRINTING SAMPLES SUMMARIES
    ##

    logger.info("Make sample summaries for genes")
    aa_variant_matrix.print_gene_summary()
    try:
        SUMMARY1 = open("%s/%s_gene_summary.dat"%(options.output, ref_name1), "w")
        SUMMARY2 = open("%s/%s_gene_summary.dat"%(options.output, ref_name2), "w")
        SUMMARY3 = open("%s/%s_gene_summary.dat"%(options.output, ref_name3), "w")
        SUMMARY1.write(aa_variant_matrix.print_gene_summary())
        SUMMARY2.write(aa_variant_matrix2.print_gene_summary())
        SUMMARY3.write(aa_variant_matrix3.print_gene_summary())
    except (IOError, FileNotFoundError) as e:
        logging.error("X ERROR: could not open the variant histogram files in writting mode.")
    SUMMARY1.close()
    SUMMARY2.close()
    SUMMARY3.close()
