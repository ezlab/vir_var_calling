#!/usr/bin/env python

"""
-- aa_variants_matrix.py is part of the VVC pipeline

This tools wrap some operations on an allele frequency matrices.

"""

import sys
import argparse
import re
import collections
import operator
import json
import textwrap
import Bio
import logging
import multiprocessing
import time
from multiprocessing.managers import BaseManager, NamespaceProxy
from threading import Thread
import pandas
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC, generic_rna
import vcf
from variants_matrix import VariantIndex, VariantList, VariantMatrix, compare_calls

__author__  = "Alexis Loetscher"
__date__    = "June 2016"
__copyright__ = ""

__status__ = "Development"

FORMAT = '[%(asctime)-15s] %(processName)s - %(levelname)s: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()
logger_handler = logging.StreamHandler()

_Observation = collections.namedtuple("_Observation", ["sample", "aa_variant"])

## SNPeff keywords
_snpeff_impact = ["HIGH", "MODERATE", "LOW", "MODIFIER"]
_snpeff_effect = ["coding_sequence_variant", "chromosome", "coding_sequence_variant",
                  "inframe_insertion", "disruptive_inframe_insertion", "inframe_deletion",
                  "disruptive_inframe_deletion", "downstream_gene_variant", "exon_variant",
                  "exon_loss_variant", "frameshift_variant", "gene_variant",
                  "intergenic_region", "conserved_intergenic_variant", "intragenic_variant",
                  "intron_variant", "conserved_intron_variant", "miRNA", "missense_variant",
                  "initiator_codon_variant", "stop_retained_variant", "rare_amino_acid_variant",
                  "splice_acceptor_variant", "splice_donor_variant", "splice_region_variant",
                  "stop_lost", "5_prime_UTR_premature start_codon_gain_variant", "start_lost",
                  "stop_gained", "synonymous_variant", "start_retained", "stop_retained_variant",
                  "transcript_variant", "regulatory_region_variant", "upstream_gene_variant",
                  "3_prime_UTR_variant", "3_prime_UTR_truncation", "5_prime_UTR_variant",
                  "5_prime_UTR_truncation", "sequence_feature", "non_coding_exon_variant",
                  "non_coding_transcript_variant", "5_prime_UTR_premature_start_codon_gain_variant"]
_snpeff_warning = ["WARNING_REF_DOES_NOT_MATCH_GENOME", "WARNING_SEQUENCE_NOT_AVAILABLE",
                   "WARNING_TRANSCRIPT_INCOMPLETE", "WARNING_TRANSCRIPT_MULTIPLE_STOP_CODONS",
                   "WARNING_TRANSCRIPT_NO_START_CODON", "ERROR_CHROMOSOME_NOT_FOUND",
                   "ERROR_OUT_OF_CHROMOSOME_RANGE"]


## Default filters
_default_filter = {"pos":[[1,"end"]],
                   "qual":0,
                   "DP":0,
                   "AF":0,
                   "TVOTES":0,
                   "SB":1000000,
                   "impact":["HIGH", "MODERATE", "LOW"],
                   "effect":_snpeff_effect,
                   "warnings":_snpeff_warning
        }

## Symbols
_gene_sep = ":"


###
## Manager multiprocessing
##

class VariantMatrixManager(BaseManager): pass

class VariantMatrixProxy(NamespaceProxy):
    _exposed_ = ('__getattribute__', '__setattr__', '__delattr__')

def Manager():
    m = VariantMatrixManager()
    m.start()
    return m

VariantMatrixManager.register('VariantMatrix', VariantMatrix, exposed=dir(VariantMatrix))

def extract_info_hgvs_tag(tag):
    """ Extract info from HGVS tag
    """
    gene = re.sub(r":.*$", "", tag)
    hgvs_tag = re.sub(r"^.*:", "", tag)
    pos = re.sub(r"[^0-9]+", "", hgvs_tag)
    ref = re.findall(r"[A-Z]{1}[a-z]{2}", hgvs_tag)[0]
    alt = re.findall(r"[A-Z]{1}[a-z]{2}", hgvs_tag)[1]
    return gene, pos, ref, alt

def make_combined_var_database(var_list):
    """
    Make a list of variant formatted as such:
    <gene>:<pos>:<ref>:<alt>
    """
    combined_var_database = []
    ## Regex to match a protein snp
    aa_snp = re.compile(r"[a-zA-Z0-9]+:p\.[A-Z]{1}[a-z]{2}[0-9]+[A-Z]{1}[a-z]{2}")
    for var in var_list:
        if not re.match(aa_snp, var):
            continue
        gene, pos, ref, alt = extract_info_hgvs_tag(var)
        combined_var_database.append((gene, pos, ref, alt))
    return combined_var_database

def get_gene_name(aa_variant):
    """ Get gene name from aa_variant ID
    """
    return re.split("%s"%_gene_sep, aa_variant)[0]

def get_ref_name(fasta_file_name):
    """ Get reference name
    """
    ref_name = re.sub(r"^.+/", "", fasta_file_name)
    ref_name = re.split("\.", ref_name)
    return ref_name[0]

def get_sample_id(fname):
    """ Get sample id from file name.
    """
    fname = re.sub(r"^.+\/", "", fname)
    fname = re.sub(r"\..+$", "", fname)
    return fname

def read_cov_file(fname):
    """ Read coverage file
    """
    pos2cov = {}
    with open("%s"%(fname)) as COV:
        for rawline in COV.readlines():
            if rawline == "" or rawline[0] == "#":
                continue
            line = rawline.rstrip("\n").strip()
            sline = line.split("\t")
            pos, cov = [int(x) for x in sline[1:]]
            pos2cov[pos] = cov
    return(pos2cov)


class RunAnalysis(Thread):
    def __init__(self, variant_matrix, vcffiles, covfiles, output, ref_name, update, covmat_path,
                 depthmat_path, n_callers ):
        Thread.__init__(self)
        self.variant_matrix = variant_matrix
        self.vcffiles = vcffiles
        self.covfiles = covfiles
        self.output = output
        self.ref_name = ref_name
        self.covmat_path = covmat_path
        self.depthmat_path = depthmat_path
        self.update = update
        self.n_callers = int(n_callers)

    def run(self):
        ###----------------------------------
        ## FILLING MATRICES
        ##
        logger_handler.setFormatter(FORMAT)

        logger.info("Reading VCF files for strain: %s"%(self.ref_name))
        ## Fill up the matrix with a list of VCF files
        for vfile in self.vcffiles:
            sample = get_sample_id(vfile)
            self.variant_matrix.add_from_vcf(vfile, sample=sample)

        ###----------------------------------
        ## CHECKING COVERAGE
        ##

        ## Load coverage related matrices
        if not self.update:
            covmat = pandas.read_csv(self.covmat_path, dtype={'sample':str})
            covmat = covmat.set_index("sample")
            self.variant_matrix.smpl2gene_cov = covmat

            depthmat = pandas.read_csv(self.depthmat_path, dtype={'sample':str})
            depthmat = depthmat.set_index("sample")
            self.variant_matrix.smpl2gene_depth = depthmat

        logger.info("Reading coverage files for strain: %s"%(self.ref_name))
        ## Using coverage files, check if the non variants are due to lack of depth
        if self.covfiles:
            for cfile in self.covfiles:
                sample = get_sample_id(cfile)
                pos2cov = read_cov_file(cfile)
                self.variant_matrix.check_coverage(pos2cov, sample, self.update, threshold=6)

        ###----------------------------------
        ## Write general summaries
        ##

        ## Write summary for matrix 1
        tic = time.clock()
        SUMMATRIX1 = open("%s/summary_%s.dat"%(self.output, self.ref_name), "w")
        SUMMATRIX1.write("- Raw data \n")
        SUMMATRIX1.write(self.variant_matrix.print_stats())
        elapsed = time.clock() - tic
        logger.info("%s - %.3f - Print stats of raw data"%(self.ref_name, elapsed))


        ###----------------------------------
        ## 6 VOTES
        ##
        tic =time.clock()
        self.variant_matrix.filter_maj(self.n_callers, excl_caller=["freebayes", "gatk"])
        #self.variant_matrix.filter_maj(self.n_callers)
        elapsed = time.clock() - tic
        logger.info("%s - %.3f - Filtered for maj: %s callers"%(self.ref_name, elapsed, self.n_callers))

        tic = time.clock()
        SUMMATRIX1.write("- After filtering maj\n")
        SUMMATRIX1.write(self.variant_matrix.print_stats(filtered=True))
        elapsed = time.clock() - tic
        logger.info("%s - %.3f - Print stats of filtered data: %s callers"%(self.ref_name, elapsed, self.n_callers))

        ###----------------------------------
        ## COMPARING CALLSETS
        ##
        logger.info("%s - Compare call sets"%(self.ref_name))
        ## Compare the two callsets
        ##TODO: implement that in new_self.variant_matrix

        logger.info("%s - Print correction matrices"%(self.ref_name))
        ## 
        matrix1 = "%s/%s_gene_cov_corr.dat"%(self.output, self.ref_name)
        self.variant_matrix.make_smpl2gene_cov_mat(file_path=matrix1)

        matrix1 = "%s/%s_gene_depth_corr.dat"%(self.output, self.ref_name)
        self.variant_matrix.make_smpl2gene_depth_mat(file_path=matrix1)
        ###----------------------------------
        ## VARIANT MATRICES
        ##
        logger.info("%s - "%(self.ref_name))
        for info in ["binary", "AF"]:
            ## Print amino acid variants
            tic = time.clock()
            matrix1 = "%s/%s_aa_variant_matrix.%s.dat"%(self.output, self.ref_name, info)
            self.variant_matrix.make_smpl2aa_var_mat(info, filtered=True, file_path=matrix1)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Print amino acid variant matrix: %s"%(self.ref_name, elapsed, info))

            ## Print non synonymous amino acid variants
            tic = time.clock()
            matrix1 = "%s/%s_aa_variant_matrix.non_synonymous.%s.dat"%(self.output, self.ref_name, info)
            self.variant_matrix.make_smpl2aa_var_mat(info, filtered=True,
                                                non_synonymous=True, file_path=matrix1)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Print non synonymous amino acid variant matrix: %s"%(self.ref_name, elapsed, info))

            ## Print genomic variants
            tic = time.clock()
            matrix1 = "%s/%s_gen_variant_matrix.%s.dat"%(self.output, self.ref_name, info)
            self.variant_matrix.make_smpl2gen_var_mat(info, filtered=True, file_path=matrix1)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Print genomic variant matrix: %s"%(self.ref_name, elapsed, info))

        ## Print genomic variants
        tic = time.clock()
        matrix1 = "%s/%s_align.dat"%(self.output, self.ref_name)
        self.variant_matrix.make_alignment(filtered=True, file_path=matrix1)
        elapsed = time.clock() - tic
        logger.info("%s - %.3fs - Print alternative alleles matrix: %s"%(self.ref_name, elapsed, info))

        for n_samp in [0, 5, 1]:
            ## Filter by number of samples
            self.variant_matrix.filter_n_samples(n_samp, invert=True)

            tic = time.clock()
            SUMMATRIX1.write("- After filtering number of samples: %s\n"%((n_samp)))
            SUMMATRIX1.write(self.variant_matrix.print_stats(filtered=True))
            elapsed = time.clock() - tic
            logger.info("%s - %s - Print stats of filtered data: %s callers and minimum %s samples"%(self.ref_name, elapsed, self.n_callers, n_samp))

            ###----------------------------------
            ## MAKING SLIDING WINDOW
            ##
            ##
            tic = time.clock()
            sw1 = "%s/%s_SW.max%ssamp.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.print_pos_sliding_window(100, 1000, filtered=True, file_path=sw1)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make sliding window"%(self.ref_name, elapsed))

            tic = time.clock()
            sw1 = "%s/%s_SW_per_sample.max%ssamp.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_pos_sliding_window_per_sample(100, 1000, filtered=True, file_path=sw1)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make sliding window per sample"%(self.ref_name, elapsed))

            ###----------------------------------
            ## FULL DATASET VARIANTS TO SAMPLES
            ##
            ## Print amino acid variants histogram
            tic = time.clock()
            list1 = "%s/%s_aa_variant_list.max%ssamp.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.print_aa_var2smpl_list(file_path=list1, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make amino acid variant list"%(self.ref_name, elapsed))

            ## Print amino acid variants histogram
            tic = time.clock()
            list1 = "%s/%s_aa_variant_list.max%ssamp.non_synonymous.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.print_aa_var2smpl_list(file_path=list1, non_synonymous=True, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make synonymous amino acid variant list"%(self.ref_name, elapsed))

            ## Print amino acid variants histogram
            tic = time.clock()
            list1 = "%s/%s_gen_variant_list.max%ssamp.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.print_gen_var2smpl_list(file_path=list1, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make genomic variant list"%(self.ref_name, elapsed))

            ## Print sample histogram (amino acid variants)
            tic = time.clock()
            list1 = "%s/%s_aa_sample_list.max%ssamp.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.print_smpl2aa_var_list(file_path=list1, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make sample list for amino acid variants"%(self.ref_name, elapsed))

            ## Print sample histogram (amino acid variants)
            tic = time.clock()
            list1 = "%s/%s_aa_sample_list.max%ssamp.non_synonymous.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.print_smpl2aa_var_list(file_path=list1, non_synonymous=True, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make sample list for amino acid variants"%(self.ref_name, elapsed))

            ## Print sample histogram (genomic variants)
            tic = time.clock()
            list1 = "%s/%s_gen_sample_list.max%ssamp.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.print_smpl2gen_var_list(file_path=list1, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make sample list for genomic variants"%(self.ref_name, elapsed))

            ###----------------------------------
            ## Collapsed gene matrices
            ##
            ## Print collapsed gene matrix
            tic = time.clock()
            genemat1 = "%s/%s_gene_matrix.max%ssamp.binary.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_smpl2gene_mat(file_path=genemat1, binary=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make collapsed gene matrices - binary"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genemat1 = "%s/%s_gene_matrix.max%ssamp.counts.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_smpl2gene_mat(file_path=genemat1)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make collapsed gene matrices - counts:"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genemat1 = "%s/%s_gene_matrix.max%ssamp.cov_corr_counts.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_smpl2gene_mat(file_path=genemat1, correct_cov=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make collapsed gene matrices - coverage corrected counts"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genemat1 = "%s/%s_gene_matrix.max%ssamp.depth_corr_counts.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_smpl2gene_mat(file_path=genemat1, correct_depth=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make collapsed gene matrices - depth corrected counts"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genemat1 = "%s/%s_gene_matrix.max%ssamp.binary.non_synonymous.dat"%(self.output, self.ref_name,
                                                                                n_samp)
            self.variant_matrix.make_smpl2gene_mat(file_path=genemat1, binary=True, non_synonymous=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make non synonymous collapsed gene matrices - binary"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genemat1 = "%s/%s_gene_matrix.max%ssamp.counts.non_synonymous.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_smpl2gene_mat(file_path=genemat1, non_synonymous=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make non synonymous collapsed gene matrices - counts:"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genemat1 = "%s/%s_gene_matrix.max%ssamp.depth_corr_counts.non_synonymous.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_smpl2gene_mat(file_path=genemat1, correct_cov=True, non_synonymous=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make non synonymous collapsed gene matrices - coverage corrected counts"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genemat1 = "%s/%s_gene_matrix.max%ssamp.cov_corr_counts.non_synonymous.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_smpl2gene_mat(file_path=genemat1, correct_depth=True, non_synonymous=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make non synonymous collapsed gene matrices - depth corrected counts"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genesum = "%s/%s_gene_summary.max%ssamp.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.print_gene_summary(file_path=genesum, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make gene summary"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genesum = "%s/%s_aa_variant_summary.max%ssamp.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_aa_variant_summary(file_path=genesum, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make amino acid variant summary"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genesum = "%s/%s_gen_variant_summary.max%ssamp.dat"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.make_gen_variant_summary(file_path=genesum, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make genomic variant summary"%(self.ref_name, elapsed))

            ## Print collapsed gene matrix
            tic = time.clock()
            genesum = "%s/%s_gene_matrix.max%ssamp"%(self.output, self.ref_name, n_samp)
            self.variant_matrix.print_smpl2gene_summaries(file_prefix=genesum, filtered=True)
            elapsed = time.clock() - tic
            logger.info("%s - %.3fs - Make sample to gene matrices for each categories"%(self.ref_name, elapsed))

        SUMMATRIX1.close()

###---------------------------------------------------------------------------
## Main
def main():
    parser = argparse.ArgumentParser(description="This tool extract amino acid variants from a snpEff annotated VCF file.")

    parser.add_argument('-v', '--vcffiles', dest='vcffiles',
                        help='List of VCF file', metavar='FILE', required=True, nargs="+")
    parser.add_argument('-v2', '--vcffiles2', dest='vcffiles2',
                        help='List of VCF file', metavar='FILE', required=True, nargs="+")
    parser.add_argument('-m', '--multivcffile', dest='mvcffile',
                        help='Multi-sample VCF file', metavar='FILE', required=False)
    parser.add_argument('-m2', '--multivcffiles2', dest='mvcffile2',
                        help='Multi-sample VCF file', metavar='FILE', required=False)
    parser.add_argument('-c', '--covfiles', dest='covfiles',
                        help='List of COV file', metavar='FILE', required=False, nargs="+")
    parser.add_argument('-c2', '--covfiles2', dest='covfiles2',
                        help='List of COV file', metavar='FILE', required=False, nargs="+")
    parser.add_argument('-r', '--gbreference', dest='gbreference',
                        help='reference', metavar='FILE', required=True)
    parser.add_argument('-r2', '--gbreference2', dest='gbreference2',
                        help='reference', metavar='FILE', required=True)
    parser.add_argument('-f', '--filters', dest='filters',
                        help='Json file containing filters', metavar='FILE', required=False)
    parser.add_argument('-b', '--sampleblacklist', dest='sampleblacklist',
                        help='IDs of blacklisted samples', metavar='FILE', required=False)
    parser.add_argument('-o', '--outputfolder', dest='output',
                        help='Were to store all output', required=True)
    parser.add_argument('-cm', '--coord_map', dest='coord_map',
                        help='Were to store all output', required=False)
    parser.add_argument('-com', '--covmat', dest='covmat',
                        help='Uncovered sites matrices for ref 1', required=False)
    parser.add_argument('-com2', '--covmat2', dest='covmat2',
                        help='Uncovered sites matrices for ref 2', required=False)
    parser.add_argument('-dm', '--depthmat', dest='depthmat',
                        help='Mean depth per gene for ref 1', required=False)
    parser.add_argument('-dm2', '--depthmat2', dest='depthmat2',
                        help='Mean depth per gene for ref 2', required=False)
    parser.add_argument('-u', '--update', dest='update', action="store_true", default=False,
                        help='Update the correction matrices', required=False)
    parser.add_argument('-n', '--n_callers', dest='n_callers', default=6, type=int,
                        help='Threshold on callers', required=False)

    opts = parser.parse_args()

    ## Read filters, take default if not set
    if opts.filters:
        with open("%s"%(opts.filters), "r") as JSON:
            filters = json.load(JSON)
    else:
        filters = _default_filter

    ## Read the blacklist of samples
    blacklist = []
    if opts.sampleblacklist:
        with open("%s"%opts.sampleblacklist, "r") as BLACKLIST:
            for rawline in BLACKLIST.readlines():
                if (rawline == ""
                        or rawline[0] == "#"):
                    continue
                line = rawline.rstrip('\n').strip()
                sline = re.split(r",|\t|\s", line)
                blacklist = blacklist + sline

    ## Read the coordinate maps between ref1 and ref2
    coord_map = {}
    if opts.coord_map:
        with open("%s"%(opts.coord_map), "r") as MAP:
            json_map = json.load(MAP)
            for protein in json_map:
                coord_map[protein] = dict(zip([x[0] for x in json_map[protein]],
                                              [x[1] for x in json_map[protein]]))

    ## Get reference tag #1
    ref_name1 = get_ref_name(opts.gbreference)
    ref_name2 = get_ref_name(opts.gbreference2)

    manager = Manager()

    callers = sorted(["freebayes", "lofreq", "samtools", "snver", "varscan2", "gatk"])
    ## Create the aa_variant matrix objects
    variant_matrix1 = VariantMatrix(filters, opts.gbreference, blacklist,
                                    callers=callers)
    ## Create an other matrix, different reference
    variant_matrix2 = VariantMatrix(filters, opts.gbreference2, blacklist,
                                    callers=callers)
    print(dir(variant_matrix1))
    p1 = RunAnalysis(variant_matrix1, opts.vcffiles, opts.covfiles, opts.output,
                     ref_name1, opts.update, opts.covmat, opts.depthmat, opts.n_callers)
    p2 = RunAnalysis(variant_matrix2, opts.vcffiles2, opts.covfiles2, opts.output,
                     ref_name2, opts.update, opts.covmat2, opts.depthmat2, opts.n_callers)


    p1.start()
    p2.start()
    p1.join()
    p2.join()

    variant_matrix1.filter_maj(opts.n_callers, excl_caller=["freebayes", "gatk"])
    variant_matrix2.filter_maj(opts.n_callers, excl_caller=["freebayes", "gatk"])

    refcomp = "%s/refcomp_%s_vs_%s.dat"%(opts.output, ref_name1, ref_name2)
    compare_calls(variant_matrix1, variant_matrix2,
                  ref_name1, ref_name2,
                  coord_map=coord_map,
                  file_path=refcomp)

if __name__ == "__main__":
    main()
