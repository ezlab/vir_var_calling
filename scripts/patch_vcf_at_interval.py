#!/usr/bin/env python
## Last modified:
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
from collections import OrderedDict
import logging
import json
from vcf import Reader, Container, Writer
from interval import interval

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

###------------------------------------------------------------------------------------------------
## MAIN
##
def main():
    """ Main
    """
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-f1', '--vcffile1', dest='vcffile1',
                        help='The vcf file to patch',
                        metavar='FILE', required=True)
    parser.add_argument('-f2', '--vcffile2', dest='vcffile2',
                        help='The patch vcf',
                        metavar='FILE', required=True)
    parser.add_argument('-i', '--interval', dest='interval',
                        help='Interval that should be patched',
                        required=True)
    parser.add_argument('-o', '--outvcffile', dest='outvcffile',
                        help='The patched vcf file',
                        metavar='FILE', required=True)

    opts = parser.parse_args()

    if opts.interval:
        int_start = opts.interval.split(":")[0]
        int_end = opts.interval.split(":")[1]
        inter = interval([int_start, int_end])
    else:
        inter = interval([0, 0])
    print(inter)
    with open(opts.vcffile1, "r") as VCF1:
        vcffile1 = Container(reader=Reader(VCF1))

    with open(opts.vcffile2, "r") as VCF2:
        vcffile2 = Container(reader=Reader(VCF2))

    ## Make an empty vcf file with the fused headers
    outvcf = vcffile1.copy_empty()
    outvcf_tmp2 = vcffile2.copy_empty()
    outvcf.merge(outvcf_tmp2)

    for record in vcffile1:
        if record.POS not in inter:
            outvcf.append(record)
    for record in vcffile2:
        if record.POS in inter:
            outvcf.append(record)
    outvcf.sort_pos()

    with open("%s"%(opts.outvcffile), "w") as OUTVCF:
        vcf_writer = Writer(OUTVCF, outvcf)
        for record in outvcf:
            vcf_writer.write_record(record)

if __name__ == "__main__":
    main()
