#!/usr/bin/env python
## Last modified: Tue 20 Dec 2016 07:10:32 PM CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
import copy
import pandas
from interval import interval, inf, imath
from vcf import Reader, Writer, Container

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

def read_coord_map(FILE):
    coord_map = {}
    for rawline in FILE.readlines():
        line = rawline.rstrip("\n").strip()
        if line == "":
            continue
        sline = re.split(r"\t", line)
        coord_map[sline[1]] = sline[0]
    return coord_map



###-----------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-iv', '--vcffile', dest='vcffile',
                        help='VCF file from which to extract the positions',
                        metavar='FILE', required=True)
    parser.add_argument('-im', '--mapfile', dest='mapfile',
                        help='Map containing the positions',
                        metavar='FILE', required=True)
    parser.add_argument('-ov', '--outvcffile', dest='outvcffile',
                        help='Written VCF file', metavar='FILE', required=True)
    parser.add_argument('-c', '--newchr', dest='newchr',
                        help='New chromosome field',
                        type=str, required=True)

    opts = parser.parse_args()

    ## Read the map of coordinates
    with open(opts.mapfile, "r") as MAP:
        coord_map = pandas.read_csv(MAP, sep="\t", header=None)
        coord_map.columns = ["chrom", "start", "end"]
        coord_map.loc[:,"cum_len"] = coord_map.apply(lambda x: x["end"] - x["start"], axis=1).cumsum()

    with open(opts.vcffile, "r") as VCF:
        vcfcontainer = Container(reader=Reader(VCF))

    new_vcfcontainer = copy.copy(vcfcontainer)
    new_vcfcontainer.add_info("ORIG_POS", 1, "Integer", "Coordinate in the genome", None, None)
    new_vcfcontainer.add_contig(opts.newchr, sum(coord_map.loc[:,"cum_len"]))
    new_vcfcontainer.empty()
    for record in vcfcontainer:
        if not re.search(record.CHROM, opts.newchr):
            continue
        rel_pos = coord_map.loc[coord_map.apply(lambda x:
            x["end"] >= record.POS >= x["start"], axis=1),:].apply(lambda x:
                x["cum_len"] + (record.POS-x["end"]+1), axis=1)
        if len(rel_pos) > 0:
            rel_pos = list(rel_pos)[0]
            new_record = copy.copy(record)
            new_record.CHROM = opts.newchr
            new_record.POS = rel_pos
            new_record.add_info("ORIG_POS", record.POS)
            new_vcfcontainer.append(new_record)
    new_vcfcontainer.sort_pos()

    with open("%s"%opts.outvcffile, "w") as VCF:
        vcf_writer = Writer(VCF, new_vcfcontainer)
        for record in new_vcfcontainer:
            vcf_writer.write_record(record)

if __name__ == "__main__":
    main()

