#!/usr/bin/env python

"""
-- extract_highcov_seq.py is part of the VVC pipeline

Given a coverage file and a genbank file, this script returns the sequences that
are covered above a user-set threshold (default 500) and their annotations.
"""

__author__  = "Alexis Loetscher"
__date__    = "June 2016"
__copyright__ = "" 

__status__ = "Development"

import sys
import os
import re
import argparse
import collections
import Bio
from Bio import SeqIO
from Bio.SeqFeature import SeqFeature, FeatureLocation

def find_intervals(coverage, threshold):
    """
    Find the intervals above the defined threshold
    """
    intervals = list()
    first_pos = (1,1)
    prev_pos = (1,1)
    for pos in coverage:
        if prev_pos[1] < threshold and pos[1] >= threshold:
                first_pos = pos
        elif pos[1] < threshold and prev_pos[1] >= threshold:
                intervals.append((first_pos[0], prev_pos[0]))
        prev_pos = pos
    return intervals

def read_coverage(COVERAGE):
    """
    Read the output file from bedtools
    """
    coverage = list()
    for rawline in COVERAGE.readlines():
        line = rawline.rstrip("\n").strip()
        if line == "":
            continue
        sline = re.split(r"\s", line)
        if len(sline) == 3:
            ## <pos><read depth>
            coverage.append((int(sline[1]),int(sline[2])))
    return coverage

def read_intervals(INTERVALS):
    """
    Read file containing intervals <start><end>
    """
    intervals = list()
    for rawline in INTERVALS.readlines():
        line = rawline.rstrip("\n").strip()
        if line == "":
            continue
        sline = re.split(r",", line)
        if len(sline) == 3:
            ## <start><end>
            intervals.append((int(sline[1]),int(sline[2])))
    return intervals

## MAIN
def main():
    parser = argparse.ArgumentParser(description='Extract sequence from a fasta file and a list of intervals.')

    parser.add_argument('-c', '--coveragefile', dest='coveragefile',
                        help='Coverage file', 
                        metavar='FILE', required=True)
    parser.add_argument('-g', '--genbankfile', dest='genbankfile',
                        help='Genbank file',
                        metavar='FILE', required=True)
    parser.add_argument('-i', '--intervalfile', dest='intervalfile',
                         help='File containing a list of interval',
                         metavar='FILE', required=False)
    parser.add_argument('-t', '--threshold', dest='threshold',
                         help='Threshold to be considered high depth',
                         type=int, required=False, default=500)

    opts = parser.parse_args()

    ## Open a genbank file
    with open(opts.genbankfile, "r") as GB:
        records = Bio.SeqIO.parse(GB, "genbank")
        records = list(records)[0]
        sequence = records.seq

    if opts.intervalfile:
        with open(opts.intervalfile, "r") as INTERVALS:
            intervals = read_intervals(INTERVALS)

    with open(opts.coveragefile, "r") as COVERAGE:
        coverage = read_coverage(COVERAGE)

    intervals = find_intervals(coverage, int(opts.threshold))

    for interval in intervals:
        feature_interval = SeqFeature(FeatureLocation(interval[0], interval[1]))
        print("-----------------------------------")
        print("%s -> %s"%(interval[0], interval[1]))
        seq = feature_interval.extract(sequence)
        print(seq)
        for feature in records.features:
            if (interval[0] in feature):
                if (feature.type != "source"):
                    gene = ""
                    if ("gene" in feature.qualifiers):
                        gene = feature.qualifiers["gene"][0]
                    print("%s\t%s"%(feature.type, gene))

if __name__ == "__main__":
    main()
