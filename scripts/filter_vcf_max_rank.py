#!/usr/bin/env python
## Last modified: Wed 15 Mar 2017 10:05:21 AM CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
from vcf import Container, Reader, Writer

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

_callers = sorted(["freebayes", "lofreq", "samtools", "snver", "varscan2", "gatk"])

def get_max_rank(vcffile, n_votes, excl_callers):
    """ Get worst score for the given number of votes
    """
    max_rank_snp = 0
    max_rank_indel = 0
    caller_idx = [_callers.index(caller) for caller in _callers
                  if caller not in excl_callers]
    for record in vcffile:
        sample = record.get_samples()[0]
        votes = record.get_samp_field_data(sample, "VOTES")
        t_votes = sum([votes[i] for i in caller_idx])
        if record.INFO["TYPE"] == "snp":
            if t_votes >= n_votes and record.QUAL >= max_rank_snp:
                max_rank_snp = record.QUAL
        elif record.INFO["TYPE"] == "indel":
            if t_votes >= n_votes and record.QUAL >= max_rank_indel:
                max_rank_indel = record.QUAL
    return max_rank_snp, max_rank_indel

def make_rank_list(vcffile):
    """ Make list of ranks
    """
    vcffile.sort_score()
    rank_list = []
    i = 1
    for record in vcffile:
        sample = record.get_samples()[0]
        t_votes = record.get_samp_field_data(sample, "TVOTES")
        rank_list.append((str(record.INFO["TYPE"]), str(i), str(record.QUAL), str(t_votes)))
        i += 1
    vcffile.sort_pos()
    return rank_list

def to_rank_sum(vcffile, excl_callers):
    """ Make sum of rank as the score
    """
    caller_idx = [_callers.index(caller) for caller in _callers
                  if caller not in excl_callers]
    for record in vcffile:
        sample = record.get_samples()[0]
        ranks = [float(x) for x in record.get_samp_field_data(sample, "RANKS")]
        ranks = [ranks[i] for i in caller_idx]
        new_qual = sum(ranks)
        record.QUAL = new_qual
    return vcffile



###-----------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-iv', '--invcffile', dest='invcffile',
                        help='Input VCF file', metavar='FILE', required=True)
    parser.add_argument('-n', '--n_votes', dest='n_votes',
                        help='Number of votes for setting the score limit',
                        metavar='N', type=int, required=True, nargs="+")
    parser.add_argument('-s', '--rank_sum', dest='rank_sum',
                        help='Take sum of ranks instead of harmonic mean',
                        required=False, action="store_true")
    parser.add_argument('-ov', '--outvcffile', dest='outvcffile',
                        help='Output VCF file', metavar='FILE', required=True)
    parser.add_argument('-l', '--rank_sum_l', dest='list_rank',
                        help='Output VCF file', metavar='FILE', required=False)
    parser.add_argument('-hf', '--hard_filter', dest='hard_filter',
                        help='Hard filter VCF file', action="store_true", required=False)

    opts = parser.parse_args()

    #excl_callers = ["gatk", "freebayes"]
    excl_callers = []
    caller_idx = [_callers.index(caller) for caller in _callers
                  if caller not in excl_callers]
    opts.n_votes = [n_votes - len(excl_callers) for n_votes in opts.n_votes]

    with open("%s"%opts.invcffile, "r") as VCF:
        vcffile = Container(reader=Reader(VCF))

    ## Make rank sum to be the qual field
    if opts.rank_sum:
        vcffile = to_rank_sum(vcffile, excl_callers)

    ## Iterate over the number of votes to consider
    for n_votes in opts.n_votes:
        max_rank_snp, max_rank_indel = get_max_rank(vcffile, n_votes, excl_callers)
        print(max_rank_indel)
        with open("%s.%svotes.vcf"%(opts.outvcffile, n_votes), "w") as VCF:
            ## Add new filter: if the rank is lower than the highest for superior minimal number
            ## of votes, then pass it.
            vcffile.add_filter("MAX", "Maximum rank for the given number of vote: %s. \
                               Values: snp: %s indel:%s"%(n_votes, max_rank_snp, max_rank_indel))
            vcffile.add_filter("MAJ", "Number of vote: %s."%(n_votes))
            outvcffile = Writer(VCF, vcffile)
            ## Filter the VCF entries
            for record in vcffile:
                ## Test stuff
                if "ANN" in record.INFO:
                    if not isinstance(record.INFO["ANN"], bool):
                        pass
                        #print(dir(record.INFO["ANN"][0]))
                sample = record.get_samples()[0]
                ## Different scores for indels and snp
                if record.is_snp:
                    max_rank = max_rank_snp
                elif record.is_indel:
                    max_rank = max_rank_indel
                votes = record.get_samp_field_data(sample, "VOTES")
                t_votes = sum([votes[i] for i in caller_idx])
                ## Adjust filters
                if record.QUAL <= max_rank and t_votes >= n_votes:
                    record.FILTER = []
                elif record.QUAL <= max_rank and t_votes < n_votes:
                    record.add_filter("MAJ")
                else:
                    record.FILTER = None
                ## Hard-filter if politely asked for
                if opts.hard_filter and t_votes < n_votes:
                    continue
                else:
                    outvcffile.write_record(record)

    if opts.list_rank:
        rank_sum_list = make_rank_list(vcffile)
        with open("%s"%opts.list_rank, "w") as LIST:
            for dataline in rank_sum_list:
                LIST.write("\t".join(dataline) + "\n")

if __name__ == "__main__":
    main()
