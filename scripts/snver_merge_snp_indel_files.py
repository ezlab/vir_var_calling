#!/usr/bin/env python
## Last modified: Fri 10 Feb 2017 05:10:12 PM CET
"""
SNVer produces two files, containing the SNP and the indels, resprectively.
This script fuse them into one entity.
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
from vcf import Container, Reader, Writer

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()


def correct_genotype_fields(vcffile, ploidy, sample):
    vcffile.add_format("PL", 2, "Integer", "The phred-scaled score rounded to the closest")
    for record in vcffile:
        ## Coorrect GT field
        record.samples[0].sample = sample
        record._sample_indexes = {sample:0}
        new_fields = dict([(key,{}) for key in record.FORMAT.split(":")])
        for sample in record.get_samples():
            fields = record.get_samp_data(sample)
            new_PL = [4096 if x>4096 else x for x in fields["PL"]]
            ## Correct GT field
            if ploidy == 1:
                if fields["GT"] in ["1/0", "1/1"]:
                    fields["GT"] = "1"
                elif fields["GT"] == "0/0":
                    fields["GT"] = "0"
                fields["PL"] = [new_PL[2], new_PL[0]]
            ## Correct PL field
            ## Set limit to 4096
            for field in fields:
                new_fields[field][sample] = fields[field]
        record.add_samp_data(new_fields)
    return vcffile

###-----------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-p', '--prefix', dest='prefix',
                        help='Prefix of indel and snp vcf files',
                        type=str, required=True)
    parser.add_argument('-pl', '--ploidy', dest='ploidy',
                        help='Haploid or diploid call?',
                        type=int, required=True)
    parser.add_argument('-s', '--sample', dest='sample',
                        help='',
                        type=str, required=True)


    opts = parser.parse_args()

    with open("%s.filter.vcf"%(opts.prefix), "r") as SNPFILE:
        snpfile = Container(reader=Reader(SNPFILE))
        snpfile.formats["PL"] = snpfile.formats["PL"]._replace(num="G")

    with open("%s.indel.filter.vcf"%(opts.prefix), "r") as INDELFILE:
        indelfile = Container(reader=Reader(INDELFILE))
        snpfile.formats["PL"] = snpfile.formats["PL"]._replace(num="G")

    snpfile.merge(indelfile)

    snpfile.samples[0] = opts.sample
    snpfile._sample_indexes = {}
    snpfile._sample_indexes[opts.sample] = 0
    snpfile = correct_genotype_fields(snpfile, opts.ploidy, opts.sample)

    snpfile.formats["PL"] = snpfile.formats["PL"]._replace(num="G")
    with open("%s.vcf"%(opts.prefix), "w") as VCF:
        vcf_writer = Writer(VCF, snpfile)
        for record in snpfile:
            vcf_writer.write_record(record)

if __name__ == "__main__":
    main()
