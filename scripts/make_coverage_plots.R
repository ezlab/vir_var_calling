#!/usr/bin/Rscript

.libPaths( c( .libPaths(), "~/R/x86_64-pc-linux-gnu-library/3.3/"  )   )
require(ggplot2)
require(reshape2)
require(RColorBrewer)
require(RGraphics)
require(gridExtra)
require(intervals)
require(ape)
require(ape)
require(ggtree)
require(ggstance)
require(zoo)

###
is.in_interval <- function(int, intervals_list){
	bool <- length(interval_overlap(int, intervals_list)[[1]])!=0
	return(bool)
}


###
## Add variant position to coverage plot
add_var <- function(data, var_pos){
	data[,"var_pos"] <- 0
	print(colnames(data))
	for (pos in var_pos$V1){
		data$var_pos[data$pos==pos] <- 1 
	}
	return(data)
}

###
## Calculates stats and metrics about coverage
get_covstats <- function(list_sample, sample, base_wise_cov, genome_length){
  cov10X <- base_wise_cov[base_wise_cov$depth>=10,]
  cov6X <- base_wise_cov[base_wise_cov$depth>=6,]
  mean_cov <- mean(base_wise_cov$depth)
  mean_cov10X <- mean(cov10X$depth)
  mean_cov6X <- mean(cov6X$depth)
  fract_cov10X <- length(cov10X$depth)/genome_length
  fract_cov6X <- length(cov6X$depth)/genome_length
  list_sample[list_sample$sample==sample,]$mean_cov <- mean_cov
  list_sample[list_sample$sample==sample,]$mean_cov10X <- mean_cov10X
  list_sample[list_sample$sample==sample,]$mean_cov6X <- mean_cov6X
  list_sample[list_sample$sample==sample,]$fract_cov10X <- fract_cov10X
  list_sample[list_sample$sample==sample,]$fract_cov6X <- fract_cov6X
  return(list_sample)
}

###
## Get coverage on region
get_covX <- function(base_wise_cov, threshold=6){
  return(nrow(base_wise_cov[which(base_wise_cov$depth>=threshold),])/nrow(base_wise_cov))
}

###
## Get mean depth on region
get_mean_depth <- function(base_wise_cov){
  return(mean(base_wise_cov$depth))
}

###
## Sliding window
sliding_window <- function(positions, length,  width=1000, step=100){
  data_SW <- data.frame(window = integer(0), count = integer(0))
  
  windows <- seq(1,length,by=step)
  for(w in windows){
    pos_in_interval <- length(positions[positions >= w & positions <= w+width])
    new_row <- c(w, pos_in_interval)
    data_SW <- rbind(data_SW, new_row)
  }
  return(data_SW)
}

###
## Get repeat regions from  genbank file
get_repeats <- function(gb){
  repeats <- data.frame(ranges(subset(otherFeatures(gb), type=="repeat_region")))
  repeats <- subset(repeats, select=c("start", "end"))
  repeats <- cbind(chrom=rep(strain_ncbi_id, nrow(repeats)), repeats)
  return(repeats)
}

###
## Get CDS regions from genbank file
get_CDS <- function(gb){
  CDS <- data.frame(ranges(subset(cds(gb))))
  CDS <- subset(CDS, select=c("start", "end"))
  CDS <- cbind(chrom=rep(strain_ncbi_id, nrow(CDS)), CDS)
  return(unique(CDS))
}

###
## Get genes regions from genbank file
get_genes <- function(gb){
  gene_list <- unique(unlist(cds(gb_file)$gene))
  genes <- data.frame(chrom=character(0), start=integer(0), end=integer(0), gene=character(0))
  for (this_gene in gene_list){
    sub.this_gene <- subset(cds(gb_file), gene==this_gene)
    start <- head(start(ranges(sub.this_gene)), n=1)
    end <- tail(end(ranges(sub.this_gene)), n=1)
    genes <- rbind(genes, data.frame(chrom=strain_ncbi_id, start=start, end=end, gene=this_gene))
  }
  return(genes)
}

###
## Get coverage
get_coverage_dat <- function(fname){
  cov_date <- read.csv(file=fname, header=FALSE, sep="\t")
  colnames(cov_date) <- c("strain", "pos", "depth")
  return(cov_date)
}

###------------------------------------------------------------------------------------------------
## PLOT FUNCTIONs
##

make_covplot <- function(reference, sample, base_wise_cov, repeats, CDS, start, end){

	mean_cov <- get_mean_depth(base_wise_cov)
	fract_cov10X <- get_covX(base_wise_cov, 10)
	fract_cov6X <- get_covX(base_wise_cov, 6)
	
	end <- tail(base_wise_cov$pos, n=1)
	start <- head(base_wise_cov$pos, n=1)
	step <- 10^(floor(log10(end-start))-1)/2
	start <- ((start + step/2) / step) * step
	end <- ((end + step/2) / step) * step
	
	print(start)
	print(end)
	
	## Set legends	
	title <- paste ("Mapping of sample", sample, "on reference", reference, del=" ") 
	lblX <- paste("Position\nMean coverage: ", round(mean_cov,3), " | Coverage at 10X: ", round(fract_cov10X,3), " | Coverage at 6X: ", round(fract_cov6X,3), sep="" )
	lblY <- "Coverage"

	maxcov = max(base_wise_cov$depth)
	print(maxcov)

	base_wise_cov.long <- melt(base_wise_cov, id.vars=c("pos", "strain"))

	print(mean(base_wise_cov$depth))
	base_wise_cov.long$value <- as.numeric(base_wise_cov.long$value)

	.e <- environment()
	## Parameter the plot
	print(tail(base_wise_cov))
	covplot <- ggplot(environment=.e)
	covplot <- covplot + geom_rect(data=repeats, aes(xmin=start, xmax=end), ymin=0, ymax=maxcov, fill="black", alpha=0.1)
	covplot <- covplot + geom_rect(data=CDS, aes(xmin=start, xmax=end), ymin=0, ymax=maxcov, fill="blueviolet", alpha=0.1)
	covplot <- covplot + geom_ribbon(data = base_wise_cov, aes(x=pos,ymax=depth,ymin=0), alpha=.6, fill="blue")
	covplot <- covplot + geom_line(data = base_wise_cov, aes(pos,depth), size=.2, color="darkblue")
	covplot <- covplot + geom_hline(yintercept=mean_cov, color="red", linetype="dotted")
	covplot <- covplot + scale_x_continuous(breaks=seq(start, end, step)) 
	covplot <- covplot + theme(axis.text.x = element_text(angle = 60, hjust = 1))
	covplot <- covplot + labs(title=title, x=lblX, y=lblY)

	return(covplot)
}

###-------------------------------------------------------
## MAIN
##

option_list <- list(
  make_option(c("-f", "--datafile"), type="character", default=NULL, help="", metavar="character"),
  make_option(c("-n", "--name"), type="character", default=NULL, help="", metavar="character"),
  make_option(c("-g", "--reference_file"), type="character", default=NULL, help="", metavar="character"),
  make_option(c("-r", "--reference"), type="character", default=NULL, help="", metavar="character"),
  make_option(c("-p", "--outprefix"), type="character", default=NULL, help="", metavar="character"),
  make_option(c("-o", "--outpng"), type="character", default=NULL, help="", metavar="character")
)

opt_parser <- OptionParser(option_list=option_list)
opt <- parse_args(opt_parser)

## Test values
opt$name <- "17113"
opt$datafile <- "on_bee_raid2/assembly_batch2/stats/17113/17113.NC_007605.bbmap.pos_wise_cov.dat"
opt$reference_file <- "reference_pool/NC_007605.gb"
opt$outprefix <- "test.covplot"

data <- get_coverage_dat(opt$datafile)

gb_file_raw <- parseGenBank(file=opt$reference_file, partial=TRUE)
gb_file <- make_gbrecord(gb_file_raw)

strain_name <- names(getSeq(gb_file))
strain_ncbi_id <- accession(gb_file)
reference_length <- width(getSeq(gb_file))

## Read repeats interval file
repeats <- get_repeats(gb_file)
genes <- get_genes(gb_file)
CDS <- get_CDS(gb_file)

## Make and print coverage plot
fname <- paste0(opt$outprefix, ".png")
covplot <- make_covplot(strain_ncbi_id, opt$name, data, repeats, CDS)
ggsave(covplot, filename=fname, height=20, width=30)

## Make plots for genes
gene_prefix <- paste0(opt$outprefix, "_gene_plots")
dir.create(gene_prefix, showWarnings = FALSE)
for (gene_line in 1:nrow(genes)){
 start <- genes[gene_line,]$start
 end <- genes[gene_line,]$end
 gene <- as.character(genes[gene_line,]$gene)
 
 ## Subset intervals
 interval <- c(start:end)
 repeats_interval <- repeats[repeats$start %in% interval | repeats$end %in% interval,]
 CDS_interval  <- CDS[CDS$start %in% interval | CDS$end %in% interval,]
 reference_length <- length(interval)

 ##
 # window <- 1000
 # step <- 100
 # pos <- seq(0,reference_length,by=step)
 # sliding_window_cov <- data.frame(matrix(nrow=length(pos),ncol=0))
 # rownames(sliding_window_cov) <- pos

 data.sub <- data[data$pos %in% interval,]

 ## Fill up coverage matrices
 cov_6x_all[gene,as.character(sample)] <- get_covX(test_smpl, 6)
 cov_10x_all[gene,as.character(sample)] <- get_covX(test_smpl, 10)

 # ## Coverage sliding window
 # sliding_window_sample <- rollapply(test_smpl$depth, width=window, by=step, FUN=mean, align="left", partial=T)
 # sliding_window_cov <- cbind(sliding_window_cov, sliding_window_sample)
 # colnames(sliding_window_cov)[[ncol(sliding_window_cov)]] <- sample

 ## Make plot
 gene_compliant <- sub("/", "_", gene)
 fname <- paste0(gene_prefix, "/", gene_compliant, ".png")
 covplot <- make_covplot(strain_name, sample, data.sub, repeats_interval, CDS_interval)
 ggsave(covplot, file=fname, width=30)
 print(proc.time()-ptm)
}

## Transpose matrix
sliding_window_cov <- t(sliding_window_cov)