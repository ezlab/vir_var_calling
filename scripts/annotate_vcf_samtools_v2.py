#!/usr/bin/env python
## Last modified: mar 21 fev 2017 14:23:18 CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
import vcf
from vcf import Container, Reader, Writer
from vcf.model import _Call

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

def merge_annotation(to_annotate, annotation):
    """
    Assign or reassign format fields from a VCF record to another.
    """
    ##
    format_to_add = ["AD", "AF", "SP", "PL"]
    info_to_add = ["DP", "MQ"]
    fields_to_add = info_to_add + format_to_add

    ## Add everything in format fields
    values_to_add = dict([(k,{}) for k in to_annotate.FORMAT.split(":")])
    for sample in annotation.get_samples():
        samp_data = to_annotate.get_samp_data(sample)
        samp_data_ann = annotation.get_samp_data(sample)
        for field in fields_to_add:
            if field in samp_data:
                to_annotate.INFO[field] = samp_data[field]
            elif field in samp_data_ann:
                to_annotate.INFO[field] = samp_data_ann[field]
            elif field in annotation.INFO:
                to_annotate.INFO[field] = annotation.INFO[field]
            else:
                to_annotate.INFO[field] = "."
        if "AD" in samp_data_ann:
            DP = annotation.INFO["DP"]
            AD = samp_data_ann["AD"]
            allele_frequencies = [0 if DP==0 else ad/DP for ad in AD]
            to_annotate.INFO["AF"] = allele_frequencies
    return to_annotate

def get_minimal_representation(pos, ref, alt):
    """
    Trim overhanging positions in variant representation
    """
    ## Don't remap anything if it's a snv
    if len(ref) == 1 and len(alt) == 1:
        alt = str(alt[0])
        return pos, ref, alt
    else:
        alt = str(alt[0])
        ## Strip right position until different base are found
        while(alt[-1] == ref[-1] and min(len(alt),len(ref)) > 1):
            alt = alt[:-1]
            ref = ref[:-1]
        ## Strip left position until different base are found, increment position
        while(alt[0] == ref[0] and min(len(alt),len(ref)) > 1):
            alt = alt[1:]
            ref = ref[1:]
            pos += 1
        return pos, ref, alt


def is_indel(vcf_record):
    """
    Check if variant is indel
    """
    pos, ref, alt = get_minimal_representation(vcf_record.POS, vcf_record.REF, vcf_record.ALT)
    return (len(ref) != 1 or len(alt) != 1)


###------------------------------------------------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-iv', '--invcffile', dest='invcffile',
                        help='VCF file to annoatate',
                        metavar='FILE', required=True)
    parser.add_argument('-a', '--annotationfile', dest='annotationfile',
                        help='VCF file containing information about all position',
                        metavar='FILE', required=True)
    parser.add_argument('-ov', '--outvcffile', dest='outvcffile',
                        help='Annotated VCF file',
                        metavar='FILE', required=True)

    opts = parser.parse_args()

    with open(opts.invcffile, "r") as VCF:
        vcffile = Container(reader = Reader(VCF))

    ## Annotation file for SNPs
    with open(opts.annotationfile, "r") as VCF:
        annotationfileSNP = Container(reader=Reader(VCF), skip_invariant=True)
        var_list = {}
        for record in annotationfileSNP:
            rec_id = (record.CHROM, record.POS, record.REF, str(record.ALT[0]))
            var_list[rec_id] = record
    ## Set up fields
    ## NOTE: I really don't like hardcoding the number of values in fields.
    new_vcf = vcffile.copy()
    new_vcf.empty()
    new_vcf.add_info("PL", "G", "Integer", "Phred-scaled likelihood scores (samtools)", None, None)
    new_vcf.add_info("AD", "R", "Integer", "Allelic depth (samtools)", None, None)
    new_vcf.add_info("DP", 1, "Integer", "Filtered read depth (samtools)", None, None)
    new_vcf.add_info("AF", "R", "Float", "Allelic frequency", None, None)
    new_vcf.add_info("SP", 1, "Float", "Phred-scaled p-value for strand bias (samtools)", None, None)
    new_vcf.add_info("MQ", 1, "Float", "RMS mapping quality (samtools)", None, None)

    ## Traverse the VCF file to be annotated and transfer format and info fields.
    for record in vcffile:
        rec_id = (record.CHROM, record.POS, record.REF, str(record.ALT[0]))
        votes = record.INFO["VOTES"]
        if rec_id in var_list:
            new_record = merge_annotation(record, var_list[rec_id])
        else:
            logging.info("Did not find annotation for variant: %s. Support: %s:%s"%(str(rec_id), sum(votes), str(votes)))
        new_vcf.append(record)
    new_vcf.sort_pos()

    ## Write the new VCF file
    with open(opts.outvcffile, "w") as VCF:
        writer = Writer(VCF, new_vcf)
        for record in new_vcf:
            if not record.FORMAT:
                record.samples = []
            writer.write_record(record)

if __name__ == "__main__":
    main()
