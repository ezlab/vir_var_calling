#!/usr/bin/env python
## Last modified: Mon 05 Dec 2016 07:12:21 PM CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
from vcf import Reader, Container, Writer

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()


###-----------------------------------------------------------
## MAIN
##
def main():
    """ Main
    """
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-gv', '--genomevcffile', dest='genomevcffile',
                        help='VCF file of the genome', metavar='FILE', required=True)
    parser.add_argument('-pv', '--partvcffile', dest='partvcffile',
                        help='VCF file of the gene. It should contain the field',
                        metavar='FILE', required=True)
    parser.add_argument('-o', '--outvcffile', dest='outvcffile',
                        help='VCF file with annotation of the gene', metavar='FILE', required=True)

    opts = parser.parse_args()

    with open(opts.genomevcffile, "r") as VCF:
        genomevcffile = Container(reader=Reader(VCF))


    with open(opts.partvcffile, "r") as VCF:
        genevcffile = Container(reader=Reader(VCF))

    for record1 in genomevcffile:
        for record2 in genevcffile:
            if "ANN" not in record1.INFO and "ANN" not in record1.INFO:
                continue
            if record1.POS == record2.INFO["ORIG_POS"]:
                record1.INFO["ANN"].extend(record2.INFO["ANN"])

    with open(opts.outvcffile, "w") as VCF:
        vcf_writer = Writer(VCF, genomevcffile)
        for record in genomevcffile:
            if "ANN" in record.INFO:
                i = 0
                for ann in record.INFO["ANN"]:
                    record.INFO["ANN"][i] = record.INFO["ANN"][i]._replace(warnings_errors = ",".join(ann.warnings_errors))
                    print(record.INFO["ANN"][i].warnings_errors)
                    i += 1
            vcf_writer.write_record(record)

if __name__ == "__main__":
    main()
