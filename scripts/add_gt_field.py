#!/usr/bin/env python
## Last modified: Wed 21 Dec 2016 04:39:12 PM CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
from vcf import Container, Reader, Writer

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

###-----------------------------------------------------------
## MAIN
##
def main():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-iv', '--invcffile', dest='invcffile',
            help='Some file', metavar='FILE', required=True)
    parser.add_argument('-ov', '--outvcffile', dest='outvcffile',
            help='Some file', metavar='FILE', required=False)

    opts = parser.parse_args()

    with  open(opts.invcffile, "r") as VCF:
        invcffile = Container(reader=Reader(VCF))

    for record in invcffile:
        if not record.FORMAT or "GT" not in record.FORMAT:
            record.add_format("GT")
            samples = [x.sample for x in record.samples]
            gt_fields = {"GT":dict(zip(samples, ["."]*len(samples)))}
            record.add_samp_data(gt_fields)

    with open(opts.outvcffile, "w") as VCF:
        vcf_writer = Writer(VCF, invcffile)
        for record in invcffile:
            vcf_writer.write_record(record)


if (__name__ == "__main__"):
    main()
