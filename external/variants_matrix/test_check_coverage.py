#!/usr/bin/python
## Last modified:
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

###-----------------------------------------------------------
## MAIN
##
def main():
    """
    Main function
    """
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-f', '--file', dest='somefile', help='Some file',
                        metavar='FILE', required=True)

    opts = parser.parse_args()

    with open("%s"%(opts.somefile), "r") as SOMEFILE:
        pass

if __name__ == "__main__":
    main()
