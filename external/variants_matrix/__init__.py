#!/usr/bin/python
## Last modified: Tue 04 Oct 2016 02:20:02 PM CEST

"""
Write doc here
"""

import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
import cyvcf2

from .variant_matrix_v3 import VariantMatrix
from .variant_matrix_v3 import VariantIndex, VariantList, VariantMatrix, compare_calls

__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

