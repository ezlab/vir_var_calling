import numpy
cimport numpy
import pandas

cdef class VariantIndex:
    cdef:
        public int pos
        public str gen_variant
        public str ref
        public str alt
        public str gene
        public str aa_variant

        public tuple parse_gHGVS_tag(self)
        public tuple parse_pHGVS_tag(self)

cdef class VariantList:
    cdef:
        set _variant_list
        set _pos
        set _gen_variants
        set _genes
        set _aa_variants
        dict _pos2var
        dict _pos2ref
        dict _aa_var2mutation_type
        dict _gen_var2mutation_type

        public void append(self, int pos, str gen_variant, str ref, str alt, str gene=*, str aa_variant=*)
        public list subset(self, int pos=*, str gen_variant=*, str gene=*, str aa_variant=*)
        public void make_aa_var2mutation_type_list(self)
        public void make_gen_var2mutation_type_list(self)
        public void print_aa_var2mutation_type_list(self)

cdef class VariantMatrix:
    cdef:
        dict _variant_matrix
        VariantList _variant_list
        set _sample_list
        list _blacklist
        dict _filters
        dict _var2smpl
        dict _aa_var2smpl
        dict _gen_var2smpl
        dict _gene2smpl
        dict _smpl2var
        dict _smpl2aa_var
        dict _smpl2gen_var
        set _uncovered
        set _filtered
        dict _gene_info
        dict _pos2genes
        str _reference
        int _reference_length
        list _gene_location
        _smpl2gene_uncov_sites
        _smpl2gene_depth_sites
        _smpl2gene_cov
        _smpl2gene_depth
        list _callers
        str _mode
        list _timing
        dict _gene_list
        _gen_variant_summary
        _aa_variant_summary
        _gene_summary

    cpdef public list keys(self)
    cpdef public str print_stats(self, bint filtered=*)
    cpdef public void reset_filter(self)
    cpdef public set filter_max(self, int n_votes, list excl_caller=*)
    cpdef public set filter_maj(self, int n_votes, list excl_caller=*)
    cpdef public set filter_n_samples(self, int n_sample, bint invert=*)
    cpdef public set filter_mutation_type(self, list mutation_types, bint invert=*)
    cpdef public void get_info_from_gff(self, str gfiles)
    cpdef public void add_from_vcf_multi(self, str vcf_path)
    cpdef public void add_from_vcf(self, str vcf_path, str sample=*)
    cpdef public void check_coverage(self, dict cov_file, str sample, bint update=*, int threshold=*)
    cpdef public numpy.ndarray make_smpl2gene_mat(self, str file_path=*, bint binary=*,
                                            bint filtered=*, bint non_synonymous=*,
                                            bint correct_depth=*, bint correct_cov=*)
    cpdef public numpy.ndarray make_smpl2gen_var_mat(self, str info, str file_path=*, bint filtered=*)
    cpdef public numpy.ndarray make_smpl2aa_var_mat(self, str info, str file_path=*, bint filtered=*,
                                              bint non_synonymous=*)
    cpdef public numpy.ndarray make_smpl2gene_cov_mat(self, str file_path=*)
    cpdef public numpy.ndarray make_smpl2gene_depth_mat(self, str file_path=*)
    cpdef public numpy.ndarray make_alignment(self, str file_path=*, bint filtered=*)
    cpdef public dict make_var2smpl_list(self, bint filtered=*)
    cpdef public dict make_aa_var2smpl_list(self, bint filtered=*, bint non_synonymous=*)
    cpdef public dict make_gen_var2smpl_list(self, bint filtered=*)
    cpdef public dict make_gene2smpl_list(self, bint filtered=*)
    cpdef public dict make_smpl2var_list(self, bint filtered=*)
    cpdef public dict make_smpl2aa_var_list(self, bint filtered=*, bint non_synonymous=*)
    cpdef public dict make_smpl2gen_var_list(self, bint filtered=*)
    cpdef public numpy.ndarray make_gen_variant_summary(self, bint filtered=*, str file_path=*)
    cpdef public numpy.ndarray make_aa_variant_summary(self, bint filtered=*, str file_path=*)
    cpdef public numpy.ndarray make_gene_summary(self, bint filtered=*)
    cpdef public dict make_pos_sliding_window(self, int step, int window, bint filtered=*)
    cpdef public numpy.ndarray make_pos_sliding_window_per_sample(self, int step, int window,
                                                                  bint filtered=*, str file_path=*)
