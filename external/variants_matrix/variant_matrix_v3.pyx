#!/usr/bin/python
## Last modified:
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import io
import sys
import collections
import logging
import json
import copy
import time
import pandas
import numpy
cimport numpy
import vcf
import cyvcf2
from operator import attrgetter, itemgetter, methodcaller
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqFeature import FeatureLocation
from cpython.object cimport Py_LT, Py_LE, Py_EQ, Py_GE, Py_GT, Py_NE

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:  %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

## Index for the VariantsMatrix class
_Observation = collections.namedtuple("_Observation", ["sample", "variant"])
_VariantIndex = collections.namedtuple("_VariantIndex", ["pos", "gen_variant", "ref", "alt",
                                                         "gene", "aa_variant"])

## SNPeff keywords
_snpeff_impact = ["HIGH", "MODERATE", "LOW", "MODIFIER"]
_snpeff_effect = ["coding_sequence_variant", "chromosome", "coding_sequence_variant",
                  "inframe_insertion", "disruptive_inframe_insertion", "inframe_deletion",
                  "disruptive_inframe_deletion", "downstream_gene_variant", "exon_variant",
                  "exon_loss_variant", "frameshift_variant", "gene_variant",
                  "intergenic_region", "conserved_intergenic_variant", "intragenic_variant",
                  "intron_variant", "conserved_intron_variant", "miRNA", "missense_variant",
                  "initiator_codon_variant", "stop_retained_variant", "rare_amino_acid_variant",
                  "splice_acceptor_variant", "splice_donor_variant", "splice_region_variant",
                  "stop_lost", "5_prime_UTR_premature start_codon_gain_variant", "start_lost",
                  "stop_gained", "synonymous_variant", "start_retained", "stop_retained_variant",
                  "transcript_variant", "regulatory_region_variant", "upstream_gene_variant",
                  "3_prime_UTR_variant", "3_prime_UTR_truncation", "5_prime_UTR_variant",
                  "5_prime_UTR_truncation", "sequence_feature", "non_coding_exon_variant",
                  "non_coding_transcript_variant", "5_prime_UTR_premature_start_codon_gain_variant"]
_snpeff_warning = ["WARNING_REF_DOES_NOT_MATCH_GENOME", "WARNING_SEQUENCE_NOT_AVAILABLE",
                   "WARNING_TRANSCRIPT_INCOMPLETE", "WARNING_TRANSCRIPT_MULTIPLE_STOP_CODONS",
                   "WARNING_TRANSCRIPT_NO_START_CODON", "ERROR_CHROMOSOME_NOT_FOUND",
                   "ERROR_OUT_OF_CHROMOSOME_RANGE"]

_mutations = {1:"SNP", 2:"Insertion", 3:"Duplication", 4:"Deletion", 5:"Indel",
              6:"Frameshift_variant", 7:"Stop_codon_variant", 8:"Synonymous_variant",
              0:"Undetected", -1:"Error", -9:"Uncovered"}

_Annotation = collections.namedtuple('Annotation', [
    'alt', 'effect', 'impact', 'gene_name', 'gene_id', 'feature_type', 'feature_id',
    'transcript_biotype', 'rank_total', 'HGVS_c', 'HGVS_p', 'cDNA_pos', 'CDS_pos',
    'prot_pos', 'dist_to_feature', 'warnings_errors'])

## Default filters
_default_filter = {
    "pos":[[1, "end"]],
    "qual":0,
    "DP":0,
    "AF":0,
    "TVOTES": 4,
    #"SB":1000000000,
    "impact":["HIGH", "MODERATE", "LOW"],
    "effect":_snpeff_effect,
    "warnings":_snpeff_warning
}

_ambiguous_iupac = {("A",):"A",
                    ("C",):"C",
                    ("G",):"G",
                    ("T",):"T",
                    ("U",):"U",
                    ("A", "G"):"R",
                    ("C", "T"):"Y",
                    ("C", "G"):"S",
                    ("A", "T"):"W",
                    ("G", "T"):"K",
                    ("A", "C"):"M",
                    ("C", "G", "T"):"B",
                    ("A", "G", "T"):"D",
                    ("A", "C", "T"):"H",
                    ("A", "C", "G"):"V",
                    ("A", "C", "T", "G"):"N"
                    }

## Symbols
_gene_sep = ":"

def _parse_ann_field(ann_field):
    val = []
    alt = re.search("^[NATGC]+|", ann_field)[0]
    for ann in re.split(f"^{alt}\||,{alt}\|", ann_field)[1:]:
        sann = ann.split("|")
        sann = [alt] + sann
        sann[15] = sann[15].split(",")
        try:
            val.append(_Annotation(*sann))
        except TypeError:
            continue
    return val


## NOTE: Not necessarily needed. Might be remplaced by a simple list
class SampleList(object):
    """
    List containing all information about samples
    """
    def __init__(self):
        pass

class Observation(_Observation):
    """ 
    """
    def __new__(cls, sample, variant):
        observation = super(Observation, cls).__new__(cls, sample, variant)
        return observation

cdef class VariantIndex(object):
    """ Special container for variant index
    """
#    def __new__(cls, pos, gen_variant, ref, alt, gene=None, aa_variant=None):
#        variant_index = super(VariantIndex, cls).__new__(cls, pos, gen_variant, ref, alt,
#                                                       gene, aa_variant)
#        return variant_index

    def __init__(self, pos, gen_variant, ref, alt, gene=None, aa_variant=None):
        self.pos = pos
        self.gen_variant = gen_variant
        self.ref = ref
        self.alt = alt
        self.gene = gene
        self.aa_variant = aa_variant

    def __hash__(self):
        if self.gene == None:
            gene = ""
        else:
            gene = self.gene
        if self.aa_variant == None:
            aa_variant = ""
        else:
            aa_variant = self.aa_variant
        return(hash(str(self.pos) + self.ref + self.alt + gene + self.gen_variant + aa_variant))

    def __richcmp__(self, other, op):
        if other is str:
            print(other)
        if op == Py_LT:
            return self.pos < other.pos
        elif op == Py_LE:
            return self.pos <= other.pos
        elif op == Py_EQ:
            return self.gen_variant == other.gen_variant and self.aa_variant == other.aa_variant
        elif op == Py_GE:
            return self.pos >= other.pos
        elif op == Py_GT:
            return self.pos > other.pos
        elif op == Py_NE:
            return (self.pos != other.pos or self.ref != other.pos or
                    self.alt != other.alt or self.gene != other.gene)

    cdef tuple parse_gHGVS_tag(self):
        """ Returns mutation type given a HGVS tag
            _mutations = {1:"SNP", 2:"Insertion", 3:"Duplication", 4:"Deletion", 5:"Indel",
                          6:"Frameshift_variant", 7:"Stop_codon_variant", 8:"Synonymous_variant",
                          0:"Undetected", -1:"Error", -9:"Uncovered"}
        """
        if self.gen_variant is None:
            return 0
        ## Get mutation type
        if re.search(r"ins", self.gen_variant):
            mutation_type = 2
        elif re.search(r"dup", self.gen_variant):
            mutation_type = 3
        elif re.search(r"del", self.gen_variant):
            mutation_type = 4
        elif re.search(r">", self.gen_variant):
            mutation_type = 1
        else:
            mutation_type = -1
        ## Get position in protein
        pos = re.search(r"[0-9]+", self.gen_variant).group(0)
        return (pos, mutation_type)

    cdef tuple parse_pHGVS_tag(self):
        """ Returns mutation type given a HGVS tag
            _mutations = {1:"SNP", 2:"Insertion", 3:"Duplication", 4:"Deletion", 5:"Indel",
                          6:"Frameshift_variant", 7:"Stop_codon_variant", 8:"Synonymous_variant",
                          0:"Undetected", -1:"Error", -9:"Uncovered"}
        """
        if self.aa_variant is None:
            return (-1,-1)
        #aa_tri_letter_code = re.compile(r"[A-Z]{1}[a-z]{2}")
        ## Get mutation type
        if re.search(r"ins", self.aa_variant):
            mutation_type = 2
        elif re.search(r"dup", self.aa_variant):
            mutation_type = 3
        elif re.search(r"del", self.aa_variant):
            mutation_type = 4
        elif re.search(r"insdel", self.aa_variant):
            mutation_type = 5
        elif re.search(r"fs", self.aa_variant):
            mutation_type = 6
        elif re.search(r"\*", self.aa_variant):
            mutation_type = 7
        elif re.search(r"p.[A-Z]{1}[a-z]{2}[0-9]+[A-Z]{1}[a-z]{2}", self.aa_variant):
            ref = re.search(r"[A-Z]{1}[a-z]{2}", self.aa_variant).group(0)
            if self.aa_variant.count(ref) == 2:
                mutation_type = 8
            else:
                mutation_type = 1
        else:
            mutation_type = -1
        ## Get position in protein
        pos = re.search(r"[0-9]+", self.aa_variant).group(0)
        return (pos, mutation_type)

    @property
    def p_pos(self):
        return self.parse_pHGVS_tag()[0]
    @property
    def g_pos(self):
        return self.parse_gHGVS_tag()[0]
    @property
    def p_mutation(self):
        return self.parse_pHGVS_tag()[1]
    @property
    def g_mutation(self):
        return self.parse_gHGVS_tag()[1]

cdef class VariantList(object):
    """
    List containing indexes of all variants.
    References _VariantIndex namedtuple
    """
    def __init__(self):
        ## List containing all _VariantIndex
        self._variant_list = set()
        ## Set containing variants identifiers
        self._pos = set()
        self._gen_variants = set()
        self._genes = set()
        self._aa_variants = set()
        ## List of stuff
        self._pos2var = {}
        self._pos2ref = {}
        self._aa_var2mutation_type = {}
        self._gen_var2mutation_type = {}

    def __getitem__(self, key):
        pos, gen_variant, ref, alt, gene, aa_variant = key
        gene = str(gene) if gene is not None else None
        aa_variant = str(aa_variant) if aa_variant is not None else None
        variant_index = VariantIndex(int(pos), str(gen_variant), ref, alt, gene, aa_variant)
        if variant_index in self._variant_list:
            return variant_index
        else:
            logger.info("No such variant: %s"%(str(variant_index)))
            return None

    def __iter__(self):
        return iter(self._variant_list)

    def __len__(self):
        return len(self._variant_list)

    def __contains__(self, key):
        return key in self._variant_list

    def __str__(self):
        outstr = ""
        out = {}
        for variant in self._variant_list:
            if variant.pos not in out:
                out[variant.pos] = {}
            if variant.gen_variant not in out[variant.pos]:
                out[variant.pos][variant.gen_variant] = []
            out[variant.pos][variant.gen_variant].append((variant.gene, variant.aa_variant))
        for pos in sorted(out):
            outstr += "%s:\n"%(pos)
            for gen_variant in out[pos]:
                outstr += "\t%s\n"%(gen_variant)
                for aa_var in out[pos][gen_variant]:
                    outstr += "\t\t%s, %s\n"%(aa_var[0], aa_var[1])
        return outstr

    cdef void append(self, int pos, str gen_variant, str ref, str alt, str gene=None, str aa_variant=None):
        """ Add to variant list
        """
        ## Feed variant identifiers to the sets
        self._pos.add(pos)
        self._gen_variants.add(gen_variant)
        if gene is not None:
            self._genes.add(gene)
        if aa_variant is not None:
            self._aa_variants.add(aa_variant)
        variant_index = VariantIndex(pos, gen_variant, ref, alt, gene, aa_variant)
        ## Add in self
        self._variant_list.add(variant_index)

    cdef list subset(self, int pos=0, str gen_variant=None, str gene=None, str aa_variant=None):
        """ Return a list of _VariantIndex
        """
        ## Set default values
        if pos is None:
            pos = self._pos
        if gen_variant is None:
            gen_variant = self._gen_variants
        if gene is None:
            gene = self._genes
        if aa_variant is None:
            aa_variant = self._aa_variants
        ## Put values in sets
        if not isinstance(pos, set) and not isinstance(pos, range) and not isinstance(pos, list):
            pos = set([pos])
        if not isinstance(gen_variant, set) and not isinstance(gen_variant, list):
            gen_variant = set([gen_variant])
        if not isinstance(gene, set) and not isinstance(gene, list):
            gene = set([gene])
        if not isinstance(aa_variant, set) and not isinstance(aa_variant, list):
            aa_variant = set([aa_variant])
        return [variant_index for variant_index in self
                if variant_index.pos in pos
                and variant_index.gen_variant in gen_variant
                and variant_index.gene in gene
                and variant_index.aa_variant in aa_variant]

    cdef void make_aa_var2mutation_type_list(self):
        """ Make list of amino acids with mutation types
        """
        for variant in self:
            self._aa_var2mutation_type[variant.aa_variant] = variant.p_mutation

    cdef void make_gen_var2mutation_type_list(self):
        """ Make list of amino acids with mutation types
        """
        for variant in self:
            self._gen_var2mutation_type[variant.gen_variant] = variant.g_mutation

    cdef void print_aa_var2mutation_type_list(self):
        """ Print list of amino acid variants
        """
        pass

    @property
    def pos2var(self):
        """ Make a dict of variants per position
        """
        if self._pos2var == {}:
            for variant in self:
                if variant.pos not in self._pos2var:
                    self._pos2var[variant.pos] = set()
                self._pos2var[variant.pos].add(variant)
        return self._pos2var
    @property
    def pos2ref(self):
        """ Make a dict of variants per position
        """
        if self._pos2ref == {}:
            for var in self:
                if var.pos not in self._pos2ref and var.g_mutation == 1:
                    self._pos2ref[var.pos] = var.ref
        return self._pos2ref
    @property
    def sorted_pos(self):
        """ Return the list of positions
        """
        return sorted(list(self._pos))
    @property
    def sorted_gen_variants(self):
        """ Return the list of genomic variants
        """
        return sorted(list(self._gen_variants))
    @property
    def sorted_genes(self):
        """ Return the list of genomic genes
        """
        return sorted(list(self._genes))
    @property
    def sorted_aa_variants(self):
        """ Return the list of amino acid variants
        """
        return sorted(list(self._aa_variants))
    @property
    def pos(self):
        """ Return the list of positions
        """
        return self._pos
    @property
    def gen_variants(self):
        """ Return the list of genomic variants
        """
        return self._gen_variants
    @property
    def genes(self):
        """ Return the list of genomic genes
        """
        return self._genes
    @property
    def aa_variants(self):
        """ Return the list of amino acid variants
        """
        return self._aa_variants

    @property
    def aa_variants_non_sym(self):
        """ Return the list of amino acid variants
        """
        aa_variants_non_sym = [variant for variant in self if variant.aa_variant is not None]
        return set([variant for variant in aa_variants_non_sym if variant.p_mutation != 8])

cdef class VariantMatrix(object):
    """ Bluh
    """
    def __init__(self, filters, reference, blacklist, callers=[], mode="mono"):
        ## Dict containing all observations
        self._variant_matrix = {}
        ## Lists of indices
        self._variant_list = VariantList()
        self._sample_list = set()
        ## Black list
        self._blacklist = blacklist
        ## Filters
        self._filters = filters
        ## Content
        self._var2smpl = {}
        self._aa_var2smpl = {}
        self._gen_var2smpl = {}
        self._gene2smpl = {}
        self._smpl2var = {}
        self._smpl2aa_var = {}
        self._smpl2gen_var = {}
        ## Uncovered sites
        self._uncovered = set()
        ## Filtered entry
        self._filtered = set()
        ## Additional information
        self._gene_info = {}
        self._gene_location = []
        self._smpl2gene_uncov_sites = pandas.DataFrame()
        self._smpl2gene_depth_sites = pandas.DataFrame()
        self._smpl2gene_cov = pandas.DataFrame()
        self._smpl2gene_depth = pandas.DataFrame()
        self._callers = callers
        self._mode = mode
        ## Timing
        self._timing = []
        ## Parse gff file
        self.get_info_from_gff(reference)

    def __getitem__(self, key):
        return self._variant_matrix[key]

    def __setitem__(self, key, value):
        self._variant_matrix[key] = value

    def __iter__(self):
        return iter(self._variant_matrix)

    def __len__(self):
        return len(self._variant_matrix)

    cpdef list keys(self):
        return list(self._variant_matrix.keys())

    cpdef str print_stats(self, bint filtered=False):
        """ Print stats about the matrix
        """
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        n_smp = len(self.sample_list)
        n_obs = len(subset)
        n_gen_var = len(set([observation.variant.gen_variant for observation in subset]))
        n_gen_var_per_smp = n_obs/n_smp
        n_codon_var = len(set([observation.variant.gen_variant for observation in subset
                               if observation.variant.aa_variant is not None]))
        codon_var = set([observation for observation in subset
                         if observation.variant.aa_variant is not None])
        n_codon_var_per_smp = len(codon_var)/n_smp
        n_aa_var = len(set([observation.variant.aa_variant for observation in codon_var
                            if observation.variant.p_mutation != 8]))
        n_aa_var_per_smp = len(set([observation for observation in codon_var
                                    if observation.variant.p_mutation != 8]))/n_smp
        outstr = " This matrix contains:\n"
        outstr += "N samples                        : %s\n"%(n_smp)
        outstr += "N observations                   : %s\n"%(n_obs)
        outstr += "N genomic variants               : %s\n"%(n_gen_var)
        outstr += "N codon variants                 : %s\n"%(n_codon_var)
        outstr += "N amino acid variants            : %s\n"%(n_aa_var)
        outstr += "N genomic variants per sample    : %s\n"%(n_gen_var_per_smp)
        outstr += "N codon variants per sample      : %s\n"%(n_codon_var_per_smp)
        outstr += "N amino acid variants per sample : %s\n"%(n_aa_var_per_smp)
        return outstr

    ###
    ## Filters
    ##

    cpdef void reset_filter(self):
        self._filtered = set()

    cpdef set filter_max(self, int n_votes, list excl_caller=[]):
        """ Filter records based on number of votes
        """
        caller_idx = [self._callers.index(caller) for caller in self._callers
                      if caller not in excl_caller]
        filtered = set()
        n_votes -= len(excl_caller)
        for observation in self:
            record = self[observation]
            votes = record.format("VOTES").tolist()[0]
            votes = [votes[i] for i in caller_idx]
            if sum(votes) >= n_votes:
                filtered.add(observation)
        if len(self._filtered) == 0:
            self._filtered = filtered
        else:
            self._filtered = self._filtered.intersection(filtered)
        return self._filtered

    cpdef set filter_maj(self, int n_votes, list excl_caller=[]):
        """ Filter records based on number of votes
        """
        filtered = set()
        thresholds = {}
        caller_idx = [self._callers.index(caller) for caller in self._callers
                      if caller not in excl_caller]
        for sample in self.sample_list:
            thresholds[sample] = {}
            var_sub = [self[obs] for obs in self if obs.sample == sample]
            indels = []
            snps = []
            for record in var_sub:
                ## NOTE: Note sure this work that way with a multi sample VCF...
                votes = record.format("VOTES").tolist()[0]
                votes = [votes[i] for i in caller_idx]
                if (sum(votes) >= n_votes - len(excl_caller)):
                    if record.INFO["TYPE"] == "indel":
                        indels.append(record)
                    elif record.INFO["TYPE"] == "snp":
                        snps.append(record)
            ##
            if len(indels) != 0:
                list_of_srank_indels = []
                for indel in indels:
                    list_of_srank_indels.append(sum([indel.format("RANKS")[0][i] for i in caller_idx]))
                s_indel = max(list_of_srank_indels)
            else:
                s_indel = 0
            if len(snps) != 0:
                list_of_srank_snps = []
                for snp in snps:
                    list_of_srank_snps.append(sum([snp.format("RANKS")[0][i] for i in caller_idx]))
                s_snp = max(list_of_srank_snps)
            else:
                s_snp = 0
            ##
            thresholds[sample]["indel"] = s_indel
            thresholds[sample]["snp"] = s_snp
        for observation in self:
            record = self[observation]
            ranks = [record.format("RANKS")[0][i] for i in caller_idx]
            score = sum(ranks)
            if record.INFO["TYPE"] == "snp":
                if score <= thresholds[observation.sample]["snp"]:
                    filtered.add(observation)
            if record.INFO["TYPE"] == "indel":
                if score <= thresholds[observation.sample]["indel"]:
                    filtered.add(observation)
        #print(thresholds)
        if len(self._filtered) == 0:
            self._filtered = filtered
        else:
            self._filtered = self._filtered.intersection(filtered)
        return self._filtered

    cpdef set filter_n_samples(self, int n_sample, bint invert=False):
        """ Filter record based on number of samples
        """
        if n_sample == 0:
            if len(self._filtered) == 0:
                self._filtered = self.keys()
            return self._filtered
        filtered = set()
        self.make_var2smpl_list()
        for observation in self:
            variant = observation.variant
            if len(self._var2smpl[variant]) >= n_sample and not invert:
                filtered.add(observation)
            elif len(self._var2smpl[variant]) <= n_sample and invert:
                filtered.add(observation)
        if len(self._filtered) == 0:
            self._filtered = filtered
        else:
            self._filtered = self._filtered.intersection(filtered)
        return self._filtered

    cpdef set filter_mutation_type(self, list mutation_types, bint invert=False):
        """ Filter observations based on the mutation type
        """
        filtered = set()
        if not isinstance(mutation_types, list):
            mutation_types = list(mutation_types)
        for observation in self:
            if observation.variant.aa_variant is None:
                continue
            mutation_type = observation.variant.p_mutation
            if mutation_type in mutation_types and not invert:
                filtered.add(observation)
            elif mutation_types not in mutation_types and invert:
                filtered.add(observation)
        if len(self._filtered) == 0:
            self._filtered = filtered
        else:
            self._filtered = self._filtered.intersection(filtered)
        return self._filtered

    ###
    ## Input data
    ##

    cpdef void get_info_from_gff(self, str gfile):
        """ Get information about genes from features in GFF file
        """
        self._pos2genes = {}
        with open("%s"%(gfile), "r") as INGB:
            for record in SeqIO.parse(INGB, "gb"):
                self._reference = record.id
                self._reference_length = len(record.seq)
                for feature in record.features:
                    if feature.type == "CDS":
                        gene = re.sub(r"-|\.|\/", "_", feature.qualifiers["gene"][0])
                        if gene not in self._gene_info:
                            self._gene_info[gene] = {"gene_length":0, "prot_length":0}
                        self._gene_info[gene]["prot_length"] = len(feature.qualifiers["translation"][0])
                        for part in feature.location.parts:
                            self._gene_info[gene]["gene_length"] += len(part)
                        self._gene_info[gene]["feature"] = feature
                        for pos in feature.location+FeatureLocation(feature.location.end, feature.location.end+1):
                            if pos not in self._pos2genes:
                                self._pos2genes[pos] = set()
                            self._pos2genes[pos].add(gene)

    cpdef void add_from_vcf_multi(self, str vcf_path):
        self._mode = "multi"
        vcffile = vcf.Reader(filename=vcf_path)
        self._sample_list = set([sample for sample in vcffile.samples
                                 if sample not in self._blacklist])
        record_list = []
        for record in vcffile:
            pos = <int>record.POS
            ref = record.REF
            if record.is_indel:
                alt = str(list(record.ALT)[0])
            else:
                alt_tuple = tuple([str(allele) for allele in record.ALT])
                alt = _ambiguous_iupac[tuple(alt_tuple)]
            gen_variant = record.hgvs_tag[0]
            has_aa_var = False
            aa_variants = []
            if "ANN" in record.INFO:
                if not isinstance(record.INFO["ANN"], bool):
                    for annotation in record.INFO["ANN"]:
                        if len((set(_default_filter["warnings"]).intersection(
                                set(annotation.warnings_errors)))) == 0:
                            if annotation.HGVS_p:
                                gene = re.sub(r"-|\.|\/", "_", annotation.gene_name)
                                aa_variant = "%s:%s"%(gene, annotation.HGVS_p)
                                variant_index = VariantIndex(pos, gen_variant, ref, alt, gene=gene, aa_variant=aa_variant)
                                aa_variants.append(variant_index)
            for sample in self.sample_list:
                if record.get_samp_field_data(sample, "TVOTES") == None and record.get_samp_field_data(sample, "VOTES") == None:
                    continue
                if len(aa_variants) != 0:
                    for aa_variant_index in aa_variants:
                        gene = aa_variant_index.gene
                        aa_variant = aa_variant_index.aa_variant
                        self._variant_list.append(pos, gen_variant, ref, alt, gene, aa_variant)
                        observation = _Observation(sample, aa_variant_index)
                        self[observation] = record
                else:
                    variant_index = VariantIndex(pos, gen_variant, ref, alt, None, None)
                    self._variant_list.append(pos, gen_variant, ref, alt)
                    observation = Observation(sample, variant_index)
                    self[observation] = record

    cpdef void add_from_vcf(self, str vcf_path, str sample=None):
        """ Parse VCF files into the matrix. Compatible with PyVCF only
        """
        self._mode = "mono"
        vcffile = cyvcf2.Reader(vcf_path)
        ## Add all samples in the vcf file into the sample list
        if sample is None:
            sample = vcffile.samples[0]
        ## Loop through the records
        if sample in self._blacklist:
            logger.info("Sample %s in blacklist. Skipping."%(sample))
            return

        logger.info("%s - Process sample: %s"%(self._reference, sample))
        self._sample_list.add(sample)
        i=0
        tic = time.clock()
        elapsed_step = 0
        for record in vcffile:
            pos = <int>record.POS
            ref = record.REF
            if record.is_indel:
                alt = str(list(record.ALT)[0])
            else:
                alt_tuple = tuple([str(allele) for allele in record.ALT])
                alt = _ambiguous_iupac[tuple(alt_tuple)]
            gen_variant = record.INFO["tag"]
            has_aa_var = False
            if  record.INFO.get("ANN"):
                if not isinstance(record.INFO["ANN"], bool):
                    all_annotations = _parse_ann_field(record.INFO.get("ANN"))
                    for annotation in all_annotations:
                        if annotation.HGVS_p:
                            if len((set(_default_filter["warnings"]).intersection(
                                    set(annotation.warnings_errors)))) == 0:
                                gene = re.sub(r"-|\.|\/", "_", annotation.gene_name)
                                aa_variant = "%s:%s"%(gene, annotation.HGVS_p)
                                ## Add variant in list
                                self._variant_list.append(pos, gen_variant, ref, alt, gene, aa_variant)
                                variant_index = VariantIndex(pos, gen_variant, ref, alt,
                                                             gene=gene, aa_variant=aa_variant)
                                observation = Observation(sample, variant_index)
                                self[observation] = record
                                has_aa_var = True
                                i+=1
            if not has_aa_var:
                self._variant_list.append(pos, gen_variant, ref, alt)
                variant_index = VariantIndex(pos, gen_variant, ref, alt, None, None)
                observation = Observation(sample, variant_index)
                self[observation] = record
                i+=1
        toc = time.clock()
        self._timing.append(toc - tic)
        logger.info("%s - %.3fs - Added %s variants in sample: %s"%(self._reference, toc-tic, i, sample))

    def prep_corr_matrices(self):
        """ Prepare the correction matrices
        """
        genes = set(self._gene_info.keys()) | set(self.variant_list.sorted_genes)
        self._smpl2gene_uncov_sites = pandas.DataFrame(index=self.sample_list,
                                                       columns=genes)
        for gene in genes:
            for sample in self.sample_list:
                self._smpl2gene_uncov_sites.at[sample, gene] = set()
        self._smpl2gene_depth_sites = pandas.DataFrame(index=self.sample_list,
                                                       columns=genes)
        self._smpl2gene_depth_sites = self._smpl2gene_depth_sites.fillna(0).astype(int)

    cpdef void check_coverage(self, dict cov_file, str sample, bint update=False, int threshold=10):
        """ Check for coverage at site where no variant is found.
            Input file is a two column matrix with position and read depth
        """
        tic = time.clock()
#        ## If the name of the sample is not provided, uses the file name
#        if sample is None:
#            sample = re.sub(r".cov.dat", "", cov_path)
        ## Check if the sample for the given coverage file exists
        if sample not in self.sample_list:
            logger.warning("This file correspond to no sample in the matrix!")
            return 
        ## Take only observation from this samples. Saves a lot of time.
        obs_per_sample = [obs for obs in self if obs.sample == sample]

        ## Update if the sample is not found in the provided matrix
        if sample not in self.smpl2gene_cov.index or update:
            self._smpl2gene_uncov_sites.loc[sample]
            update = True
        if sample not in self.smpl2gene_depth.index or update:
            self._smpl2gene_depth_sites.loc[sample]
            update = True


#        ##
#        if not update:
#            cov_file = set([item for item in cov_file if item[0] in self.variant_list.pos])
        ## Make total location of genes
        gene_locuses = sum([self._gene_info[gene]["feature"].location for gene in self._gene_info])
        elapsed_step = 0
        i = 0
        ## Loop trhough all sites
        for pos, cov in cov_file.items():
            ## Store position of all genes
            tac = time.clock()
            if update:
                if pos not in self._pos2genes:
                    continue
                for gene in self._pos2genes[pos]:
                    i+=1
                    if gene not in self._gene_info:
                        logging.warning("Gene: %s, Sample: %s"%(gene, sample))
                        continue
                    if pos not in self._gene_info[gene]["feature"]:
                        continue
                    if gene not in self.variant_list.genes:
                        continue
                    self._smpl2gene_depth_sites.at[sample, gene] += cov
                    if cov < threshold:
                        self._smpl2gene_uncov_sites.at[sample, gene].add(pos)
            elapsed_step += time.clock() - tac
            ## Store positions of all variants, yet not covered enough
            if (pos in self.variant_list.pos and cov < threshold):
                for variant in self.variant_list.pos2var[pos]:
                    observation = Observation(sample, variant)
                    if observation not in obs_per_sample:
                        self._uncovered.add(observation)
                    else:
                        continue
        toc = time.clock()
        logger.info("%s - %.3fs - Checking coverage of sample: %s"%(self._reference, toc-tic, sample))

    ###
    ## Print matrices
    ##

    def print_csv(self, mat, file_path=None):
        if file_path is not None:
            mat.to_csv(file_path)
        return mat.to_csv(sys.stdout)

    cpdef numpy.ndarray make_smpl2gene_mat(self,  str file_path=None, bint binary=False,
                                          bint filtered=True, bint non_synonymous=False,
                                          bint correct_depth=False, bint correct_cov=False):
        """ Bluh
        """
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        ## Initialize the dataframe
        smpl2gene_mat = pandas.DataFrame(index=self.sample_list,
                                         columns=self.variant_list.genes)
        smpl2gene_mat = smpl2gene_mat.fillna(0).astype(float)
        gen_vars_only = set()
        syn_vars_only = set()
        ## Loop through all observation
        prev_observation = None
        for observation in subset:
            if prev_observation and observation.variant == prev_observation.variant and self._mode == "multi":
                pass
            else:
                record = self[observation]
            if observation.variant.aa_variant is None:
                continue
            if non_synonymous and observation.variant.p_mutation == 8:
                syn_vars_only.add(observation)
                continue
            if not binary:
                smpl2gene_mat.at[observation.sample, observation.variant.gene] += 1
            else:
                smpl2gene_mat.at[observation.sample, observation.variant.gene] = 1
            prev_observation = observation
        ## Correct gene counts in function of coverage of genes
        if correct_cov and not binary:
            for sample in smpl2gene_mat.index:
                for gene in smpl2gene_mat.columns:
                    corr = self._smpl2gene_cov.at[sample, gene]
                    smpl2gene_mat.at[sample, gene] *= abs(corr)
        ## Correct gene counts in function of depth of genes
        if correct_depth and not binary:
            for sample in smpl2gene_mat.index:
                for gene in smpl2gene_mat.columns:
                    corr = self._smpl2gene_depth.at[sample, gene]
                    if corr != 0:
                        norm_corr = abs(1-1/corr)
                    else:
                        norm_corr = abs(0)
                    if norm_corr != 0:
                        smpl2gene_mat.at[sample, gene] /= norm_corr if norm_corr != 0 else 0
                    else:
                        smpl2gene_mat.at[sample, gene] = 0
#                    norm_corr = 1-1/corr if corr != 0 else 0
#                    smpl2gene_mat[gene][sample] /= norm_corr if norm_corr != 0 else 0
        ## Print in file
        if file_path is not None:
            smpl2gene_mat.to_csv(file_path, index_label="sample")
        return smpl2gene_mat.as_matrix()

    cpdef numpy.ndarray make_smpl2gen_var_mat(self, str info, str file_path=None,
                                             bint filtered=False):
        """ Make matrices of genomic variants
        """
        ## Subset
        if filtered:
            subset = self._filtered
            var_list = list(set([observation.variant.gen_variant for observation in self._filtered]))
        else:
            subset = self.keys()
            var_list = self.variant_list.sorted_gen_variants
        ## Initialize dataframe
        smpl2gen_var_mat = pandas.DataFrame(index=self.sample_list,
                                            columns=var_list)
        smpl2gen_var_mat = smpl2gen_var_mat.fillna(0).astype(float)
        ## Fill uncovered observation with -9
        for observation in self._uncovered:
            if observation.variant.gen_variant in var_list:
                smpl2gen_var_mat.set_value(observation.sample, observation.variant.gen_variant, -9)
        ## Loop through all observations
        prev_observation = None
        for observation in subset:
            if prev_observation and observation.variant == prev_observation.variant and self._mode == "multi":
                pass
            else:
                record = self[observation]
            ## Fetch info in the INFO fields
            if info in record.INFO:
                to_out = record.INFO[info]
            ## Fetch info in the FORMAT fields
            elif info in record.FORMAT:
                to_out = record.format(info).tolist()[0]
                if info == "AF":
                    if to_out != None:
                        to_out = to_out[1]
                    else:
                        to_out = -1
            ##
            elif info == "binary":
                to_out = 1
            else:
                to_out = -2
            if isinstance(to_out, list):
                to_out = to_out[0]

            smpl2gen_var_mat.set_value(observation.sample, observation.variant.gen_variant, to_out)
            prev_observation = observation
        ## Print in file
        if file_path is not None:
            smpl2gen_var_mat.to_csv(file_path, index_label="sample")
        return smpl2gen_var_mat.as_matrix()

    cpdef numpy.ndarray make_smpl2aa_var_mat(self, str info, str file_path=None, bint filtered=False,
                                             bint non_synonymous=False):
        """ Make matrices ofamino acid variants
        """
        ## Subset
        if filtered:
            subset = self._filtered
            if non_synonymous:
                var_list = list(set([observation.variant.aa_variant for observation in self._filtered if observation.variant.p_mutation != 8]))
            else:
                var_list = list(set([observation.variant.aa_variant for observation in self._filtered]))
        else:
            subset = self.keys()
            var_list = self.variant_list.sorted_aa_variants
        subset = sorted(subset, key=attrgetter("variant.pos"))
        ## Initialize dataframe
        smpl2aa_var_mat = pandas.DataFrame(index=self.sample_list,
                                           columns=var_list)
        smpl2aa_var_mat = smpl2aa_var_mat.fillna(0).astype(float)
        ## Fill uncovered observation with -9
        for observation in self._uncovered:
            if observation.variant.aa_variant is not None and observation.variant.aa_variant in var_list:
                smpl2aa_var_mat.set_value(observation.sample, observation.variant.aa_variant, -9)
        ## Loop through all observations
        prev_observation = None
        for observation in subset:
            if observation.variant.aa_variant is not None:
                if non_synonymous and observation.variant.p_mutation == 8:
                    continue
                if prev_observation and observation.variant == prev_observation.variant and self._mode == "multi":
                    pass
                else:
                    record = self[observation]
                if info in record.INFO:
                    to_out = record.INFO[info]
                elif info in record.FORMAT:
                    to_out = record.format(info).tolist()[0]
                    if info == "AF":
                        if to_out != None:
                            to_out = to_out[1]
                        else:
                            to_out = None
                elif info == "binary":
                    to_out = 1
                elif info == "score":
                    to_out = record.QUAL
                else:
                    to_out = -2
                if isinstance(to_out, list):
                    to_out = to_out[0]
                smpl2aa_var_mat.set_value(observation.sample, observation.variant.aa_variant, to_out)
                prev_observation = observation
        ## Print in file
        if file_path is not None:
            smpl2aa_var_mat.to_csv(file_path, index_label="sample")
        smpl2aa_var_mat.as_matrix()
        return smpl2aa_var_mat.as_matrix()

    cpdef numpy.ndarray make_smpl2gene_cov_mat(self, str file_path=None):
        """ Make matrice of mean coverage per gene per sample
        """
#        if self._smpl2gene_uncov_sites == {}:
#            logger.error("This function is not available without checking coverage.")
#            return self._smpl2gene_cov
        for sample in self.sample_list:
            if sample not in list(self._smpl2gene_cov.index):
                df = pandas.Series([0]*len(self.variant_list.genes), index=self.variant_list.genes, name=sample)
                self._smpl2gene_cov = self._smpl2gene_cov.append(df)
                for gene in self.variant_list.genes:
                    length = self._gene_info[gene]["gene_length"]
                    uncoverage = len(self._smpl2gene_uncov_sites.at[sample, gene])
                    coverage = 1 - uncoverage/length
                    self._smpl2gene_cov.at[sample, gene] = coverage
        ## Print in file
        if file_path is not None:
            self._smpl2gene_cov.to_csv(file_path, index_label="sample")
        return self._smpl2gene_cov.as_matrix()

    cpdef numpy.ndarray make_smpl2gene_depth_mat(self, str file_path=None):
        """ Make matrice of mean coverage per gene per sample
        """
#        if self._smpl2gene_depth_sites == {}:
#            logger.error("This function is not available without checking coverage.")
#            return self._smpl2gene_depth
        for sample in self.sample_list:
            if sample not in self._smpl2gene_depth.index:
                df = pandas.Series([0]*len(self.variant_list.genes), index=self.variant_list.genes, name=sample)
                self._smpl2gene_depth = self._smpl2gene_depth.append(df)
                for gene in self.variant_list.genes:
                    length = self._gene_info[gene]["gene_length"]
                    depth = self._smpl2gene_depth_sites.at[sample, gene]
                    norm_depth = depth/length
                    self._smpl2gene_depth.at[sample, gene] = norm_depth
        ## Print in file
        if file_path is not None:
            self._smpl2gene_depth.to_csv(file_path, index_label="sample")
        return self._smpl2gene_depth.as_matrix()

    cpdef numpy.ndarray make_alignment(self, str file_path=None, bint filtered=False):
        """ Build sequences from the variants
        """
        ## Subset
        if filtered:
            subset = self._filtered
            pos_list = list(sorted(set([observation.variant.pos for observation in self._filtered
                                        if observation.variant.g_mutation == 1])))
        else:
            subset = self.keys()
            pos_list = self.variant_list.sorted_pos
        ## Initialize dataframe
        alt_mat = pandas.DataFrame(index=self.sample_list,
                                   columns=pos_list)
        alt_mat = alt_mat.fillna("-").astype(str)
        for col in alt_mat:
            alt_mat[col].replace("-", self.variant_list.pos2ref[col], inplace=True)
        ## Fill uncovered observation with N (ambiguous)
        for observation in self._uncovered:
            if observation.variant.pos in pos_list and observation.variant.g_mutation == 1:
                alt_mat[observation.variant.pos][observation.sample] = "N"
        ## Loop through all observations
        for observation in subset:
            if observation.variant.pos is not None and observation.variant.g_mutation == 1:
                alt_mat[observation.variant.pos][observation.sample] = observation.variant.alt
        ## Print in file
        outstr = ""
        for sample in alt_mat.index:
            sequence = ""
            for pos in alt_mat.columns:
                sequence += alt_mat[pos][sample]
            outstr += ">%s\n"%sample
            outstr += "%s\n"%sequence
        if file_path is not None:
            with open("%s.fasta"%file_path, "w") as FASTA:
                FASTA.write(outstr)
        if file_path is not None:
            alt_mat.to_csv(file_path, index_label="sample")
        return alt_mat.as_matrix()

    ###
    ## Make lists
    ##

    cpdef dict make_var2smpl_list(self, bint filtered=False):
        """ Make list  samples per amino acid variants
        """
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        subset = set([observation for observation in subset if observation.variant is not None])
        self._var2smpl = {}
        for observation in subset:
            if observation.variant not in self._var2smpl:
                self._var2smpl[observation.variant] = set()
            self._var2smpl[observation.variant].add(observation.sample)
        return self._var2smpl

    cpdef dict make_aa_var2smpl_list(self, bint filtered=False, bint non_synonymous=False):
        """ Make list  samples per amino acid variants
        """
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        subset = set([observation for observation in subset
                    if observation.variant.aa_variant is not None])
        self._aa_var2smpl = {observation.variant.aa_variant:set() for observation in subset}
        for observation in subset:
            if non_synonymous and observation.variant.p_mutation == 8:
                continue
            self._aa_var2smpl[observation.variant.aa_variant].add(observation.sample)
        return self._aa_var2smpl

    cpdef dict make_gen_var2smpl_list(self, bint filtered=False):
        """ Make list of samples per genomic acid variants
        """
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        subset = set([observation for observation in subset
                      if observation.variant.gen_variant is not None])
        self._gen_var2smpl = {observation.variant.gen_variant:set() for observation in subset}
        for observation in subset:
            self._gen_var2smpl[observation.variant.gen_variant].add(observation.sample)
        return self._gen_var2smpl

    cpdef dict make_gene2smpl_list(self, bint filtered=False):
        """ Make list of samples per genes acid variants
        """
        self._gene2smpl = {}
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        for observation in subset:
            if observation.variant.gene not in self._gene2smpl:
                self._gene2smpl[observation.variant.gene] = set()
            self._gene2smpl[observation.variant.gene].add(observation.sample)
        return self._gene2smpl

    cpdef dict make_smpl2var_list(self, bint filtered=False):
        """ Make list of amino acid variants per sample
        """
        self._smpl2var = {sample:set() for sample in self.sample_list}
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        for observation in subset:
            self._smpl2var[observation.sample].add(observation.variant)
        return self._smpl2var

    cpdef dict make_smpl2aa_var_list(self, bint filtered=False, bint non_synonymous=False):
        """ Make list of amino acid variants per sample
        """
        self._smpl2aa_var = {sample:set() for sample in self.sample_list}
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        for observation in subset:
            if observation.variant.aa_variant is None:
                continue
            if non_synonymous and observation.variant.p_mutation == 8:
                continue
            self._smpl2aa_var[observation.sample].add(observation.variant.aa_variant)
        return self._smpl2aa_var

    cpdef dict make_smpl2gen_var_list(self, bint filtered=False):
        """ Make list of  acid variants per sample
        """
        self._smpl2gen_var = {sample:set() for sample in self.sample_list}
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        for observation in subset:
            self._smpl2gen_var[observation.sample].add(observation.variant.gen_variant)
        return self._smpl2gen_var

    ###
    ## Print lists
    ##

    def print_var2smpl_list(self, filtered=False, file_path=None):
        """ 
        """
        outstr = ""
        self.make_var2smpl_list(filtered=filtered)
        for variant in self._var2smpl:
            outstr += "%s\t%s\t%s\t%s\t%s\t%s\n"%(variant.pos, variant.ref, variant.alt,
                                                  variant.gene, variant.aa_variant,
                                                  len(self._var2smpl[variant]))
        ## Write in file
        if isinstance(file_path, str):
            with open("%s"%(file_path), "w") as LIST:
                LIST.write(outstr)
        else:
            file_path.write(outstr)
        return outstr

    def print_aa_var2smpl_list(self, filtered=False, non_synonymous=False, file_path=None):
        """ Print number of samples per amino acid variant
        """
        outstr = ""
        self.make_aa_var2smpl_list(filtered=filtered, non_synonymous=non_synonymous)
        for aa_variant in sorted(self._aa_var2smpl):
            outstr += "%s\t%s\n"%(aa_variant, len(self._aa_var2smpl[aa_variant]))
        ## Write in file
        if isinstance(file_path, str):
            with open("%s"%(file_path), "w") as LIST:
                LIST.write(outstr)
        else:
            file_path.write(outstr)
        return outstr

    def print_gen_var2smpl_list(self, filtered=False, file_path=None):
        """ Print number of samples per genomic variant
        """
        outstr = ""
        self.make_gen_var2smpl_list(filtered=filtered)
        for gen_variant in sorted(self._gen_var2smpl):
            outstr += "%s\t%s\n"%(gen_variant, len(self._gen_var2smpl[gen_variant]))
        ## Write in file
        if file_path:
            with open("%s"%(file_path), "w") as LIST:
                LIST.write(outstr)
        return outstr

    def print_gene2smpl_list(self, filtered=False, file_path=None):
        """ Print number of samples per genomic variant
        """
        outstr = ""
        self.make_gene2smpl_list(filtered=filtered)
        for gene in sorted(self._gene2smpl):
            outstr += "%s\t%s\n"%(gene, len(self._gene2smpl[gene]))
        ## Write in file
        if file_path:
            with open("%s"%(file_path), "w") as LIST:
                LIST.write(outstr)
        return outstr

    def print_smpl2aa_var_list(self, filtered=False, non_synonymous=False, file_path=None):
        """ Print number of samples per genomic variant
        """
        outstr = ""
        self.make_smpl2aa_var_list(filtered=filtered, non_synonymous=non_synonymous)
        for gen_variant in sorted(self._smpl2aa_var):
            outstr += "%s\t%s\n"%(gen_variant, len(self._smpl2aa_var[gen_variant]))
        ## Write in file
        if file_path:
            with open("%s"%(file_path), "w") as LIST:
                LIST.write(outstr)
        return outstr

    def print_smpl2gen_var_list(self, filtered=False, file_path=None):
        """ Print number of samples per genomic variant
        """
        outstr = ""
        self.make_smpl2gen_var_list(filtered=filtered)
        for gen_variant in sorted(self._smpl2gen_var):
            outstr += "%s\t%s\n"%(gen_variant, len(self._smpl2gen_var[gen_variant]))
        ## Write in file
        if file_path:
            with open("%s"%(file_path), "w") as LIST:
                LIST.write(outstr)
        return outstr

    ###
    ## Summaries
    ##
    cpdef numpy.ndarray make_gen_variant_summary(self, bint filtered=False, str file_path=None):
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        items = ["total", "missense", "indel"]
        self._gen_variant_summary = pandas.DataFrame(index=self.sample_list,
                                                     columns=items)
        self._gen_variant_summary = self._gen_variant_summary.fillna(0).astype(int)
        for observation in subset:
            if observation.variant.gen_variant is None:
                continue
            sample = observation.sample
            mutation_type = observation.variant.g_mutation
            if (mutation_type == 1):
                temp_val = self._gen_variant_summary.get_value(sample, "missense") + 1
                self._gen_variant_summary.set_value(sample, "missense", temp_val)
            elif (mutation_type in [2, 3, 4, 5]):
                temp_val = self._gen_variant_summary.get_value(sample, "indel") + 1
                self._gen_variant_summary.set_value(sample, "indel", temp_val)
            temp_val = self._gen_variant_summary.get_value(sample, "total") + 1
            self._gen_variant_summary.set_value(sample, "total", temp_val)
        if file_path:
            self._gen_variant_summary.to_csv(file_path, sep="\t", index_label="sample")
        return self._gen_variant_summary.as_matrix()

    cpdef numpy.ndarray make_aa_variant_summary(self, bint filtered=False, str file_path=None):
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        items = ["total", "synonymous", "missense", "indel", "disruptive"]
        self._aa_variant_summary = pandas.DataFrame(index=self.sample_list,
                                                    columns=items)
        self._aa_variant_summary = self._aa_variant_summary.fillna(0).astype(int)
        for observation in subset:
            if observation.variant.aa_variant is None:
                continue
            aa_variant = observation.variant.aa_variant
            sample = observation.sample
            mutation_type = observation.variant.p_mutation
            if (mutation_type == 8):
                temp_val = self._aa_variant_summary.get_value(sample, "synonymous") + 1
                self._aa_variant_summary.set_value(sample, "synonymous", temp_val)
            elif (mutation_type == 1):
                temp_val = self._aa_variant_summary.get_value(sample, "missense") + 1
                self._aa_variant_summary.set_value(sample, "missense", temp_val)
            elif (mutation_type in [2, 3, 4, 5]):
                temp_val = self._aa_variant_summary.get_value(sample, "indel") + 1
                self._aa_variant_summary.set_value(sample, "indel", temp_val)
            elif (mutation_type in [6, 7]):
                temp_val = self._aa_variant_summary.get_value(sample, "disruptive") + 1
                self._aa_variant_summary.set_value(sample, "disruptive", temp_val)
            temp_val = self._aa_variant_summary.get_value(sample, "total") + 1
            self._aa_variant_summary.set_value(sample, "total", temp_val)
        if file_path:
            self._aa_variant_summary.to_csv(file_path, sep="\t", index_label="sample")
        return self._aa_variant_summary.as_matrix()

    cpdef numpy.ndarray make_gene_summary(self, bint filtered=False):
        self._gene_list = {}
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        items = ["total", "synonymous", "missense", "indel", "disruptive"]
        self._gene_summary = pandas.Panel(items=items,
                                          minor_axis=self.sample_list,
                                          major_axis=self.variant_list.sorted_genes)
        self._gene_summary = self._gene_summary.fillna(0).astype(int)
        for observation in subset:
            if observation.variant.aa_variant is None:
                continue
            gene_name = observation.variant.gene
            sample = observation.sample
            if (observation.variant.p_mutation == 8):
                temp_val = self._gene_summary.get_value("synonymous", gene_name, sample) + 1
                self._gene_summary.set_value("synonymous", gene_name, sample, temp_val)
            elif (observation.variant.p_mutation == 1):
                temp_val = self._gene_summary.get_value("missense", gene_name, sample) + 1
                self._gene_summary.set_value("missense", gene_name, sample, temp_val)
            elif (observation.variant.p_mutation in [2, 3, 4, 5]):
                temp_val = self._gene_summary.get_value("indel", gene_name, sample) + 1
                self._gene_summary.set_value("indel", gene_name, sample, temp_val)
            elif (observation.variant.p_mutation in [6, 7]):
                temp_val = self._gene_summary.get_value("disruptive", gene_name, sample) + 1
                self._gene_summary.set_value("disruptive", gene_name, sample, temp_val)
            temp_val = self._gene_summary.get_value("total", gene_name, sample) + 1
            self._gene_summary.set_value("total", gene_name, sample, temp_val)
        return self._gene_summary.as_matrix()

    def print_smpl2gene_summaries(self, filtered=False, file_prefix=None):
        if self._gene_summary is None:
            self.make_gene_summary()
        for category in ["synonymous", "missense", "indel", "disruptive"]:
            self._gene_summary[category].to_csv("%s.%s.dat"%(file_prefix, category), sep="\t",
                                                index_label="sample")

    def print_gene_summary(self, filtered=False, file_path=None):
        self.make_gene_summary(filtered=filtered)
        outstr = "gene\tprot_size\ttotal\tsynonymous\tmissense\tindel\tdisruptive"
        tic = time.clock()
        for gene in self._gene_summary.major_axis:
            line = "%s\t%s\t"%(gene, self._gene_info[gene]["prot_length"])
            vals = collections.OrderedDict()
            for sample in self._gene_summary.minor_axis:
                for var_type in self._gene_summary.items:
                    if var_type not in vals:
                        vals[var_type] = 0
                    vals[var_type] += self._gene_summary[var_type][sample][gene]
            line += "\t".join([str(val) for val in list(vals.values())])
            outstr = "%s\n%s"%(outstr,line)
        elapsed = time.clock() - tic
        if file_path:
            with open("%s"%(file_path), "w") as LIST:
                LIST.write(outstr)
        return outstr

    ###
    ## Sliding window
    ##

    cpdef dict make_pos_sliding_window(self, int step, int window, bint filtered=False):
        """ Fit all observed variants into a sliding window
        """
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        ## Create windows
        windows = {}
        cdef int i = 0
        cdef step_i
        while i < self._reference_length:
            windows[int(i)] = {"SNP":0, "INDEL":0}
            i += step
        for observation in subset:
            pos = int(observation.variant.pos)
            mutation = observation.variant.g_mutation
            step_i = pos-window-pos%step+step
            if step_i < 0:
                step_i = 0
            while pos > step_i and pos < step_i + window:
                if step_i > self._reference_length:
                    break
                if mutation == 1:
                    windows[step_i]["SNP"] += 1
                elif mutation in [2, 4]:
                    windows[step_i]["INDEL"] += 1
                step_i += step
        return windows

    cpdef numpy.ndarray make_pos_sliding_window_per_sample(self, int step, int window,
                                                            bint filtered=False, str file_path=None):
        """ Fit all observed variants into a sliding window
        """
        ## Subset
        if filtered:
            subset = self._filtered
        else:
            subset = self.keys()
        ## Create windows
        windows = pandas.DataFrame(index=range(1, self._reference_length, step),
                                   columns=self.sample_list)
        windows = windows.fillna(0)
        cdef int step_i
        cdef int temp_val
        for observation in subset:
            pos = int(observation.variant.pos)
            mutation = observation.variant.g_mutation
            step_i = pos-window-pos%step+step+1
            if step_i < 1:
                step_i = 1
            while pos > step_i and pos < step_i + window:
                if step_i > self._reference_length:
                    break
                temp_val = windows.get_value(step_i, observation.sample)
                temp_val += 1
                windows.set_value(step_i, observation.sample, temp_val)
                step_i += step
        if file_path is not None:
            windows.to_csv(file_path)
        return windows.as_matrix()

    def print_pos_sliding_window(self, step, window, file_path=None, filtered=False):
        """ Blug
        """
        windows = self.make_pos_sliding_window(step, window, filtered=filtered)
        outstr = ""
        for step in windows:
            outstr += "%s\t%s\t%s\n"%(step, windows[step]["SNP"], windows[step]["INDEL"])
        if file_path:
            with open("%s"%(file_path), "w") as LIST:
                LIST.write(outstr)
        return outstr

    @property
    def variant_list(self):
        return self._variant_list

    @property
    def sample_list(self):
        return sorted(list(self._sample_list))

    @property
    def gene_location(self):
        return self._gene_location

    @property
    def gene_info(self):
        return self._gene_info

    @property
    def smpl2gene_cov(self):
        return self._smpl2gene_cov
    @smpl2gene_cov.setter
    def smpl2gene_cov(self, smpl2gene_cov):
        self._smpl2gene_cov = smpl2gene_cov

    @property
    def smpl2gene_depth(self):
        return self._smpl2gene_depth
    @smpl2gene_depth.setter
    def smpl2gene_depth(self, smpl2gene_depth):
        self._smpl2gene_depth = smpl2gene_depth

    @gene_location.setter
    def gene_location(self, key):
        self._gene_location = key
    @property
    def filtered(self):
        return self._filtered

def compare_calls(matrix1, matrix2, ref1, ref2, coord_map={}, prefilter=0, file_path=None):
    """ Compare called aa_variants
    """
    with open("%s"%(file_path), "w") as FILE:
        ## Get the list of samples per variants
        var2smpl1 = copy.copy(matrix1.make_var2smpl_list(filtered=True))
        var2smpl2 = copy.copy(matrix2.make_var2smpl_list(filtered=True))
        ## Union of all variants
        all_keys = set(matrix1.variant_list).union(set(matrix2.variant_list))
        ## Loop through the union of all keys
        for key in all_keys:
            if key.aa_variant is None:
                continue
            ## If a variant against ref1 has an equivalent in ref2.
            ## E.g: Leu343Ala and Ala343Leu
            alt_tag = key
            ref_variant = False
            if key.p_mutation in [1]:
                pos = key.p_pos
                ref = re.findall(r"[A-Z]{1}[a-z]{2}", key.aa_variant)[0]
                alt = re.findall(r"[A-Z]{1}[a-z]{2}", key.aa_variant)[1]
                if key.gene in coord_map:
                    if key.p_pos in coord_map[key.gene]:
                        pos = coord_map[key.gene][key.p_pos]
                alt_tag = "%s:p.%s%s%s"%(key.gene, alt, pos, ref)
                if alt_tag in var2smpl2 and key in var2smpl1 and alt != ref:
                    var2smpl1[key].extend(var2smpl2[alt_tag])
                    var2smpl2[alt_tag].extend(var2smpl1[key])
                    ref_variant = True
                else:
                    ref_variant = False
            ## Control if the sets are empty, assign empty list if so.
            list1 = var2smpl1[key] if (key in var2smpl1) else []
            if ref_variant:
                list2 = var2smpl2[alt_tag] if (alt_tag in var2smpl2) else []
            else:
                list2 = var2smpl2[key] if (key in var2smpl2) else []
            ## Get intersection of set1 and set2
            ref1_inter_ref2 = set(list1).intersection(list2)
            ## Get keys unique to set1 anmd set2, respectively
            ref1_only = set(list1) - set(ref1_inter_ref2)
            ref2_only = set(list2) - set(ref1_inter_ref2)
            if len(list1) >= prefilter and len(list2) >= prefilter:
                FILE.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\n"%(key.gene, key.aa_variant, len(ref1_only),
                                                           len(ref1_inter_ref2), len(ref2_only),
                                                           str(ref_variant), _mutations[key.p_mutation]))
