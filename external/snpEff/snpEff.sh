#!/bin/bash

cmd=$1
if [[ ${cmd} == "" ]]; then
	cmd="ann"
fi
shift
args=$@

SNPEFF_DIR="/home/loetscal/software/snpEff"

java -Xmx4G -jar ${SNPEFF_DIR}/snpEff.jar ${cmd} -c ${SNPEFF_DIR}/snpEff.config ${args}

#java -Xmx4G -jar ${SNPEFF_DIR}/snpEff.jar -c ${SNPEFF_DIR}/snpEff.config ${args}
