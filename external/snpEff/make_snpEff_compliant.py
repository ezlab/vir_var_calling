#!/usr/bin/python3

"""
-- gb2fasta.py is part of the VVC pipeline

Convert a genbank file into a fasta file
"""

__author__  = "Alexis Loetscher"
__date__    = "June 2016"
__copyright__ = "" 

__status__ = "Prototype"

import sys
import os
import re
import argparse
import collections
import Bio
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation


###
## MAIN
def main():
    parser = argparse.ArgumentParser(description='Extract sequence from a fasta file and a list of intervals.')

    parser.add_argument('-g', '--genbankfile', dest='genbankfile',
                        help='Genbank file',
                        metavar='FILE', required=False)

    opts = parser.parse_args()

    ## Open a genbank file
    with open(opts.genbankfile, "r") as GB:
        GB = open("%s"%opts.genbankfile, "r")
        records = Bio.SeqIO.parse(GB, "genbank")
        records = list(records)[0]

        new_record = SeqRecord(
                records.seq,
                name=records.id,
                id=records.id,
                annotations=records.annotations,
                dbxrefs=records.dbxrefs
                )
        for feature in records.features:
            if (feature.type == "gene" or feature.type == "CDS"):
                feature.qualifiers["locus_tag"] = feature.qualifiers["gene"]
            if(feature.type=="mRNA"):
                continue
            new_record.features.append(feature)
        NEWGB = open("test.gbk", "w")
        SeqIO.write(new_record, NEWGB, "genbank")
    NEWGB.close()

if (__name__ == "__main__"):
    main()
