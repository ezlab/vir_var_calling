#!/usr/bin/python
# Last modified: Tue 29 Nov 2016 06:33:34 PM CET
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
from Bio.SeqFeature import FeatureLocation

__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

FORMAT = '[%(asctime)-15s] %(levelname)s:%(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger()

###-----------------------------------------------------------
## MAIN
##
if (__name__ == "__main__"):
    parser = argparse.ArgumentParser(description="")

    parser.add_argument('-f', '--file', dest='gbfile',  help='Genbank file', metavar='FILE', required=True)
    parser.add_argument('-o', '--outfile', dest='outfile',  help='Output genbank file', metavar='FILE', required=True)

    opts = parser.parse_args()

    gene_name = "LMP-2A"

    try:
        GB = open("%s"%(opts.gbfile), "r")
        gbparser = SeqIO.parse(GB, "genbank")
        for record in gbparser:
            for feature in record.features:
                if feature.type == "CDS":
                    if feature.qualifiers["gene"][0] == gene_name:
                        new_seq = feature.extract(record.seq)
                        new_feature = feature
                if feature.type == "source":
                    new_source = feature
            new_record = SeqRecord(new_seq, id="%s_%s"%(record.id, gene_name), name="%s_%s"%(record.id, gene_name), 
                                   description=".", annotations=record.annotations)
            new_source.location = FeatureLocation(0, len(new_seq), strand = 1)
            new_record.features.append(new_source)
            new_record.features.append(new_feature)
            #print(new_record)
    except (FileNotFoundError, IOError) as e:
        logging.info("Could not open %s in reading mode"%(opts.gbfile), e)
    GB.close()

    try:
        OUT = open("%s"%(opts.outfile), "w")
        SeqIO.write(new_record, OUT, "genbank")
    except (FileNotFoundError, IOError) as e:
        logging.info("Could not open %s in reading mode"%(opts.outfile), e)
    OUT.close()
