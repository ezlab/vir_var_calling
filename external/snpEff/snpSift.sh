#!/usr/bin/bash

cmd=$1
if [[ ${cmd} == "" ]]; then
	cmd="concordance"
fi
shift
args=$@
echo ${args}

SNPEFF_DIR="/home/cegg/loetscha/software/snpEff"

java -Xmx4G -jar ${SNPEFF_DIR}/SnpSift.jar ${cmd} ${args}

#java -Xmx4G -jar ${SNPEFF_DIR}/snpEff.jar -c ${SNPEFF_DIR}/snpEff.config ${args}
