#!/usr/bin/python
## Last modified: Fri 31 Mar 2017 10:36:27 AM CEST
"""
Write doc here
"""

#pylint: disable-msg=W0401,W0611,R0902
from __future__ import print_function
import argparse
import math
import random
import re
import os
import sys
import collections
import logging
import json
import datetime
import time
import copy
from contextlib import suppress

try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

try:
    import pysam
except ImportError:
    pysam = None

try:
    import cparse
except (ImportError,SystemError):
    cparse = None
#cparse = None

import vcf
from vcf.parser import _vcf_metadata_parser
from vcf.model import _Call, _Record, make_calldata_tuple
from vcf.model import _Substitution, _Breakend, _SingleBreakend, _SV
from vcf.constants import *


__author__ = "Alexis Loetscher"
__email__ = "alexis.loetscher@unige.ch"
__credits__ = ["Alexis Loetscher"]

__date__ = ""
__version__ = ""
__copyright__ = ""
__license__ = ""

__status__ = "Prototype"

SINGULAR_METADATA = ["fileformat", "fileDate", "reference"]
MANDATORY_FIELDS = ["CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER"]

_Info = collections.namedtuple('Info', ['id', 'num', 'type', 'desc', 'source', 'version'])
_Filter = collections.namedtuple('Filter', ['id', 'desc'])
_Alt = collections.namedtuple('Alt', ['id', 'desc'])
_Format = collections.namedtuple('Format', ['id', 'num', 'type', 'desc'])
_SampleInfo = collections.namedtuple('SampleInfo', ['samples', 'gt_bases', 'gt_types', 'gt_phases'])
_Sample = collections.namedtuple('Sample', ['id', 'genomes', 'mixture', 'description'])
_Contig = collections.namedtuple('Contig', ['id', 'length'])

class Container(object):
    """ Contains _Records objects """
    def __init__(self, reader=None, fileformat=None, reference=None, skip_invariant=False):
        """"""
        self._record_list = []
        self.metadata = collections.OrderedDict()
        self.infos = collections.OrderedDict()
        self.formats = collections.OrderedDict()
        self.alts = {}
        self.samples = []
        self.sampleInfo = collections.OrderedDict()
        self.filters = {}
        self.contigs = collections.OrderedDict()
        self._sample_indexes = {}
        self._column_headers = ["CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "FORMAT"]
        self._format_cache = {}
        self._header_lines = []
        self._sample_indexes = {}
        if reader:
            self.import_all(reader, skip_invariant=skip_invariant)

        if fileformat:
            self.metadata["fileformat"] = fileformat
        if reference:
            self.metadata["reference"] = reference
        self.metadata["fileDate"] = str(datetime.datetime.now())

    def __contains__(self, record):
        return record in self._record_list

    def __iter__(self):
        return iter(self._record_list)

    def __getitem__(self, key):
        return self._record_list[key]

    def __delitem__(self, key):
        del self._record_list[key]

    def __len__(self):
        return len(self._record_list)

    def __contains__(self, item):
        return item in self._record_list

    def __next__(self):
        return next(self._record_list)

    def _parse_sample_format(self, samp_fmt):
        """ Parse the format of the calls in this _Record """
        samp_fmt = make_calldata_tuple(samp_fmt.split(':'))

        for fmt in samp_fmt._fields:
            try:
                entry_type = self.formats[fmt].type
                entry_num = self.formats[fmt].num
            except KeyError:
                entry_num = None
                try:
                    entry_type = RESERVED_FORMAT[fmt]
                except KeyError:
                    entry_type = 'String'
            samp_fmt._types.append(entry_type)
            samp_fmt._nums.append(entry_num)
        return samp_fmt

    def append(self, record):
        #TODO: Append the line between two existing position
        for field in MANDATORY_FIELDS:
            if field not in dir(record):
                raise NotAVCFEntry("This is not a VCF entry!")

        for sample in self.samples:
            if not record.has_samp_data(sample):
                empty_call_data = make_calldata_tuple([])()
                call = _Call(record, sample, empty_call_data)
                record.samples.append(call)
        self._record_list.append(record)

    def pop(self, record):
        with suppress(ValueError):
            while True:
                self._record_list.remove(record)
        return record

    def remove(self, record):
        with suppress(ValueError):
            while True:
                self._record_list.remove(record)

    def add_samp_format(self, samp_fmt):
        if samp_fmt not in self._format_cache:
            self._format_cache[samp_fmt] = self._parse_sample_format(samp_fmt)

    def add_record(self, chrom, pos, ID, ref, alt, qual, filt,
                    info, fmt, samples=None):
        ## Transform alt alleles into built-in objects
        ## TODO: Use the other chlidren object to be more accurate.
        alt_list = re.split(",", alt)
        alt = []
        for allele in alt_list:
            alt.append(_Substitution(allele))
        ## Add an empty sample attribute
        record = _Record(chrom, pos, ID, ref, alt, qual, filt,
                info, fmt, self._sample_indexes)
        ## If no sample data provided, then add empty
        for sample in self.samples:
            empty_call_data = make_calldata_tuple([])()
            call = _Call(record, sample, empty_call_data)
            record.samples.append(call)
        self.append(record)

    def add_metaline(self, ID, metaline):
        """
        Add or edit the description of a line of metadata
        """
        if ID not in self.metadata:
            self.metadata[ID] = []
        self.metadata[ID].append(metaline)

    def add_sample(self, ID, genomes, mixture, desc):
        new_sample = _Sample(ID, genomes, mixture, desc)
        self.sampleInfo[ID] = new_sample
        self.samples.append(ID)
        self._sample_indexes[ID] = len(self._sample_indexes)

    def add_info(self, ID, number, typ, desc, source, version):
        """
        Add or edit the description of an INFO field
        """
        new_info = _Info(ID, number, typ, desc, source, version)
        self.infos[ID] = new_info

    def add_filter(self, ID, desc):
        """
        Add or edit the description of a filter
        """
        new_filter = _Filter(ID, desc)
        self.filters[ID] = new_filter

    def add_format(self, ID, num, typ, desc):
        """
        Add or edit the description of a FORMAT field
        """
        new_format = _Format(ID, num, typ, desc)
        self.formats[ID] = new_format

    def add_contig(self, ID, length):
        """
        Add or edit the description of a FORMAT field
        """
        new_contig = _Contig(ID, length)
        self.contigs[ID] = new_contig

    def sort_chrom(self):
        contig_list = list(self.contigs.keys())
        self._record_list = sorted(self._record_list, key=lambda x: contig_list.index(x.CHROM))


    def sort_pos(self):
        """
        Sort the VCF file using the POS field
        """
        self._record_list = sorted(self._record_list, key=lambda x: int(x.POS))
        if len(set([x.CHROM for x in self]) - set(self.contigs.keys())) == 0:
            self.sort_chrom()

    def sort_score(self):
        """
        Sort the VCF file using the QUAL field
        """
        self._record_list = sorted(self._record_list, key=lambda x: x.QUAL)
        if len(set([x.CHROM for x in self]) - set(self.contigs.keys())) == 0:
            self.sort_chrom()

    def find_record(self, chrom=None, pos=None, ref=None, alt=None):
        """
        Find a record based on CHROM, POS, REF or ALT field, or a combination of those.
        """
        time1 = time.time()
        record_list = []
        def find_chrom(chrom):
            return [record for record in self._record_list if record.CHROM == chrom]
        def find_pos(pos):
            return [record for record in self._record_list if record.POS == pos]
        def find_ref(ref):
            return [record for record in self._record_list if record.REF == ref]
        def find_alt(alt):
            return [record for record in self._record_list if alt in record.ALT]
        def merge(record_list1, record_list2):
            for record in record_list2:
                if record not in record_list1:
                    record_list1.append(record)
            return record_list1

        if chrom: record_list = merge(record_list, find_chrom(chrom))
        if pos: record_list = merge(record_list, find_pos(pos))
        if ref: record_list = merge(record_list, find_ref(ref))
        if alt: record_list = merge(record_list, find_alt(alt))
        print("%s"%(time.time()-time1))
        return record_list

    def empty(self):
        """
        Discard all records
        """
        self._record_list = []

    def copy_empty(self):
        """
        Return an empty copy of this vcf file
        """
        vcf_copy = copy.deepcopy(self)
        vcf_copy._record_list = []
        return vcf_copy

    def copy(self):
        return copy.copy(self)

    def import_all(self, reader, skip_invariant=False):
        """
        Store the output of a Reader into self. Copies all attributes.
        """
        self._column_headers = reader._column_headers
        self._format_cache = reader._format_cache
        self._header_lines = reader._header_lines
        self._sample_indexes = reader._sample_indexes
        self.metadata = reader.metadata
        self.infos = reader.infos
        self.formats = reader.formats
        self.alts = reader.alts
        self.samples = reader.samples
        self.filters = reader.filters
        self.contigs = reader.contigs
        for record in reader:
            if skip_invariant:
                if len(record.ALT) == 1 and str(record.ALT[0]) == "<*>":
                    continue
            self.append(record)

    def merge(self, vcf_container):
        for info in vcf_container.infos:
            if info not in self.infos:
                self.infos[info] = vcf_container.infos[info]
        for form in vcf_container.formats:
            if form not in self.formats:
                self.formats[form] = vcf_container.formats[form]
        for filt in vcf_container.filters:
            if filt not in self.filters:
                self.filters[filt] = vcf_container.filters[filt]
        for alt in vcf_container.alts:
            if alt not in self.alts:
                self.alts[alt] = vcf_container.alts[alt]
        for record in vcf_container:
            self.append(record)
        self.sort_pos()

    def timing(f):
        def wrap(*args):
            time1 = time.time()
            ret = f(*args)
            time2 = time.time()
            print("%s function took %0.3f mv")%(f.func_name, (time2-time1)*1000.0)
            return ret
        return wrap

class Error(Exception):
    pass

class NotAVCFEntry(Error):
    def __init__(self, expression):
        self.expression = expression

