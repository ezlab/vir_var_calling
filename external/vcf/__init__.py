#!/usr/bin/env python
"""
A VCFv4.0 and 4.1 parser for Python.

Online version of PyVCF documentation is available at http://pyvcf.rtfd.org/
"""

from .parser import Reader, Writer
from .parser import VCFReader, VCFWriter
from .filters import Base as Filter
from .sample_filter import SampleFilter
## Added by Alexis Loetscher
from .constants import *
from .container import Container

#VERSION = '0.6.8'
VERSION = 'dev'
