localrules: library_qc

import subprocess
import pandas

from Bio import SeqIO

SAMPLE_SHEET = pandas.read_csv(config["SAMPLE_SHEET"])
SAMPLES=SAMPLE_SHEET.loc[:, "sample"].tolist()

MAPPER=config["MAPPER"]

PROJECT_NAME=config["PROJECT_NAME"]
REF=config["REF"]
REF_DIR=config["REF_DIR"]
REF_FILE=config["REF_FILE"]
WORKDIR=config["WORKDIR"]
PIPELINE_DIR=config["PIPELINE_DIR"]
SCRIPT_DIR=config["SCRIPT_DIR"]

REF_SEQ = []
for ref_file in REF_FILE.values():
    fasta = list(SeqIO.parse(ref_file, "fasta"))
    seq = [seq.id for seq in fasta][0]
    REF_SEQ.append(seq)

CUT_GENES=config["CUT_GENES"]

## Use different mappers for some interval?
USE_INTERVAL=config["USE_INTERVAL"]
INTERVAL=config["INTERVAL"]
INTERVAL_NAME=config["INTERVAL_NAME"]
INTERVAL_START=config["INTERVAL"]["start"]
INTERVAL_END=config["INTERVAL"]["end"]
INTERVAL_MAPPER=config["INTERVAL_MAPPER"]

CALLERS=config["callers"]

## Load executable paths
EXE_SAMTOOLS=config["exe"]["samtools"]
EXE_BEDTOOLS=config["exe"]["bedtools"]
EXE_BCFTOOLS=config["exe"]["bcftools"]
EXE_VCFUTILS=config["exe"]["vcfutils.pl"]
EXE_PICARD=config["exe"]["picard"]
EXE_GATK=config["exe"]["GATK"]
EXE_FILTER="%s/%s"%(SCRIPT_DIR, config["exe"]["filter_vcf_max_rank"])
EXE_SEPARATECOV="%s/%s"%(SCRIPT_DIR, config["exe"]["separate_cov"])
EXE_COVONALTHAP="%s/%s"%(SCRIPT_DIR, config["exe"]["covonalthap"])


if USE_INTERVAL == "True":
    TAG = [f"{ref}_{INTERVAL_NAME}" for ref in REF]
else:
    TAG = REF

## Get the appropriate tags for a given reference

def get_prim_contig(wildcards):
    fasta = SeqIO.parse(get_ref_file(wildcards) , "fasta")
    return [record.id for record in fasta][0]

def get_ref(wildcards):
    if "tag" in dir(wildcards):
        if wildcards.tag in REF:
            ref = [re.search(refx, wildcards.tag) for refx in REF
                    if re.search(refx, wildcards.tag)][0][0]
        else:
            ref = wildcards.tag
    elif "ref" in dir(wildcards):
        ref = wildcards.ref
    return ref

def get_ref_file(wildcards):
    if REF_FILE is None:
        ref = get_ref(wildcards)
        #ref_file = f"{REF_DIR}/{ref}.fasta"
        ref_file = f"{REF_DIR}/{ref}.fa"
    else:
        if "tag" in dir(wildcards):
            ref_file = REF_FILE[wildcards.tag]
        elif "ref" in dir(wildcards):
            ref_file = REF_FILE[wildcards.ref]
    return ref_file

def get_ref_snap(wildcards):
    ref = get_ref(wildcards)
    ref_file = f"{REF_DIR}/{ref}"
    return ref_file

## When not using slurm
#TMP_DIR="/tmp/vir_var_calling"
## When using slurm
TMP_DIR="tmp"

include: "rules/preproc.snake"
include: "rules/postproc.snake"
include: "rules/consensus_var_calling.snake"

rule all:
    input:
        expand("calling/{sample}/{sample}.{ref}.{MAPPER}.consensus.cont_ann.func_ann.all.vcf.bgz",
            ref=REF, sample=SAMPLES, MAPPER=MAPPER),
        expand("stats/{sample}/{sample}.{ref}.{MAPPER}.map_stats.dat",
            ref=REF, sample=SAMPLES, MAPPER=MAPPER),
        expand("stats/{sample}/{sample}.{ref}.{MAPPER}.pos_wise_cov.dat",
            ref=REF, sample=SAMPLES, MAPPER=MAPPER),
        expand("rawdata/{sample}/qc/{sample}.{direction}.fastqc.html",
            sample=SAMPLES, direction=["f", "r"]),
        expand("stats/{sample}/{sample}.{MAPPER}.cov_on_alt_haplotypes.dat",
            sample=SAMPLES, MAPPER=MAPPER)

def get_vcf_files_to_build_matrices(wildcards):
    all_vcfs = [[f"calling/{sample}/{sample}.{ref}.{MAPPER}.consensus.ann.vcf.bgz"
                 for sample in SAMPLES]
                for ref in REF]
    return all_vcfs

def get_stats_files_to_build_matrices(wildcards):
    all_stats = [[f"calling/{sample}/{sample}.{ref}.{MAPPER}.pos_wise_cov.dat"
                 for sample in SAMPLES]
                for ref in REF]
    return all_stats

rule library_qc:
    input:
        library="rawdata/{sample}/{sample}.{direction}.fastq.gz"
    output:
        qc="rawdata/{sample}/qc/{sample}.{direction}.fastqc.html"
    log:
        log="log/{sample}/{sample}.{direction}.fastqc_raw.log",
        err="log/{sample}/{sample}.{direction}.fastqc_raw.err"
    run:
        if not os.path.exists(f"rawdata/{wildcards.sample}/qc"):
            os.mkdir(f"rawdata/{wildcards.sample}/qc")
        shell("fastqc --outdir preproc/{wildcards.sample}/qc {input.library} > {log.log} 2> {log.err}")
        shell("touch {output.qc}")

###
## Using bedtools, get coverage on each position in the genome.
## NOTE: If you modify this, make sure to use the mapping used for variant calling.
## TODO: Find a more appropriate place for this rule. Should I
## create a workflow for computing all mapping stats?
## TODO: Comparison between raw and processed mappings?
rule get_pos_wise_coverage:
    input:
        indelqual_bam="mapping/{sample}/{sample}.{ref}.{mapper}.postproc.bam"
    output:
        cov_stats="stats/{sample}/{sample}.{ref}.{mapper}.pos_wise_cov.dat"
    log:
        log="log/{sample}/{sample}.{ref}.{mapper}.pos_wise_coverage.log",
        err="log/{sample}/{sample}.{ref}.{mapper}.pos_wise_coverage.err"
    run:
        shell("{EXE_SAMTOOLS} depth -aa {input.indelqual_bam} > {output.cov_stats} \
        2> {log.log}")
        shell("{EXE_SEPARATECOV} -f {output.cov_stats} -o stats/{wildcards.sample}/{wildcards.sample}.\$chrom.{MAPPER}.pos_wise_cov.dat")


###
## Using samtools, get general stats about mappings
rule get_coverage_stats:
    input:
        indelqual_bam="mapping/{sample}/{sample}.{ref}.{MAPPER}.postproc.bam"
    output:
        map_stats="stats/{sample}/{sample}.{ref}.{MAPPER}.map_stats.dat"
    log:
        log="log/{sample}/{sample}.{ref}.{MAPPER}.coverage.log",
        err="log/{sample}/{sample}.{ref}.{MAPPER}.coverage.err"
    run:
        shell("{EXE_SAMTOOLS} flagstat {input.indelqual_bam} > {output.map_stats} \
        2> {log.log}")

###
## Filter by number of votes
rule filter_by_rank:
    input:
        consensus_vcf="calling/{sample}/{sample}.{ref}.%s.consensus.ann.vcf"%(MAPPER)
    output:
        rank_list="stats/{sample}/{sample}.{ref}.%s.rank_list.vcf"%(MAPPER),
        filtered_4_vote="calling/{sample}/{sample}.{ref}.%s.consensus.ann.4votes.vcf"%(MAPPER),
        filtered_6_vote="calling/{sample}/{sample}.{ref}.%s.consensus.ann.6votes.vcf"%(MAPPER)
    log:
        log="log/{sample}/{sample}.{ref}.%s.filter.log"%(MAPPER),
        err="log/{sample}/{sample}.{ref}.%s.filter.err"%(MAPPER)
    run:
        prefix="calling/%s/%s.%s.%s.consensus.ann"%(wildcards.sample,wildcards.sample,wildcards.ref,MAPPER)
        shell("{EXE_FILTER} --invcffile {input.consensus_vcf} --n_votes 4 6 \
        --rank_sum -l {output.rank_list} \
        --outvcffile {prefix} > {log.log} 2> {log.err}")
        shell("touch {output.filtered_4_vote}")
        shell("touch {output.filtered_6_vote}")
